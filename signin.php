	<?php 
		include_once("inc/config/config.inc.php");
		
		if(@$_SESSION['user']['id']) {
			session_start();
			header("location:".BASE_URL);
			exit;
		}

		include_once("header.php"); 
	?>

	<!-- signin-page -->
	<section class="clearfix job-bg user-page">
		<div class="container">
			<div class="row text-center">
				<!-- user-login -->			
				<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
					<div class="user-account">
						<h2>Login</h2>
						<!-- form -->
						<form action="<?=BASE_URL?>inc/controllers/login_validation.php" method="POST">
							<div class="form-group">
								<input type="email" name="username" class="form-control" placeholder="Username" required>
							</div>
							<div class="form-group">
								<input type="password" name="password" class="form-control" placeholder="Password" required>
							</div>
							<button type="submit" name="client_login" class="btn" value="submit">Login</button>
							<hr/>
							<h4 class="text-center"> OR </h4>
							<hr/>
							<a href="<?=BASE_URL?>signup.php" class="btn-primary">Create a New Account</a>
						</form>
						<!-- form -->
					</div>
				</div><!-- user-login -->			
			</div><!-- row -->	
		</div><!-- container -->
	</section><!-- signin-page -->
	
	<?php include("footer.php"); ?>