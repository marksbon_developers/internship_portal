-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2018 at 11:28 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `internship`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `company_tagline` varchar(150) NOT NULL,
  `about_us` text NOT NULL,
  `residence_address` varchar(100) NOT NULL,
  `postal_address` varchar(100) NOT NULL,
  `telephone_1` varchar(20) NOT NULL,
  `telephone_2` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `website` varchar(255) NOT NULL,
  `specialization` varchar(20) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `name`, `company_tagline`, `about_us`, `residence_address`, `postal_address`, `telephone_1`, `telephone_2`, `email`, `website`, `specialization`, `status`, `date_created`) VALUES
(1, 'Duraplast Company Limited', '...where Duraplast goes, water flows.', 'DURAPLAST Limited has for many years been the main supplier of high-quality, unplasticised Polyvinyl Chloride (uPVC) and HDPE pipes and fittings for Ghana\'s water and electrical engineering markets. Equiped with ultra modern machinery, DURAPLAST uses the latest technology to produce pipes that meet the exact international standards.\r\n\r\nAll our pipes are rigorously tested in our laboratory and certified by the Ghana standards Board and meet rigid British and Metric Standards.\r\n\r\nDURAPLAST puts a lot of emphasis on manpower training as a means of finding quick solutions to our clients\' problems. Thus, our team of expert engineers and technical advisors is always available to offer technical advice to our clients on the correct use of DURAPLAST uPVC and HDPE pipes and fittings.\r\n\r\nOur state-of-art factory and highly qualified workforce have been traditionally ensuring that quality uPVC and HDPE pipes are always available to satisfy our clients\' demand. with our high technology extrusion equipment, DURAPLAST produces pipes for a wide range of applications in irrigation, bore-hole drilling, electrical power distribution and telecommunications industries.\r\n\r\nDURAPLAST\'s world-class quality products are playing a significant role in Ghana and the ECOWAS region\'s industrial and economic development. This is reflected by our large clientele base both in and outside Ghana', 'North Industrial Area', 'Plot No\'s: 10, 11, &amp; 12 Dadeban Road, North Industrial Area, Accra North.', '+233 (0)302- 223989', '+233 (0)244 336 665', 'info@duraplastghana.', 'http://www.duraplastghana.com/', '1', 'approved', '2018-06-11 16:11:05'),
(2, 'Duraplast Limited', 'Where Duraplast Goes, Water Flows', 'DURAPLAST LTD specializes in the production of unplasticised Polyvinyl Chloride (uPVC), Polyvinyl Chloride (PVC) and High DensityPolyethylene (HDPE) Pipes and Fittings. The factory and offices are located on plots 10, 11 and 12 Dadeban Road, North Industrial Area,Accra, Ghana in West Africa.\r\n\r\nThe company started operations in November 1969 with the production of unplasticized polyvinyl chloride (uPVC) pipes of various sizes. However, over the years, it has progressed to become a leader in thepipe industry in West Africa and parts of Central Africa by\r\nintroducing several other innovative products.', 'Plot No\'s: 10, 11, &amp; 12 Dadeban Road,', 'P.O. Box AN 7136 Accra - North,', '0302223989', '0302225001', 'info@duraplastgh.com', 'http://www.duraplastghana.com/', '1', 'approved', '2018-06-12 15:11:29'),
(3, 'Ashfoam', 'Feel it...Know it...', 'ABOUT ASHFOAM\r\nAshfoam is the leading foam manufacturing company in Ghana and one of the biggest foam producers in the West African Region. Established in 1978 by the great industrialist Mr. Robert Habib Hitti, Ashfoam is currently peaking with a market capitalization of size, expansions, development of new product lines, assets and sales targets.\r\n\r\nAshfoam is the only manufacturing, distribution and sale of Polyurethane Foam products in Ghana with an ISO certification (ISO 9001-2008). Ensuring you that the quality management systems live up to international standards.\r\n\r\nCurrently Ashfoam has four factories in the West African Sub Region. Two in Ghana, in Accra and Kumasi, one in Benin and one in Niger. You will find our yellow and red branded stores all over Ghana, making your next purchase easy and convenient.\r\n\r\nAshfoam is more than just a foam manufacturing company, Ashfoam is your one stop shop for all your home furnishing - including mattresses, pillows, linen, furniture and home decor.\r\n\r\nFeel It... Know It...  \r\n\r\nQuality Policy\r\nAshanti Foam Factory Limited is committed to the best quality of home furnishing products that satisfies the needs of the customer. We review our processes and take appropriate measures to promote quality awareness and continual quality improvement.\r\n\r\nVision\r\nAshfoam aspires to be a world class brand in the home furniture industry committed to the production of quality products and a brand of choice in every household.\r\n\r\nMission\r\nOur desire to be market leaders in home furnishings, driven by the sleeping, health needs and comfort of our perceived consumers, set us to be committed to producing the best quality foam products and home furniture that provides satisfaction to our consumers.', '9-12 Dadeban Road', 'P.O. Box AN 7136 Accra - North,', '0302253978', '0302628709', 'info@ashfoam.com', 'https://www.ashfoamghana.com/', '2', 'approved', '2018-06-12 15:44:39'),
(4, 'Ashfoam', 'Feel it...Know it...', 'Ashanti Foam Factory Limited is committed to the best quality of home furnishing products that satisfies the needs of the customer. We review our processes and take appropriate measures to promote quality awareness and continual quality improvement.\r\nAshfoam aspires to be a world class brand in the home furniture industry committed to the production of quality products and a brand of choice in every household.\r\nOur desire to be market leaders in home furnishings, driven by the sleeping, health needs and comfort of our perceived consumers, set us to be committed to producing the best quality foam products and home furniture that provides satisfaction to our consumers.', 'Plot No\'s: 10, 11, &amp; 12 Dadeban Road,', 'P.O. Box AN 7136 Accra - North,', '0302223989', '0302225001', 'info@ashfoam.com', 'https://www.ashfoamghana.com/', '2', 'approved', '2018-06-18 13:24:23'),
(5, 'Coolimk Limited', 'Your Best Cooling Solution', 'P.O BOX 7136 Accra North', '', 'Plot 19, Dadeban Street', '0302 224 816', '+233 (302) 228 822', 'info@coolinkghana.co', 'http://www.coolinkghana.com/', '3', 'approved', '2018-06-18 13:29:40'),
(6, 'Aramex Ghana LTD', 'Delivery Unlimited', '', 'Spintex Rd, Accra', '101 Spintex Rd PLOT 17', '0302171772', '0302646464', 'info@aramex.com', 'aramex.com', '5', 'approved', '2018-06-18 13:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `email_alerts`
--

CREATE TABLE `email_alerts` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `specialization_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_alerts`
--

INSERT INTO `email_alerts` (`id`, `student_id`, `email`, `specialization_id`, `company_id`, `status`, `date_created`) VALUES
(1, 1, 'joyful802015@gmail.com', 1, 0, 'active', '2018-06-12 14:32:31'),
(2, 2, '04TU916041@live.gtuc.edu.gh', 2, 0, 'active', '2018-06-12 16:18:49'),
(3, 1, 'nii@nai.com', 1, 0, 'active', '2018-06-19 13:10:52');

-- --------------------------------------------------------

--
-- Table structure for table `job_postings`
--

CREATE TABLE `job_postings` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `interest_category_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `region` varchar(50) NOT NULL,
  `location` varchar(255) NOT NULL,
  `experience_id` int(11) NOT NULL,
  `salary_expectation` varchar(50) NOT NULL,
  `job_description` text NOT NULL,
  `responsibilities` text NOT NULL,
  `skill_requirement` text NOT NULL,
  `duration` text NOT NULL,
  `post_deadline` date NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `approved_by` int(11) NOT NULL,
  `approved_date` datetime NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `job_postings`
--

INSERT INTO `job_postings` (`id`, `company_id`, `interest_category_id`, `job_title`, `region`, `location`, `experience_id`, `salary_expectation`, `job_description`, `responsibilities`, `skill_requirement`, `duration`, `post_deadline`, `status`, `approved_by`, `approved_date`, `date_created`) VALUES
(1, 1, 1, 'Telecomm. Eng. Asst', 'Greater Accra', 'North Industrial Area', 1, '500', 'The role of administrator involves a great deal of multitasking. You will work with teams, oversee the operations within your company, manage groups, coordinate with management and engage in planning according to the needs of your company. If there are office resource or administrative issues, you will be the person expected to deal with them.', 'If you work in a large company, you will probably have a clearly defined role. In smaller companies, you may be expected to act as a jack of all trades. This means being in charge of human resources, training functions and even accounting. This would extend to tasks such as hiring, training &amp; assessing employees, procuring office supplies and developing financial reports &amp; budgets.', 'Here is a quick list of typical administrator duties:\r\n\r\nManagement of office equipment\r\nMaintaining a clean and enjoyable working environment\r\nHandling external or internal communication or management systems\r\nManaging clerical or other administrative staff\r\nOrganizing, arranging and coordinating meetings\r\nSorting and distributing incoming and outgoing post', 'July - August 2018', '2018-08-04', 'approved', 0, '0000-00-00 00:00:00', '2018-06-11 16:12:56'),
(2, 2, 1, 'Intercom Admin', 'Greater Accra', '', 3, '600', 'Ensure not entire team to have access to everything in Intercom. \r\nMaking Sure that all team members can access the People and Company lists and use the Conversations Inbox and send replies. This feature is only available to customers on Standard plans.', 'To add a new Intercom partition or configure\r\nan existing partition.\r\nTo add a new Intercom Calling Search Space.\r\nTo add a new Intercom Translation Pattern or\r\nto configure an existing Intercom Translation\r\nPattern .', 'Bsc in Telecommunication Engineering', 'June 21, 2018 - July 21 2018', '2018-07-03', 'approved', 0, '0000-00-00 00:00:00', '2018-06-12 15:58:53'),
(3, 6, 5, 'Customer Relations Manager', 'Greater Accra', 'Spintex Rd, Accra', 1, '500', 'Customer Support  and Relations', 'Customer Support Management', 'Theoretical Knowledge in Human Relations and Customer Support', 'June 21, 2018 - July 21 2018', '2018-07-09', 'approved', 0, '0000-00-00 00:00:00', '2018-06-18 15:43:21');

-- --------------------------------------------------------

--
-- Table structure for table `news_subscribers`
--

CREATE TABLE `news_subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_courses`
--

CREATE TABLE `settings_courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_experiences`
--

CREATE TABLE `settings_experiences` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_experiences`
--

INSERT INTO `settings_experiences` (`id`, `name`, `status`, `date_created`) VALUES
(1, 'Undergraduate Internship', 'active', '2018-05-23 20:29:01'),
(2, 'Fresh Graduate', 'active', '2018-05-23 20:29:01'),
(3, 'Experienced (Non-Manager)', 'active', '2018-05-23 20:29:01'),
(4, 'Manager (Staff Supervisor/Head of Department)', 'active', '2018-05-23 20:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `settings_faculties`
--

CREATE TABLE `settings_faculties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `programme_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_interest_category`
--

CREATE TABLE `settings_interest_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_interest_category`
--

INSERT INTO `settings_interest_category` (`id`, `name`, `icon`, `status`, `date_created`) VALUES
(1, 'Telecommunication', '3.png', 'active', '2018-03-23 01:03:10'),
(2, 'Computer Eng.', '1.png', 'active', '2018-03-23 01:03:10'),
(3, 'Solar Science', '10.png', 'active', '2018-03-23 01:03:10'),
(5, 'Procurement & Log', '4.png', 'active', '2018-03-23 01:03:10'),
(6, 'Accounting', '5.png', 'active', '2018-03-23 01:03:10'),
(7, 'Banking & Finance', '11.png', 'active', '2018-03-23 01:03:10'),
(8, 'Economics', '6.png', 'active', '2018-03-23 01:03:10'),
(9, 'HR Management', '8.png', 'active', '2018-03-23 01:03:10'),
(10, 'Management', '2.png', 'active', '2018-03-23 01:03:10'),
(11, 'Information Technology', '7.png', 'active', '2018-03-23 01:04:51'),
(12, 'Miscellaneous', '12.png', 'inactive', '2018-03-23 01:04:51');

-- --------------------------------------------------------

--
-- Table structure for table `settings_posting_type`
--

CREATE TABLE `settings_posting_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `duration` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_programmes`
--

CREATE TABLE `settings_programmes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `period` varchar(20) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_programmes`
--

INSERT INTO `settings_programmes` (`id`, `name`, `code`, `period`, `status`, `date_created`) VALUES
(1, 'BSC Computer science', 'BSC/M017', '4 Years', 'active', '2018-05-11 11:26:02'),
(2, 'Diploma in Business Study (DBS)', 'DBS/IT', '2 Years', 'active', '2018-05-11 11:26:02'),
(3, 'Bsc In Infromation Techology', 'BIT/L400', '4 Years', 'active', '2018-05-29 11:40:00'),
(4, 'BSc Procurement & Logistics', 'BSC/PL/4', '4 years', 'active', '2018-05-29 11:40:00'),
(5, 'BSc Accounting', 'BSC/ACC/4', '4 Years', 'active', '2018-05-29 11:40:00'),
(6, 'BSc Human Resource Management', 'BSC/HRM/4', '4 Years', 'active', '2018-05-29 11:40:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings_progrogramme_session`
--

CREATE TABLE `settings_progrogramme_session` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_progrogramme_session`
--

INSERT INTO `settings_progrogramme_session` (`id`, `name`, `status`, `created_date`) VALUES
(1, 'Morning', 'active', '0000-00-00 00:00:00'),
(2, 'Evening', 'active', '0000-00-00 00:00:00'),
(3, 'Weekends', 'active', '0000-00-00 00:00:00'),
(4, 'Distance', 'active', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `signup_approval_requests`
--

CREATE TABLE `signup_approval_requests` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_biodata`
--

CREATE TABLE `student_biodata` (
  `id` int(11) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `other_names` varchar(100) NOT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `marital_status` varchar(50) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `residence_address` varchar(255) DEFAULT NULL,
  `postal_address` text,
  `photo_id` int(11) DEFAULT NULL,
  `resume_id` tinyint(4) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone_no_1` varchar(255) NOT NULL,
  `phone_no_2` varchar(255) NOT NULL,
  `index_no` varchar(50) NOT NULL,
  `programme_code` varchar(10) NOT NULL,
  `admission_date` date NOT NULL,
  `completion_date` date NOT NULL,
  `current_level` int(3) NOT NULL,
  `current_registered_courses` text NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `student_biodata`
--

INSERT INTO `student_biodata` (`id`, `surname`, `other_names`, `gender`, `date_of_birth`, `marital_status`, `nationality`, `residence_address`, `postal_address`, `photo_id`, `resume_id`, `email`, `phone_no_1`, `phone_no_2`, `index_no`, `programme_code`, `admission_date`, `completion_date`, `current_level`, `current_registered_courses`, `status`, `created_date`) VALUES
(1, 'Offei', 'Bismark', 'male', '2018-05-08', 'single', 'ghanaian', 'abelemkpe', 'box AB 1022, Ablekuma - Accra.', NULL, 1, '04TU616041@live.gtuc.edu.gh', '02345678', '024567854', '04TU616041', 'BSC/M017', '2014-08-03', '2018-10-23', 300, 'Data Structures II, Operations Research I, Java Programming', 'active', '2018-05-14 10:58:18'),
(2, 'Assogba', 'Djimon', 'male', '1990-12-12', 'married', 'ghanaian', 'Tesano-Area', 'box LP 1023, Lapaz - Accra.', NULL, 2, '04TU916041@live.gtuc.edu.gh', '0246419008', '0266419008', '04TU916041', 'BSC/PL/4', '2017-05-06', '2018-06-28', 400, 'Data Structures II, Operations Research I, Java Programming\r\n', 'active', '2018-05-28 16:56:42'),
(3, 'OSMANU', 'ABUBAKARI', 'male', '1990-04-20', 'single', 'ghanaian', 'Tesano-Area', 'box AN 1022, Accra - North', NULL, 3, '04TU916056@live.gtuc.edu.gh', '0244415002', '0274441008', '04TU916056', 'BIT/L400', '2017-05-06', '2018-06-26', 400, 'Computer Security, System Admin, Managemnet Info System, Electronic Commerce', 'active', '2018-05-28 16:56:42'),
(4, 'Appiah', 'Isaac', 'male', '1990-04-20', 'single', 'ghanaian', 'Kaneshie First Light. ', 'box KT 10, Kotobabi - Accra.', NULL, 4, '04TU916069@live.gtuc.edu.gh', '0244415002', '0244415003', '04TU916069', 'BSC/ACC/4', '2017-05-06', '2018-06-20', 400, 'Computer Security, System Admin, Managemnet Info System, Electronic Commerce', 'active', '2018-05-28 16:56:42'),
(5, 'Komeh', 'Samuel', 'male', '1992-02-18', 'single', 'ghanaian', 'Tesano - Accra', 'box DC 102, Dansoman - Accra.', NULL, 5, '04TU916060@live.gtuc.edu.gh', '0266999888', '0244999888', '04TU916060', 'BSC/HRM/4', '2017-03-02', '2019-01-01', 300, 'Computer Security, System Admin, Managemnet Info System, Electronic Commerce', 'active', '2018-05-28 16:56:42'),
(6, 'Bamfo', 'Rita', 'Female', '2018-06-12', 'single', 'ghanaian', 'tesano', 'P.O Box 3425 Acrra Tesano', NULL, 6, 'rita@gtuc.com', '0245632718', '', '04TU916020', 'BSC/PL/4', '2009-05-12', '2012-06-15', 400, 'Computer Science', 'active', '2018-06-03 18:41:43'),
(7, 'Osborne', 'Mordre', 'male', '2016-09-07', 'Married', 'Ghanaian', 'Amasaman', 'P.O Box 5246 OSU', NULL, 7, 'osborne.mordred@gmail.com', '0302584256', '0302659845', '04TU916010', 'BIT/L400', '2017-09-04', '2018-09-03', 400, 'System Admin\r\nComputer Security\r\nElectronic Commerce\r\nManagement Information System', 'active', '2018-06-12 16:48:46'),
(8, 'Akom', 'Sumaya', 'female', '1994-11-14', 'single', 'Ghanaian', '101 Dome Ave', '101 Dome ave accra Ghana', NULL, NULL, '04TU916100@Live.gtuc.edu.gh', '02441587474', '', '04TU916100', 'BSC/M017', '2015-05-18', '2019-02-14', 400, '', 'active', '2018-06-18 14:57:10'),
(9, 'OFORI', 'PHILOMINA P', 'female', '1985-06-14', 'single', 'GHANAIAN', 'GTUC SCHOOL FOI', 'GHANA TECHNOLOGY UNIVERSITY COLLEGE TESANO CAMPUS', NULL, NULL, '04TU916101@LIVE.GTUC.EDU.GH', '0302555888', '0244100000', '04tu916101', 'BSC/PL/4', '2017-09-04', '2019-02-14', 400, 'Computer Security\r\nManagement Information Systems\r\nSystem Administration\r\nSupervisory \r\nProject Work ', 'active', '2018-06-18 15:36:23'),
(10, 'yeboah', 'israel', 'male', '2018-06-05', 'single', 'ghanaian', 'tesano', 'po box 3287', NULL, NULL, 'israelyeboah@gmail.com', '0221234567', '', '030914173', 'DBS/IT', '2018-06-06', '2018-06-27', 400, 'System Admin\r\nComputer Security\r\nE-Commerce', 'active', '2018-06-20 14:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `student_careerinfo`
--

CREATE TABLE `student_careerinfo` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_code` varchar(100) NOT NULL,
  `admission_date` date NOT NULL,
  `completion_date` date NOT NULL,
  `current_level` int(3) NOT NULL,
  `current_registered_courses` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_job_applications`
--

CREATE TABLE `student_job_applications` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `job_post_id` int(11) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_job_applications`
--

INSERT INTO `student_job_applications` (`id`, `student_id`, `job_post_id`, `status`, `date_created`) VALUES
(1, 3, 3, 'pending', '2018-06-18 15:45:32'),
(2, 3, 2, 'pending', '2018-06-18 16:48:59'),
(3, 1, 3, 'pending', '2018-06-27 18:53:47'),
(4, 2, 3, 'pending', '2018-06-27 19:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `student_latest_job`
--

CREATE TABLE `student_latest_job` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `position_department` varchar(50) NOT NULL,
  `period` varchar(50) NOT NULL,
  `duties` text NOT NULL,
  `career_objective` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_latest_job`
--

INSERT INTO `student_latest_job` (`id`, `student_id`, `company_name`, `position_department`, `period`, `duties`, `career_objective`, `status`, `date_created`) VALUES
(1, 1, 'Afrikiko', 'Parks &amp; Garden', 'febraury ~ march 2017', 'That one noooorrrr', 'xsdtyj ghj ndjfn skjrf sj ne', 'active', '2018-07-08 09:34:25');

-- --------------------------------------------------------

--
-- Table structure for table `student_registration`
--

CREATE TABLE `student_registration` (
  `id` int(11) NOT NULL,
  `index_no` varchar(100) NOT NULL,
  `specializations` varchar(255) NOT NULL,
  `status` enum('pending','approved','declined') DEFAULT 'pending',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `student_registration`
--

INSERT INTO `student_registration` (`id`, `index_no`, `specializations`, `status`, `created_date`, `modified_date`) VALUES
(1, '04TU616041', '[\'1\',\'3\']', 'approved', '2018-05-30 10:52:53', '0000-00-00 00:00:00'),
(2, '04TU916020', '[\'2\']', 'approved', '2018-06-03 18:43:53', '0000-00-00 00:00:00'),
(3, '04TU916041', '[\'2\']', 'approved', '2018-06-12 16:15:29', '0000-00-00 00:00:00'),
(5, '04TU916010', '[\'1\']', 'approved', '2018-06-12 16:51:13', '0000-00-00 00:00:00'),
(6, '04TU916056', '[\'11\']', 'approved', '2018-06-18 13:50:28', '0000-00-00 00:00:00'),
(7, '04TU916100', '[\'2\']', 'approved', '2018-06-18 15:39:24', '0000-00-00 00:00:00'),
(8, '030914173', '[\'3\']', 'pending', '2018-06-20 14:45:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_secondary_education`
--

CREATE TABLE `student_secondary_education` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `name_of_institution` varchar(255) NOT NULL,
  `course` varchar(255) NOT NULL,
  `admission_date` date NOT NULL,
  `completion_date` date NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_secondary_education`
--

INSERT INTO `student_secondary_education` (`id`, `student_id`, `name_of_institution`, `course`, `admission_date`, `completion_date`, `status`, `created_date`) VALUES
(1, 1, 'Presbyterian Boys Senior High', 'General Science', '2012-05-11', '2015-06-14', 'active', '2018-05-18 12:27:17'),
(2, 2, 'Mfantipim Boys Senior High', 'General Art 2', '2011-05-12', '2014-06-15', 'active', '2018-05-18 12:27:17'),
(3, 3, 'Bishop Herman Senior High', 'General Science', '2009-05-12', '2012-06-15', 'active', '2018-05-18 12:27:17'),
(4, 4, 'St. Paul Senior High', 'General Science', '2009-05-12', '2012-06-15', 'active', '2018-05-18 12:27:17'),
(5, 5, 'St. Francis Xavier Senior High', 'Home Economics', '2009-05-12', '2012-06-15', 'active', '2018-05-18 12:27:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Contains the Auto Generated ID for the User',
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL COMMENT 'Contains the Login Username of User',
  `password` varchar(100) NOT NULL COMMENT 'Contains the Login Passwordof User',
  `student_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `login_url` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted','') NOT NULL DEFAULT 'inactive' COMMENT 'Contains account info status',
  `created_by` varchar(50) NOT NULL COMMENT 'Contains the Employee ID of the Creator',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Contains the date and time the user was created'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `student_id`, `company_id`, `login_url`, `profile_picture`, `status`, `created_by`, `date_created`) VALUES
(1, 'Bismark', 'Offei', 'nii@nai.com', '$2y$10$0enlBH07qnbhXjXed.diS.5M.qOzO3c4yP9GNhI0mWjYtkEnAuKQu', 1, 0, 'student/profile.php', 'student_MQ==.jpeg', 'inactive', '', '2018-05-30 10:52:53'),
(6, 'Duraplast Company Limited', '', 'info@duraplastghana.com', '$2y$10$6YCtXjmbC9dAmAqL255LnOR5KmoUbHAUJTkhrclobdU5UCpTdgbV2', 0, 1, 'company/profile.php', 'company_MQ==.png', 'inactive', '', '2018-06-11 16:11:05'),
(4, 'admin', '', 'admin@system.com', '$2y$10$0enlBH07qnbhXjXed.diS.5M.qOzO3c4yP9GNhI0mWjYtkEnAuKQu', 0, 0, 'admin/', 'student_MDRUVTYxNjA0MQ==.jpg', 'inactive', '', '2018-05-30 10:52:53'),
(5, 'Rita', 'Bamfo', 'rita@gtuc.com', '$2y$10$z/ycyxibntoEj0orOnTKf.1BQlQ/zLt/i96glHAUE4h8v4kA4lwku', 6, 0, 'student/profile.php', 'student_MDRUVTkxNjAyMA==.jpg', 'inactive', '', '2018-06-03 18:43:53'),
(7, 'Duraplast Limited', '', 'info@duraplastgh.com', '$2y$10$t6iU4FWvK3202QFuwufBLuGH58q1dMmH5Pp/spEvrItx.Tt6bEJTi', 0, 2, 'company/profile.php', 'company_Mg==.png', 'inactive', '', '2018-06-12 15:11:29'),
(8, 'Ashfoam', '', 'info@ashfoam.com', '$2y$10$b1iNkEYLuIVRiBabPwSUR.54TXOjTDBcrPucUqTfiREQsVw3AP0T6', 0, 3, 'company/profile.php', 'company_Mw==.png', 'inactive', '', '2018-06-12 15:44:39'),
(9, 'Djimon', 'Assogba', '04TU916041@live.gtuc.edu.gh', '$2y$10$/ZjUlv19rfksvekhpmrTW.ibTPpJnCcDxqOE0kE9L356mwI6OqZ3i', 2, 0, 'student/profile.php', 'student_MDRUVTkxNjA0MQ==.jpg', 'inactive', '', '2018-06-12 16:15:29'),
(10, 'Samuel', 'Komeh', 'osborne.mordred@gmail.com', '$2y$10$OAJ5f.ELj1EvhK8Ks4tr.eDAGjKvDYZkX7PvOOUafXvfCmKWTYM3m', 5, 0, 'student/profile.php', 'student_MDRUVTkxNjA2MA==.jpg', 'inactive', '', '2018-06-12 16:42:36'),
(11, 'Coolimk Limited', '', 'info@coolinkghana.com', '$2y$10$/w9XpyizibQnS78Hsm78oOuwF6.BQMSMt9iTsE.m061asB.nWNcwu', 0, 5, 'company/profile.php', 'company_NQ==.png', 'inactive', '', '2018-06-18 13:29:40'),
(12, 'Aramex Ghana LTD', '', 'info@aramex.com', '$2y$10$s54dkEQOMhanjnK042mgiuKZhiCwHi6z9gZpYUcbbFGn/jBBaxSzW', 0, 6, 'company/profile.php', 'company_Ng==.png', 'inactive', '', '2018-06-18 13:35:30'),
(13, 'ABUBAKARI', 'OSMANU', '04TU916056@live.gtuc.edu.gh', '$2y$10$rLVrRl297xWR3WUuUJdpje3u6ApDxyIUJWenrBOi8pxgFbFyqgM82', 3, 0, 'student/profile.php', 'student_MDRUVTkxNjA1Ng==.jpg', 'inactive', '', '2018-06-18 13:50:28'),
(14, 'Sumaya', 'Akom', '04TU916100@live.gtuc.edu.gh', '$2y$10$BxI7dq5YNQT5XC3tFSF43emfBLZcH8Klv9jRrVHzkaPogz4yfLSG.', 8, 0, 'student/profile.php', 'student_MDRUVTkxNjEwMA==.png', 'inactive', '', '2018-06-18 15:39:24'),
(15, 'israel', 'yeboah', 'israelyeboah@gmail.com', '$2y$10$k3Qae1PpM3UUgaowtcWmWOGgaPWeXn4VjdXkUe3ziiNKD0Jcx1B7.', 10, 0, 'student/profile.php', 'student_MDMwOTE0MTcz.png', 'inactive', '', '2018-06-20 14:45:44');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_company_info`
-- (See below for the actual view)
--
CREATE TABLE `vw_company_info` (
`id` int(11)
,`name` varchar(150)
,`company_tagline` varchar(150)
,`about_us` text
,`residence_address` varchar(100)
,`postal_address` varchar(100)
,`telephone_1` varchar(20)
,`telephone_2` varchar(20)
,`email` varchar(20)
,`website` varchar(255)
,`specialization` varchar(20)
,`status` enum('pending','approved','declined')
,`date_created` datetime
,`company_logo` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_jobposting`
-- (See below for the actual view)
--
CREATE TABLE `vw_jobposting` (
`id` int(11)
,`company_id` int(11)
,`interest_category_id` int(11)
,`job_title` varchar(255)
,`region` varchar(50)
,`location` varchar(255)
,`experience_id` int(11)
,`salary_expectation` varchar(50)
,`job_description` text
,`responsibilities` text
,`skill_requirement` text
,`duration` text
,`post_deadline` date
,`status` enum('pending','approved','declined')
,`approved_by` int(11)
,`approved_date` datetime
,`date_created` datetime
,`company_name` varchar(150)
,`interest_category_name` varchar(50)
,`experience_level` varchar(255)
,`company_logo` varchar(255)
,`company_status` enum('pending','approved','declined')
,`company_residence_address` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_settings_interest_category`
-- (See below for the actual view)
--
CREATE TABLE `vw_settings_interest_category` (
`id` int(11)
,`name` varchar(50)
,`icon` varchar(255)
,`status` enum('active','inactive')
,`date_created` datetime
,`total_post` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_student_details`
-- (See below for the actual view)
--
CREATE TABLE `vw_student_details` (
`id` int(11)
,`surname` varchar(100)
,`other_names` varchar(100)
,`fullname` varchar(201)
,`gender` varchar(20)
,`date_of_birth` date
,`marital_status` varchar(50)
,`nationality` varchar(255)
,`residence_address` varchar(255)
,`postal_address` text
,`photo_id` int(11)
,`resume_id` tinyint(4)
,`email` varchar(255)
,`phone_no_1` varchar(255)
,`phone_no_2` varchar(255)
,`index_no` varchar(50)
,`programme_code` varchar(10)
,`status` enum('active','inactive','deleted')
,`created_date` datetime
,`programme_name` varchar(255)
,`admission_date` date
,`completion_date` date
,`current_level` int(3)
,`current_registered_courses` text
,`company_name` varchar(255)
,`position_department` varchar(50)
,`period` varchar(50)
,`duties` text
,`career_objective` text
,`shs_name` varchar(255)
,`shs_course_studied` varchar(255)
,`shs_entry_date` date
,`shs_completion_date` date
,`registration_id` int(11)
,`registration_status` enum('pending','approved','declined')
,`profile_picture` varchar(255)
,`total_jobs_applied` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_student_job_applications`
-- (See below for the actual view)
--
CREATE TABLE `vw_student_job_applications` (
`id` int(11)
,`student_id` int(11)
,`job_post_id` int(11)
,`status` enum('pending','approved','declined')
,`date_created` datetime
,`fullname` varchar(201)
,`profile_picture` varchar(255)
,`programme_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_company_info`
--
DROP TABLE IF EXISTS `vw_company_info`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_company_info`  AS  select `a`.`id` AS `id`,`a`.`name` AS `name`,`a`.`company_tagline` AS `company_tagline`,`a`.`about_us` AS `about_us`,`a`.`residence_address` AS `residence_address`,`a`.`postal_address` AS `postal_address`,`a`.`telephone_1` AS `telephone_1`,`a`.`telephone_2` AS `telephone_2`,`a`.`email` AS `email`,`a`.`website` AS `website`,`a`.`specialization` AS `specialization`,`a`.`status` AS `status`,`a`.`date_created` AS `date_created`,`b`.`profile_picture` AS `company_logo` from (`company_info` `a` left join `users` `b` on((`a`.`id` = `b`.`company_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_jobposting`
--
DROP TABLE IF EXISTS `vw_jobposting`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_jobposting`  AS  select `a`.`id` AS `id`,`a`.`company_id` AS `company_id`,`a`.`interest_category_id` AS `interest_category_id`,`a`.`job_title` AS `job_title`,`a`.`region` AS `region`,`a`.`location` AS `location`,`a`.`experience_id` AS `experience_id`,`a`.`salary_expectation` AS `salary_expectation`,`a`.`job_description` AS `job_description`,`a`.`responsibilities` AS `responsibilities`,`a`.`skill_requirement` AS `skill_requirement`,`a`.`duration` AS `duration`,`a`.`post_deadline` AS `post_deadline`,`a`.`status` AS `status`,`a`.`approved_by` AS `approved_by`,`a`.`approved_date` AS `approved_date`,`a`.`date_created` AS `date_created`,`b`.`name` AS `company_name`,`c`.`name` AS `interest_category_name`,`d`.`name` AS `experience_level`,`e`.`profile_picture` AS `company_logo`,`b`.`status` AS `company_status`,`b`.`residence_address` AS `company_residence_address` from ((((`job_postings` `a` left join `company_info` `b` on((`a`.`company_id` = `b`.`id`))) left join `settings_interest_category` `c` on((`a`.`interest_category_id` = `c`.`id`))) left join `settings_experiences` `d` on((`a`.`experience_id` = `d`.`id`))) left join `users` `e` on((`a`.`company_id` = `e`.`company_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_settings_interest_category`
--
DROP TABLE IF EXISTS `vw_settings_interest_category`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_settings_interest_category`  AS  select `a`.`id` AS `id`,`a`.`name` AS `name`,`a`.`icon` AS `icon`,`a`.`status` AS `status`,`a`.`date_created` AS `date_created`,(select count(`job_postings`.`id`) from `job_postings` where ((`job_postings`.`interest_category_id` = `a`.`id`) and (`job_postings`.`status` = 'approved') and (cast(`job_postings`.`post_deadline` as date) > now()))) AS `total_post` from `settings_interest_category` `a` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_student_details`
--
DROP TABLE IF EXISTS `vw_student_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_student_details`  AS  select `biodata`.`id` AS `id`,`biodata`.`surname` AS `surname`,`biodata`.`other_names` AS `other_names`,concat(`biodata`.`surname`,' ',`biodata`.`other_names`) AS `fullname`,`biodata`.`gender` AS `gender`,`biodata`.`date_of_birth` AS `date_of_birth`,`biodata`.`marital_status` AS `marital_status`,`biodata`.`nationality` AS `nationality`,`biodata`.`residence_address` AS `residence_address`,`biodata`.`postal_address` AS `postal_address`,`biodata`.`photo_id` AS `photo_id`,`biodata`.`resume_id` AS `resume_id`,`biodata`.`email` AS `email`,`biodata`.`phone_no_1` AS `phone_no_1`,`biodata`.`phone_no_2` AS `phone_no_2`,`biodata`.`index_no` AS `index_no`,`biodata`.`programme_code` AS `programme_code`,`biodata`.`status` AS `status`,`biodata`.`created_date` AS `created_date`,`course`.`name` AS `programme_name`,`biodata`.`admission_date` AS `admission_date`,`biodata`.`completion_date` AS `completion_date`,`biodata`.`current_level` AS `current_level`,`biodata`.`current_registered_courses` AS `current_registered_courses`,`job`.`company_name` AS `company_name`,`job`.`position_department` AS `position_department`,`job`.`period` AS `period`,`job`.`duties` AS `duties`,`job`.`career_objective` AS `career_objective`,`shs_education`.`name_of_institution` AS `shs_name`,`shs_education`.`course` AS `shs_course_studied`,`shs_education`.`admission_date` AS `shs_entry_date`,`shs_education`.`completion_date` AS `shs_completion_date`,`regis`.`id` AS `registration_id`,`regis`.`status` AS `registration_status`,`users`.`profile_picture` AS `profile_picture`,(select count(`student_registration`.`id`) from `student_registration` where (`student_registration`.`index_no` = `biodata`.`index_no`)) AS `total_jobs_applied` from (((((`student_biodata` `biodata` left join `settings_programmes` `course` on((`course`.`code` = `biodata`.`programme_code`))) left join `student_secondary_education` `shs_education` on((`biodata`.`id` = `shs_education`.`id`))) left join `student_latest_job` `job` on((`biodata`.`id` = `job`.`student_id`))) left join `student_registration` `regis` on((`biodata`.`index_no` = `regis`.`index_no`))) left join `users` on((`biodata`.`id` = `users`.`student_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_student_job_applications`
--
DROP TABLE IF EXISTS `vw_student_job_applications`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_student_job_applications`  AS  select `a`.`id` AS `id`,`a`.`student_id` AS `student_id`,`a`.`job_post_id` AS `job_post_id`,`a`.`status` AS `status`,`a`.`date_created` AS `date_created`,`b`.`fullname` AS `fullname`,`b`.`profile_picture` AS `profile_picture`,`b`.`programme_name` AS `programme_name` from (`student_job_applications` `a` left join `vw_student_details` `b` on((`a`.`student_id` = `b`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_alerts`
--
ALTER TABLE `email_alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_postings`
--
ALTER TABLE `job_postings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_subscribers`
--
ALTER TABLE `news_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_courses`
--
ALTER TABLE `settings_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_experiences`
--
ALTER TABLE `settings_experiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_faculties`
--
ALTER TABLE `settings_faculties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_interest_category`
--
ALTER TABLE `settings_interest_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_posting_type`
--
ALTER TABLE `settings_posting_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_programmes`
--
ALTER TABLE `settings_programmes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_progrogramme_session`
--
ALTER TABLE `settings_progrogramme_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signup_approval_requests`
--
ALTER TABLE `signup_approval_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_biodata`
--
ALTER TABLE `student_biodata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_careerinfo`
--
ALTER TABLE `student_careerinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_job_applications`
--
ALTER TABLE `student_job_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_latest_job`
--
ALTER TABLE `student_latest_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_registration`
--
ALTER TABLE `student_registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_no` (`index_no`);

--
-- Indexes for table `student_secondary_education`
--
ALTER TABLE `student_secondary_education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `email_alerts`
--
ALTER TABLE `email_alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_postings`
--
ALTER TABLE `job_postings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_subscribers`
--
ALTER TABLE `news_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings_courses`
--
ALTER TABLE `settings_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings_experiences`
--
ALTER TABLE `settings_experiences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings_programmes`
--
ALTER TABLE `settings_programmes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_biodata`
--
ALTER TABLE `student_biodata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_careerinfo`
--
ALTER TABLE `student_careerinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_job_applications`
--
ALTER TABLE `student_job_applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_latest_job`
--
ALTER TABLE `student_latest_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_registration`
--
ALTER TABLE `student_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_secondary_education`
--
ALTER TABLE `student_secondary_education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Contains the Auto Generated ID for the User', AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
