	<?php 
		include_once("inc/config/config.inc.php");
		
		if(@$_SESSION['user']['id']) {
			session_start();
			header("location:".BASE_URL);
			exit;
		} 

		include_once("header.php"); 
		include_once("inc/controllers/signup.php");
	?>

	<section class="job-bg user-page">
		<div class="container">
			<div class="row text-center">
				<!-- user-login -->			
				<div class="col-sm-8 col-sm-offset-1 col-md-10 col-md-offset-1">
					<div class="user-account job-user-account">
						
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#register_student" aria-controls="find-job" role="tab" data-toggle="tab"><i class="fa fa-users"></i> Student</a></li>
								<li role="presentation"><a href="#company" aria-controls="post-job" role="tab" data-toggle="tab"><i class="fa fa-building"></i> Company</a></li>
							</ul>

							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="register_student">
									<form action="<?=BASE_URL?>inc/controllers/save_student.php" method="post" enctype="multipart/form-data">
										<div class="col-md-6 form-group" style="padding-left: 0px">
											<input type="email" class="form-control" placeholder="Your Email" name="email" required>
										</div>
										<div class="col-md-6 form-group">
											<input  type="text" class="form-control" placeholder="Your Student Number" name="index_no" required>
										</div>
										<!-- <div class="col-md-6 form-group" style="padding-left: 0px">
											<select class="form-control input-lg" name="program_of_study" required>
												<option label="Select Programme Of Study"></option>
												<?php if(!empty($programme_list)) : foreach ($programme_list as $program) :?>
												<option value="<?=$program->id?>"><?=$program->name?></option>
												<?php endforeach; endif; ?>
											</select>
										</div> -->
										<div class="col-md-6 form-group" style="padding-left: 0px">
											<select class="form-control input-lg specializations" multiple="multiple" name="specialization[]" required>
                        <option label="Select Interest(s)"></option>
                        <?php if(!empty($specialization_list)) : foreach ($specialization_list as $interest) :?>
												<option value="<?=$interest->id?>"><?=$interest->name?></option>
												<?php endforeach; endif; ?>
                      </select>
										</div>
										<div class="col-md-6 form-group">
											<input type="file" class="form-control" placeholder="" name="profile_pc" title=" " required>
										</div>
										<div class="col-md-6 form-group" style="padding-left: 0px">
											<input type="password" class="form-control" placeholder="Enter Your Password" name="password" required>
										</div>
										<div class="col-md-6 form-group">
											<input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" required>
										</div>
										<div class="col-md-6 form-group">
											<div class="checkbox">
												<label class="pull-left checked" for="signing"><input type="checkbox" required name="tnc" checked id="signing"> I agree to our Terms and Conditions </label>
											</div><!-- checkbox -->	
										</div>
										<div class="col-md-6 form-group">
											<button type="submit" class="btn" name="register_student">Register</button>	
										</div>
									</form>
								</div>
								<div role="tabpanel" class="tab-pane" id="company">
									<form action="<?=BASE_URL?>inc/controllers/save_company.php" method="post" enctype="multipart/form-data">
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="company_name" class="form-control" placeholder="Company Name"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="tagline" class="form-control" placeholder="Motto / Slogan"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="email" class="form-control" placeholder="Email Address"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="location" class="form-control" placeholder="Residence Address"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="postal_address" class="form-control" placeholder="Postal Address"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="phone_num_1" class="form-control" placeholder="Primary Tel #"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="phone_num_2" class="form-control" placeholder="Secondary Tel #"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="text" name="website" class="form-control" placeholder="website"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="password" name="password" class="form-control" placeholder="Password">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
											</div>
										</div>
										<div class="col-md-6 form-group">
											<select class="form-control input-lg" name="specialization" required>
                        <option label="Select Field"></option>
                        <?php if(!empty($specialization_list)) : foreach ($specialization_list as $interest) :?>
												<option value="<?=$interest->id?>"><?=$interest->name?></option>
												<?php endforeach; endif; ?>
                      </select>
										</div>
										<div class="col-md-6">
												<label>Upload Company Logo</label>
											<div class="form-group">
												<input type="file" name="company_logo" class="form-control">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea rows="7" class="form-control" name="about_us" placeholder="About Company"></textarea>
											</div>
										</div>
										<div class="checkbox">
											<label class="pull-left checked" for="signing-2"><input type="checkbox" name="signing-2" id="signing-2">By signing up for an account you agree to our Terms and Conditions</label>
										</div><!-- checkbox -->	
										<button type="submit" class="btn" name="register_company" value="submit">Register</button>	
									</form>
								</div>
							</div>				
					</div>
				</div><!-- user-login -->			
			</div><!-- row -->	
		</div><!-- container -->
	</section><!-- signup-page -->

	<?php include("footer.php"); ?>