	<?php 
		include_once("header.php"); 
		include_once("inc/controllers/index_page.php");
	?>

	<div class="banner-job">
		<div class="banner-overlay"></div>
		<div class="container text-center">
			<h1 class="title">The Easiest Way to Get Your Intern Job</h1>
			<h3>Intern jobs available for vacation periods</h3>
			<div class="banner-form">
				<form action="<?=BASE_URL?>search.php" method="get">
					<input type="text" class="form-control" placeholder="Type your key word" name="keyword">
					<div class="dropdown category-dropdown">						
						<a data-toggle="dropdown" href="#"><span class="change-text">Location </span> <i class="fa fa-angle-down"></i></a>
						<ul class="dropdown-menu category-change">
							<li><a href="#" class="region-dropdown" data-region="gar">Greater Accra</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Central Region</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Western Region</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Eastern Region</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Volta Region</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Ashanti Region</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Brong-Ahafo </a></li>
							<li><a href="#" class="region-dropdown" data-region="">Nothern Region</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Upper East</a></li>
							<li><a href="#" class="region-dropdown" data-region="">Upper West</a></li>
						</ul>								
					</div><!-- category-change -->
					<input id="region_selected" type="hidden" name="location" value="">
					<button type="submit" class="btn btn-primary" value="Search">Search</button>
				</form>
			</div><!-- banner-form -->
			
			<ul class="banner-socail list-inline">
				<li><a target="_blank" href="https://www.facebook.com/login.php?skip_api_login=1&api_key=966242223397117&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fsharer%2Fsharer.php%3Fu%3Dhttp%253A%252F%252Fsite.gtuc.edu.gh%252Fadmission-requirements-bsc-in-information-technology%252F&cancel_url=https%3A%2F%2Fwww.facebook.com%2Fdialog%2Freturn%2Fclose%3Ferror_code%3D4201%26error_message%3DUser%2Bcanceled%2Bthe%2BDialog%2Bflow%23_%3D_&display=popup&locale=en_US" title="Facebook"><i class="fa fa-facebook"></i></a></li>
				<li><a target="_blank" href="https://twitter.com/intent/tweet?text=Admission%20requirements%20BSc%20in%20Information%20Technology&url=http%3A%2F%2Fsite.gtuc.edu.gh%2Fadmission-requirements-bsc-in-information-technology%2F&original_referer=http%3A%2F%2Fsite.gtuc.edu.gh%2Fadmission-requirements-bsc-in-information-technology%2F" title="Twitter"><i class="fa fa-twitter"></i></a></li>
				<li><a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fsite.gtuc.edu.gh%2Fadmission-requirements-bsc-in-information-technology%2F" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
				<li><a target="_blank" href="https://www.linkedin.com/start/join?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fsharing%2Fshare-offsite%3Fmini%3Dtrue%26url%3Dhttp%253A%252F%252Fsite.gtuc.edu.gh%252Fadmission-requirements-bsc-in-information-technology%252F%26title%3DAdmission%2Brequirements%2BBSc%2Bin%2BInformation%2BTechnology%26source%3DGhana%2BTechnology%2BUniversity%2BCollege" title="linkedin"><i class="fa fa-linkedin"></i></a></li>
			</ul><!-- banner-socail -->
		</div><!-- container -->
	</div><!-- banner-section -->

	<div class="page">
		<div class="container">
			<div class="section category-items job-category-items  text-center">
				<ul class="category-list">	
					<?php if(!empty($total_all_interests)) : foreach($total_all_interests as $category) : ?>
					<li class="category-item">
						<a href="<?=BASE_URL?>search.php/?cat=<?=$category->id?>">
							<div class="category-icon"><img src="<?=BASE_URL."assets/images/icon/".$category->icon?>" alt="images" class="img-responsive"></div>
							<span class="category-title"><?=$category->name?></span>
							<span class="category-quantity">(<?=$category->total_post?>)</span>
						</a>
					</li><!-- category-item -->
					<?php endforeach; endif; ?>				
				</ul>				
			</div><!-- category ad -->			

			<div class="section latest-jobs-ads">
				<div class="section-title tab-manu">
					<h4>Recent Intern Jobs</h4>
					 <!-- Nav tabs -->      
					<ul class="nav nav-tabs" role="tablist">
						<!-- <li role="presentation"><a href="#hot-jobs" data-toggle="tab">Hot Jobs</a></li> -->
						<li role="presentation" class="active"><a href="#recent-jobs" data-toggle="tab">Recent Intern Jobs</a></li>
						<!-- <li role="presentation"><a href="#recent-jobs" data-toggle="tab">Popular Jobs</a></li> -->
					</ul>
				</div>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="recent-jobs">
						<?php if(!empty($recent_job_posts)): foreach ($recent_job_posts as $job_post) : ?>
						<div class="job-ad-item">
							<div class="item-info">
								<div class="item-image-box">
									<div class="item-image">
										<a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>"><img src="<?=BASE_URL.'profiles/'.$job_post->company_logo?>" alt="Image" class="img-responsive"></a>
									</div><!-- item-image -->
								</div>

								<div class="ad-info">
									<span><a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>" class="title"><?=ucwords($job_post->job_title)?></a> @ <a class="prevent-click" href="#"><?=ucwords($job_post->company_name)?></a></span>
									<div class="ad-meta">
										<ul>
											<li><a class="prevent-click" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?=ucwords($job_post->location." - ".$job_post->region)?> </a></li>
											<li><a class="prevent-click" href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?=ucwords($job_post->duration)?></a></li>
											<li><a class="prevent-click" href="#"><i class="fa fa-money" aria-hidden="true"></i><?=ucwords("GHC ".$job_post->salary_expectation)?></a></li>
											<li><a class="prevent-click" href="#"><i class="fa fa-tags" aria-hidden="true"></i><?=ucwords($job_post->interest_category_name)?></a></li>
										</ul>
									</div><!-- ad-meta -->									
								</div><!-- ad-info -->
								<div class="button">
									<!-- <a href="<?=BASE_URL.'company/edit-intern-job.php?d='.$job_post->id?>" class="btn btn-primary">Edit Info</a> -->
									<a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>" class="btn btn-primary">View Details</a>
								</div>
							</div><!-- item-info -->
						</div><!-- ad-item -->
						<?php endforeach; endif; ?>		
					</div><!-- tab-pane -->
				</div><!-- tab-content -->
			</div><!-- trending ads -->		

			<div class="section cta cta-two text-center">
				<div class="row">
					<div class="col-sm-4">
						<div class="single-cta">
							<div class="cta-icon icon-jobs">
								<img src="assets/images/icon/31.png" alt="Icon" class="img-responsive">
							</div><!-- cta-icon -->
							<h3><?=$total_jobs?></h3>
							<h4>New Intern Jobs</h4>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p> -->
						</div>
					</div><!-- single-cta -->

					<div class="col-sm-4">
						<div class="single-cta">
							<!-- cta-icon -->
							<div class="cta-icon icon-company">
								<img src="assets/images/icon/32.png" alt="Icon" class="img-responsive">
							</div><!-- cta-icon -->
							<h3><?=$total_companies?></h3>
							<h4>Total Company</h4>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p> -->
						</div>
					</div><!-- single-cta -->

					<div class="col-sm-4">
						<div class="single-cta">
							<div class="cta-icon icon-candidate">
								<img src="assets/images/icon/33.png" alt="Icon" class="img-responsive">
							</div><!-- cta-icon -->
							<h3><?=$total_students?></h3>
							<h4>Students Hooked</h4>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p> -->
						</div>
					</div><!-- single-cta -->
				</div><!-- row -->
			</div><!-- cta -->			

		</div><!-- conainer -->
	</div><!-- page -->
	
	<?php include("footer.php"); ?>