	<?php 
		include_once("inc/config/config.inc.php");
		
		if(!isset($_SESSION['user']['id']) && $_SESSION['user']['user_type'] != "student") {
			session_start();
			header("location:".BASE_URL);
			exit;
		} 

		include_once("header.php"); 
		include_once("inc/controllers/signup.php");
	?>

	<section class="job-bg user-page">
		<div class="container">
			<div class="row text-center">
				<!-- user-login -->			
				<div class="col-sm-10 col-md-11">
					<div class="user-account job-user-account">
						<form action="<?=BASE_URL?>inc/controllers/save_student.php" method="post" enctype="multipart/form-data" style="min-height: 400px">
							
							<div class="col-md-6 form-group" style="padding-left: 0px">
								<select class="form-control input-lg" name="specialization" required>
                  <option label="Select Interest(s)"></option>
                  <?php if(!empty($specialization_list)) : foreach ($specialization_list as $interest) :?>
									<option value="<?=$interest->id?>"><?=$interest->name?></option>
									<?php endforeach; endif; ?>
                </select>
							</div>
							<!-- <div class="col-md-6 form-group" style="padding-left: 0px">
								<select class="form-control input-lg"  name="company">
                  <option label="Select Company"></option>
                  <?php if(!empty($company_list)) : foreach ($company_list as $company) :?>
									<option value="<?=$company->id?>"><?=$company->name?></option>
									<?php endforeach; endif; ?>
                </select>
							</div>
							<div class="col-md-6 form-group">
								<div class="checkbox">
									<label class="pull-left checked" for="signing"><input type="checkbox" required name="tnc" checked id="signing"> I agree to our Terms and Conditions </label>
								</div>
							</div> -->
							<div class="col-md-6 form-group">
								<button type="submit" class="btn pull-left" name="email_alert">Submit</button>	
							</div>
						</form>			
					</div>
				</div><!-- user-login -->			
			</div><!-- row -->	
		</div><!-- container -->
	</section><!-- signup-page -->

	<?php include("footer.php"); ?>