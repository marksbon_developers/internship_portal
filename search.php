<?php

	include_once("inc/config/config.inc.php");

	if(isset($_GET['cat'])) 
		$category_search_id = urldecode($_GET['cat']);

	elseif(isset($_GET['keyword']) || isset($_GET['location'])) {
		$keyword = urldecode($_GET['keyword']);
		$location = urldecode($_GET['location']);
	}
	
	else 
		header("location: ".BASE_URL);

	include_once("header.php");  
	include_once("inc/controllers/search_job.php");  
?>
	
	<section class="job-bg page job-list-page">
		<div class="container">
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li><?=(@$search_result[0]->interest_category_name) ? @$search_result[0]->interest_category_name : "No Record Found" ?></li>
				</ol><!-- breadcrumb -->						
				<h2 class="title"><?=(@$search_result[0]->interest_category_name) ? @$search_result[0]->interest_category_name : "No Record Found" ?></h2>
			</div>
			<div class="category-info">	
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<div class="accordion">
							<!-- panel-group -->
							<div class="panel-group" id="accordion">
								<!-- panel -->
								<div class="panel panel-default panel-faq">
									<!-- panel-heading -->
									<div class="panel-heading">
										<div  class="panel-title">
											<a class="prevent-click">
												<!-- data-toggle="collapse" data-parent="#accordion" href="#accordion-one" -->
												<h4>Categories<!-- <span class="pull-right"><i class="fa fa-minus"></i></span> --></h4>
											</a>
										</div>
									</div><!-- panel-heading -->

									<div id="accordion-one" class="panel-collapse collapse in">
										<!-- panel-body -->
										<div class="panel-body">
											<h5><a href="<?=BASE_URL?>"><i class="fa fa-caret-down"></i> All Categories</a></h5>
											<ul>
												<?php if(!empty($all_categories)): foreach($all_categories as $category): ?>
												<li><a href="<?=BASE_URL?>search.php/?cat=<?=$category->id?>"><img src="<?=BASE_URL."assets/images/icon/".$category->icon?>" alt="images" class="img-responsive" style="width: 20px;float:  left;"> &nbsp; <?=$category->name?>(<?=$category->total_post?>)</a></li>
											<?php endforeach; endif; ?>
											</ul>
											<!-- <div class="see-more">
												<button type="button" class="show-more one"><i class="fa fa-plus-square-o" aria-hidden="true"></i>See More</button>
												<ul class="more-category one">
													<li><a href="#">Fron end developer<span>(289)</span></a></li>
													<li><a href="#">Back end developer<span>(5402)</span></a></li>
													<li><a href="#">IT Department Manager<span>(3829)</span></a></li>
													<li><a href="#">QA Department Manager<span>(352)</span></a></li>
												</ul>
											</div> -->

										</div><!-- panel-body -->
									</div>
								</div><!-- panel -->
							 </div><!-- panel-group -->
						</div>
					</div><!-- accordion-->

					<!-- recommended-ads -->
					<div class="col-sm-8 col-md-9">				
						<div class="section job-list-item">
							<div class="featured-top">
								<!-- <h4>Showing 1-25 of 65,712 ads</h4> -->
								<!-- <div class="dropdown pull-right">
									<div class="dropdown category-dropdown">
										<h5>Sort by:</h5>						
										<a data-toggle="dropdown" href="#"><span class="change-text">Most Relevant</span><i class="fa fa-caret-square-o-down"></i></a>
										<ul class="dropdown-menu category-change">
											<li><a href="#">Most Relevant</a></li>
											<li><a href="#">Most Popular</a></li>
										</ul>								
									</div>	
								</div> -->							
							</div><!-- featured-top -->	

							<?php if(!empty($search_result)): foreach ($search_result as $job_post) : ?>
							<div class="job-ad-item">
								<div class="item-info">
									<div class="item-image-box">
										<div class="item-image">
											<a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>"><img src="<?=BASE_URL.'profiles/'.$job_post->company_logo?>" alt="Image" class="img-responsive"></a>
										</div><!-- item-image -->
									</div>

									<div class="ad-info">
										<span><a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>" class="title"><?=ucwords($job_post->job_title)?></a> @ <a class="prevent-click" href="#"><?=ucwords($job_post->company_name)?></a></span>
										<div class="ad-meta">
											<ul>
												<li><a class="prevent-click" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?=ucwords($job_post->location." - ".$job_post->region)?> </a></li>
												<li><a class="prevent-click" href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?=ucwords($job_post->duration)?></a></li>
												<li><a class="prevent-click" href="#"><i class="fa fa-money" aria-hidden="true"></i><?=ucwords("GHC ".$job_post->salary_expectation)?></a></li>
												<li><a class="prevent-click" href="#"><i class="fa fa-tags" aria-hidden="true"></i><?=ucwords($job_post->interest_category_name)?></a></li>
											</ul>
										</div><!-- ad-meta -->									
									</div><!-- ad-info -->
									<div class="button">
										<!-- <a href="<?=BASE_URL.'company/edit-intern-job.php?d='.$job_post->id?>" class="btn btn-primary">Edit Info</a> -->
										<a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>" class="btn btn-primary">View Details</a>
									</div>
								</div><!-- item-info -->
							</div><!-- ad-item -->
							<?php endforeach; else : print "<h2>No Intern Jobs Found</h2>"; endif; ?>												
						</div>
					</div><!-- recommended-ads -->
				</div>	
			</div>
		</div><!-- container -->
	</section><!-- main -->
	
<?php	include_once("footer.php");  ?>