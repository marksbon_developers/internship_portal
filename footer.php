
  <!-- footer -->
  <footer id="footer" class="clearfix">
    <!-- footer-top -->
    <section class="footer-top clearfix">
      <div class="container">
        <div class="row">
          <!-- footer-widget -->
          <div class="col-sm-3">
            <div class="footer-widget">
              <h3>Intern Job Seekers</h3>
              <ul>
                <!-- <li><a href="#">Browse Intern Jobs</a></li> -->
                <li><a href="<?=BASE_URL?>alerts.php">Email Alerts</a></li>
                <li><a href="#">SMS Alerts</a></li>
              </ul>
            </div>
          </div><!-- footer-widget -->

          <!-- footer-widget -->
          <div class="col-sm-3">
            <div class="footer-widget">
              <h3>Career Resources</h3>
              <ul>
                <li><a target="_blank" href="https://theinterviewguys.com/top-10-job-interview-questions/">Interview Tips</a></li>
                <li><a target="_blank" href="http://site.gtuc.edu.gh/partners-and-affiliates/">International Partners</a></li>
                <li><a target="_blank" href="http://site.gtuc.edu.gh/students-faqs/">FAQ</a></li>
              </ul>
            </div>
          </div><!-- footer-widget -->

          <!-- footer-widget -->
          <div class="col-sm-3">
            <div class="footer-widget social-widget">
              <h3>Connect with us</h3>
              <ul>
                <li><a target="_blank" href="https://www.facebook.com/login.php?skip_api_login=1&api_key=966242223397117&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fsharer%2Fsharer.php%3Fu%3Dhttp%253A%252F%252Fsite.gtuc.edu.gh%252Fadmission-requirements-bsc-in-information-technology%252F&cancel_url=https%3A%2F%2Fwww.facebook.com%2Fdialog%2Freturn%2Fclose%3Ferror_code%3D4201%26error_message%3DUser%2Bcanceled%2Bthe%2BDialog%2Bflow%23_%3D_&display=popup&locale=en_US"><i class="fa fa-facebook-official"></i>Facebook</a></li>
                <li><a target="_blank" href="https://twitter.com/intent/tweet?text=Admission%20requirements%20BSc%20in%20Information%20Technology&url=http%3A%2F%2Fsite.gtuc.edu.gh%2Fadmission-requirements-bsc-in-information-technology%2F&original_referer=http%3A%2F%2Fsite.gtuc.edu.gh%2Fadmission-requirements-bsc-in-information-technology%2F"><i class="fa fa-twitter-square"></i>Twitter</a></li>
                <li><a target="_blank" href="https://plus.google.com/share?url=http%3A%2F%2Fsite.gtuc.edu.gh%2Fadmission-requirements-bsc-in-information-technology%2F"><i class="fa fa-google-plus-square"></i>Google+</a></li>
                <li><a target="_blank" href="https://www.linkedin.com/start/join?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fsharing%2Fshare-offsite%3Fmini%3Dtrue%26url%3Dhttp%253A%252F%252Fsite.gtuc.edu.gh%252Fadmission-requirements-bsc-in-information-technology%252F%26title%3DAdmission%2Brequirements%2BBSc%2Bin%2BInformation%2BTechnology%26source%3DGhana%2BTechnology%2BUniversity%2BCollege"><i class="fa fa-linkedin"></i>LinkedIN</a></li>
              </ul>
            </div>
          </div><!-- footer-widget -->

          <div class="col-sm-3">
            <div class="footer-widget social-widget">
              <h3>Contact Us</h3>
              <ul>
                <li>+233 (0) 302 22 1446</li>
              </ul>
            </div>
          </div>

          <!-- footer-widget -->
          <div class="col-sm-3">
            <div class="footer-widget news-letter"><br/>
              <h4>Subscribe to Our Newsletters!</h4>
              <!-- form -->
              <form action="<?=BASE_URL?>inc/controllers/apply_job.php" method="post">
                <input type="email" class="form-control" name="email" placeholder="Enter Email Address">
                <button type="submit" class="btn btn-primary" name="subscribe">Sign Up</button>
              </form><!-- form -->
            </div>
          </div><!-- footer-widget -->
        </div><!-- row -->
      </div><!-- container -->
    </section><!-- footer-top -->

    <div class="footer-bottom clearfix text-center">
      <div class="container">
        <p>Copyright &copy; GTUC Intership Portal 2018.</a></p>
      </div>
    </div><!-- footer-bottom -->
  </footer><!-- footer -->

    <!-- JS -->
    <script src="<?=BASE_URL?>assets/js/jquery.min.js"></script>
    <script src="<?=BASE_URL?>assets/js/bootstrap.min.js"></script>
    <script src="<?=BASE_URL?>assets/js/price-range.js"></script>   
    <script src="<?=BASE_URL?>assets/js/main.js"></script>
    <script src="<?=BASE_URL?>assets/js/switcher.js"></script>
    <!-- multiple select button -->
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/fSelect.js"></script>


    <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-73239902-1', 'auto');
    ga('send', 'pageview');

    $('.region-dropdown').on('click',function(){
        $('#region_selected').val((this.text));
    });

    $('.prevent-click').on('click',function(e){
        e.preventDefault();
    });

    $('.specializations').fSelect({
      showSearch: false,
      placeholder: "Select Specialization(s)"
    });
  </script> 

  <!-- Notification -->
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/notify.js"></script>
    <script type="text/javascript">
      <?php if(isset($_SESSION['success'])) : ?>
      $.notify("<?=$_SESSION['success']?>", "success");
      <?php elseif(isset($_SESSION['error'])) : ?>
      $.notify("<?=$_SESSION['error']?>", "error");
      <?php endif; unset($_SESSION['success'],$_SESSION['error'])?>
    </script>
  <!-- Notification -->
  </body>

</html>