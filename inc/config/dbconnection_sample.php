<?php
	
	class DB_Connection {

		public $dbconnection;
		private $DB_SERVER = 'localhost';
		private $DB_USERNAME = 'root';
		private $DB_PASSWORD = 'mypassword';
		private $DB_NAME = 'test';

		public function __construct(){
			
			$db_connection = mysqli_connect($this->DB_SERVER,$this->DB_USERNAME,$this->DB_PASSWORD,$this->DB_NAME);

			if(!$db_connection) {
		    $_SESSION['error'] = str_replace(array("\r","\n"), array("\n"), @mysqli_connect_error());
			}
			else
				$this->dbconnection = $db_connection;
		}
	}
