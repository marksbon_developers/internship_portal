-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 06:05 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `internship_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `blobs`
--

CREATE TABLE `blobs` (
  `id` int(11) NOT NULL,
  `mime_type` varchar(255) NOT NULL,
  `file_size` varchar(255) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `uploader_type` enum('student','company','admin') NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `company_tagline` varchar(150) NOT NULL,
  `categorys` text NOT NULL,
  `profile_summary` text NOT NULL,
  `full_description` text NOT NULL,
  `num_of_employees` varchar(30) NOT NULL,
  `residence_address` varchar(100) NOT NULL,
  `postal_address` varchar(100) NOT NULL,
  `telephone_1` varchar(20) NOT NULL,
  `telephone_2` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `website` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `logo_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `name`, `company_tagline`, `categorys`, `profile_summary`, `full_description`, `num_of_employees`, `residence_address`, `postal_address`, `telephone_1`, `telephone_2`, `email`, `website`, `contact_person`, `logo_id`, `status`, `date_created`) VALUES
(15, '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, 'active', '2018-02-15 15:58:40'),
(16, '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, 'active', '2018-02-15 15:59:42'),
(17, '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, 'active', '2018-02-15 16:23:07'),
(18, '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, 'active', '2018-02-15 16:32:36'),
(19, 'name', 'tag', '0', 'summary', 'about', '', 'residence', 'postal', '345678', '456789', 'test@loo.com', 'website', 'ceo', 0, 'active', '2018-02-15 18:52:59'),
(20, 'Alisa Hotel', 'Guaranteeing the best &amp; luxurious accommadation ever', '0', 'This is the best company here', 'this is about the company&amp;nbsp;', '', 'that same place nooorrrr', 'Postal Address', '3456789', '23456789', 'info@alisahotel.com', 'www.alisahotel.com', 'Mr. Kingsley Mintah', 0, 'active', '2018-02-22 00:30:02'),
(21, 'Test Company 3', 'Tagline', '0', 'ABOUT COMPANY', 'ABOUT COMPANY', '', 'location', 'postal', '23456789', '3456789', 'osborne.mordred@gmai', 'www.alisahotel.com', 'CEO NAME', 0, 'active', '2018-02-23 08:47:31'),
(22, 'Marriot Hotel', 'Tagline', '0', 'About Compau', 'kdfkdfnunvdfv', '', '23rd Lane , Stanbic Heights', 'Box AK 1007, Airport', '023456789', '034567890', 'info@marriot.com', 'www.marriot.com', 'Mr. Kingsley Mintah', 0, 'active', '2018-03-01 06:03:45');

-- --------------------------------------------------------

--
-- Table structure for table `company_interest_categories`
--

CREATE TABLE `company_interest_categories` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `interest_category` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `company_signup_requests`
--

CREATE TABLE `company_signup_requests` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_signup_requests`
--

INSERT INTO `company_signup_requests` (`id`, `company_id`, `status`, `date_created`) VALUES
(1, 20, 'declined', '2018-02-23 08:47:31'),
(2, 17, 'approved', '2018-03-01 06:03:46');

-- --------------------------------------------------------

--
-- Table structure for table `company_socialmedia_links`
--

CREATE TABLE `company_socialmedia_links` (
  `id` int(11) NOT NULL,
  `company_info_id` int(11) NOT NULL,
  `facebook` text NOT NULL,
  `linked_in` text NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_socialmedia_links`
--

INSERT INTO `company_socialmedia_links` (`id`, `company_info_id`, `facebook`, `linked_in`, `status`, `date_created`) VALUES
(10, 15, '', '', 'active', '2018-02-15 15:58:40'),
(11, 16, '', '', 'active', '2018-02-15 15:59:42'),
(12, 17, '', '', 'active', '2018-02-15 16:23:07'),
(13, 18, '', '', 'active', '2018-02-15 16:32:36'),
(14, 19, 'facebook', 'llinked', 'active', '2018-02-15 18:52:59'),
(15, 20, '', '', 'active', '2018-02-22 00:30:02'),
(16, 21, 'facebook', 'linkedin', 'active', '2018-02-23 08:47:31'),
(17, 22, '', '', 'active', '2018-03-01 06:03:45');

-- --------------------------------------------------------

--
-- Table structure for table `job_postings`
--

CREATE TABLE `job_postings` (
  `id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `contract_type` enum('intern','contract','full time') NOT NULL DEFAULT 'intern',
  `years_of_experience` tinyint(2) NOT NULL,
  `salary_expectation` varchar(50) NOT NULL,
  `responsibilities` text NOT NULL,
  `skill_requirement` text NOT NULL,
  `qualification` text NOT NULL,
  `duration` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `geolocation` varchar(50) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `approved_by` int(11) NOT NULL,
  `approved_date` datetime NOT NULL,
  `posting_type_id` tinyint(2) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `job_postings`
--

INSERT INTO `job_postings` (`id`, `job_title`, `company_id`, `contract_type`, `years_of_experience`, `salary_expectation`, `responsibilities`, `skill_requirement`, `qualification`, `duration`, `location`, `geolocation`, `status`, `approved_by`, `approved_date`, `posting_type_id`, `date_created`) VALUES
(1, 'Senior Hotel Manager', 20, 'intern', 0, 'GHC 700 / Month', 'Blah Blah Blah Blah Blah Blah Blah Blah', 'Blah BlahBlah Blah Blah BlahBlah Blah', 'Computer Science/ Electronic / Electrials', '2 Months', 'Abelemkpe - Village Inn', '', 'declined', 0, '2018-02-24 00:00:00', 0, '2018-02-23 16:14:12'),
(2, 'FrontEnd Developers', 21, 'intern', 0, '550', 'No heavy responsibilities', 'Knowledgeable, Smart and ready to learn new things', 'HND / Degree Computer SCience/Engineering/ Electronic/Electrials Engineering', '3 months', 'abavana', '', 'approved', 0, '0000-00-00 00:00:00', 0, '2018-02-25 15:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `settings_faculties`
--

CREATE TABLE `settings_faculties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `programme_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_interest_category`
--

CREATE TABLE `settings_interest_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_posting_type`
--

CREATE TABLE `settings_posting_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `duration` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings_programmes`
--

CREATE TABLE `settings_programmes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `signup_approval_requests`
--

CREATE TABLE `signup_approval_requests` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup_approval_requests`
--

INSERT INTO `signup_approval_requests` (`id`, `student_id`, `company_id`, `status`, `date_created`) VALUES
(1, 0, 20, 'pending', '2018-02-23 08:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `student_biodata`
--

CREATE TABLE `student_biodata` (
  `id` int(11) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `other_names` varchar(100) NOT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `marital_status` varchar(50) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `postal_address` text,
  `user_id` int(11) DEFAULT NULL,
  `photo_id` int(11) DEFAULT NULL,
  `resume_id` tinyint(4) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `student_category_interests`
--

CREATE TABLE `student_category_interests` (
  `id` int(11) NOT NULL,
  `biodata_id` int(11) NOT NULL,
  `interest_category_id` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `student_contact_info`
--

CREATE TABLE `student_contact_info` (
  `id` bigint(20) NOT NULL,
  `biodata_id` bigint(20) NOT NULL,
  `phone_number_1` varchar(25) CHARACTER SET utf8 NOT NULL,
  `phone_number_2` varchar(25) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `residence` text CHARACTER SET utf8 NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `student_contact_info`
--

INSERT INTO `student_contact_info` (`id`, `biodata_id`, `phone_number_1`, `phone_number_2`, `email`, `residence`, `date_created`) VALUES
(1, 1, '0244444444', '0544444444', 'Claude@africaloop.com', 'Hse  No 12, Trade Fair Function.', '2018-01-09 11:48:33'),
(2, 3, '0266666666', '0566666666', 'Daniella@gmailcom', 'Kasoa - Overhead', '2018-01-11 13:06:57');

-- --------------------------------------------------------

--
-- Table structure for table `student_job_applications`
--

CREATE TABLE `student_job_applications` (
  `id` int(11) NOT NULL,
  `biodata_id` int(11) NOT NULL,
  `job_post_id` int(11) NOT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_school_info`
--

CREATE TABLE `student_school_info` (
  `id` int(11) NOT NULL,
  `biodata_id` int(11) NOT NULL,
  `student_id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `session_id` tinyint(4) NOT NULL,
  `department_id` tinyint(2) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `student_school_info`
--

INSERT INTO `student_school_info` (`id`, `biodata_id`, `student_id`, `session_id`, `department_id`, `date_created`) VALUES
(1, 1, '04TU916042', 0, 1, '2018-01-09 11:48:33'),
(2, 3, '04TU916043', 0, 1, '2018-01-11 13:06:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Contains the Auto Generated ID for the User',
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL COMMENT 'Contains the Login Username of User',
  `password` varchar(100) NOT NULL COMMENT 'Contains the Login Passwordof User',
  `student_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `login_url` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted','') NOT NULL DEFAULT 'inactive' COMMENT 'Contains account info status',
  `created_by` varchar(50) NOT NULL COMMENT 'Contains the Employee ID of the Creator',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Contains the date and time the user was created'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `student_id`, `company_id`, `login_url`, `status`, `created_by`, `date_created`) VALUES
(1, 'Osborne', 'Mordreds', 'sysadmin', '$2y$10$GuOFXrr8Xdd5JFHD9vzm8.tUeafbhkUfvImwdDkswS8NJJOqzV3BC', 1, 0, 'admin/home.php', 'active', '', '2017-05-25 06:05:10'),
(2, 'Bismark', 'Offei', 'boffei', '$2y$10$GuOFXrr8Xdd5JFHD9vzm8.tUeafbhkUfvImwdDkswS8NJJOqzV3BC', 0, 0, '', 'inactive', '2345', '2017-05-25 06:05:10'),
(4, 'Test Company 3', '', 'osborne.mordred@gmai', '$2y$10$1xFtGUL/armQsy6uaO6IPu0JnoQB2b3iMKCOFOZqEUOlBO9BXf21.', 0, 16, 'company/dashboard.php', 'inactive', '', '2018-02-23 08:47:31'),
(3, '', '', 'info@alisahotel.com', 'sysadmin', 0, 15, '', 'inactive', '', '2018-02-22 00:30:02'),
(5, 'Marriot Hotel', '', 'info@marriot.com', '$2y$10$b4rLTKryKqFm4vihy/Dw0uxV4GamuNoPOhbe7ziA7vOFlWjJdQIjO', 0, 17, 'company/dashboard.php', 'inactive', '', '2018-03-01 06:03:46');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_company_sighup_approvals`
-- (See below for the actual view)
--
CREATE TABLE `vw_company_sighup_approvals` (
`id` int(11)
,`company_id` int(11)
,`status` enum('pending','approved','declined')
,`date_created` datetime
,`company_name` varchar(150)
,`location` varchar(100)
,`interests` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_jobposting`
-- (See below for the actual view)
--
CREATE TABLE `vw_jobposting` (
`id` int(11)
,`job_title` varchar(255)
,`company_id` int(11)
,`contract_type` enum('intern','contract','full time')
,`years_of_experience` tinyint(2)
,`salary_expectation` varchar(50)
,`responsibilities` text
,`skill_requirement` text
,`qualification` text
,`duration` text
,`location` varchar(255)
,`geolocation` varchar(50)
,`status` enum('pending','approved','declined')
,`approved_by` int(11)
,`approved_date` datetime
,`posting_type_id` tinyint(2)
,`date_created` datetime
,`company_name` varchar(150)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_company_sighup_approvals`
--
DROP TABLE IF EXISTS `vw_company_sighup_approvals`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_company_sighup_approvals`  AS  select `a`.`id` AS `id`,`a`.`company_id` AS `company_id`,`a`.`status` AS `status`,`a`.`date_created` AS `date_created`,`b`.`name` AS `company_name`,`b`.`residence_address` AS `location`,`b`.`categorys` AS `interests` from (`company_signup_requests` `a` left join `company_info` `b` on((`a`.`company_id` = `b`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_jobposting`
--
DROP TABLE IF EXISTS `vw_jobposting`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_jobposting`  AS  select `a`.`id` AS `id`,`a`.`job_title` AS `job_title`,`a`.`company_id` AS `company_id`,`a`.`contract_type` AS `contract_type`,`a`.`years_of_experience` AS `years_of_experience`,`a`.`salary_expectation` AS `salary_expectation`,`a`.`responsibilities` AS `responsibilities`,`a`.`skill_requirement` AS `skill_requirement`,`a`.`qualification` AS `qualification`,`a`.`duration` AS `duration`,`a`.`location` AS `location`,`a`.`geolocation` AS `geolocation`,`a`.`status` AS `status`,`a`.`approved_by` AS `approved_by`,`a`.`approved_date` AS `approved_date`,`a`.`posting_type_id` AS `posting_type_id`,`a`.`date_created` AS `date_created`,`b`.`name` AS `company_name` from (`job_postings` `a` left join `company_info` `b` on((`a`.`company_id` = `b`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blobs`
--
ALTER TABLE `blobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_interest_categories`
--
ALTER TABLE `company_interest_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_signup_requests`
--
ALTER TABLE `company_signup_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_socialmedia_links`
--
ALTER TABLE `company_socialmedia_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_postings`
--
ALTER TABLE `job_postings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_faculties`
--
ALTER TABLE `settings_faculties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_interest_category`
--
ALTER TABLE `settings_interest_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_posting_type`
--
ALTER TABLE `settings_posting_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_programmes`
--
ALTER TABLE `settings_programmes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signup_approval_requests`
--
ALTER TABLE `signup_approval_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_biodata`
--
ALTER TABLE `student_biodata`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `resume_id` (`resume_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`),
  ADD UNIQUE KEY `photo_id` (`photo_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `student_category_interests`
--
ALTER TABLE `student_category_interests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_contact_info`
--
ALTER TABLE `student_contact_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id_UNIQUE` (`biodata_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone_number_1` (`phone_number_1`);

--
-- Indexes for table `student_job_applications`
--
ALTER TABLE `student_job_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_school_info`
--
ALTER TABLE `student_school_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id_UNIQUE` (`biodata_id`),
  ADD UNIQUE KEY `employee_id` (`student_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blobs`
--
ALTER TABLE `blobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `company_interest_categories`
--
ALTER TABLE `company_interest_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_signup_requests`
--
ALTER TABLE `company_signup_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company_socialmedia_links`
--
ALTER TABLE `company_socialmedia_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `job_postings`
--
ALTER TABLE `job_postings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings_faculties`
--
ALTER TABLE `settings_faculties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings_interest_category`
--
ALTER TABLE `settings_interest_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings_posting_type`
--
ALTER TABLE `settings_posting_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings_programmes`
--
ALTER TABLE `settings_programmes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `signup_approval_requests`
--
ALTER TABLE `signup_approval_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student_biodata`
--
ALTER TABLE `student_biodata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_category_interests`
--
ALTER TABLE `student_category_interests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_contact_info`
--
ALTER TABLE `student_contact_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student_job_applications`
--
ALTER TABLE `student_job_applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_school_info`
--
ALTER TABLE `student_school_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Contains the Auto Generated ID for the User', AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `company_interest_categories`
--
ALTER TABLE `company_interest_categories`
  ADD CONSTRAINT `compan_id_link` FOREIGN KEY (`id`) REFERENCES `company_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_biodata`
--
ALTER TABLE `student_biodata`
  ADD CONSTRAINT `student_biodata_ibfk_1` FOREIGN KEY (`photo_id`) REFERENCES `blobs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
