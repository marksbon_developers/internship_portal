<?php 
	include('../models/Retrieval_Model.php');
	
	class JobApprovals extends Retrieval_Model {
		public function total_jobs_posted() { 
			$tablename = "job_postings";
			$where_condition = array('status' => "approved");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) {
				return sizeof($query_result);
			}
			else
				return false;
		}

		public function pending_signup_approvals() { 
			$tablename = "vw_company_sighup_approvals";
			$where_condition = array('1' => "1");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) {
				return $query_result;
			}
			else
				return false;
		}

		public function pending_jobs_approvals() { 
			$tablename = "vw_jobposting";
			$where_condition = array('1'=>1);
			$query_result = $this->select_alldata_where($tablename,$where_condition);

			if(!empty($query_result[0])) {
				return $query_result;
			}
			else
				return false;
		}

		public function recent_jobs_post() { 
			$tablename = "job_postings";
			$lastmonth = date('Y-m',strtotime('last month'));
			$thismonth = date('Y-m',strtotime('this month'));
			$where_condition = array("DATE_FORMAT(CAST(approved_date as DATE), '%Y-%m') between '" => $lastmonth."' And '".$thismonth."'");
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			//print_r($query_result); exit;
			if(!empty($query_result[0])) {
				return $query_result;
			}
			else
				return ($query_result=array());
		}
	}

	/***** Creating Instance Of Class *******/
	$Job_Approval_instance = new JobApprovals();
	/***** Total Jobs Posted *******/
	$total_job_post = $Job_Approval_instance->total_jobs_posted();
	/***** Total Pending Signup Approvals *******/
	$signup_approvals = $Job_Approval_instance->pending_signup_approvals();
	
	/***** Total Pending Jobs Approvals *******/
	$pending_jobs_approvals = $Job_Approval_instance->pending_jobs_approvals();
	/***** Recent Jobs Posting *******/
	$recent_jobs = $Job_Approval_instance->recent_jobs_post();

	




	
	