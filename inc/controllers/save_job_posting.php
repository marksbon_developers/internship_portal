<?php 
	require_once('../config/config.inc.php');
	require_once('../models/Update_Model.php');
	require_once('../models/Insertion_Model.php');
	require_once('../models/Retrieval_Model.php');
	
	class Job_Posting extends Insertion_Model {

		public $upload_path = "../../profiles/";
		protected $picture_allowed_extension = array("png","jpg","jpeg");

		/***** Creating Data Array ******/
		protected function create_db_array($post_params) { 
			if(!empty($post_params)) {
				/***** Creating Array To Mimic DB Structure  ******/
				if(true)
					$insert_data['register_form']['company_id'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($_SESSION['user']['company_id'])));

				if(array_key_exists('id', $post_params))
					$insert_data['register_form']['id'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['id'])));

				if(array_key_exists('industry', $post_params))
					$insert_data['register_form']['interest_category_id'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['industry'])));

				if(array_key_exists('job_title', $post_params))
					$insert_data['register_form']['job_title'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['job_title'])));

				if(array_key_exists('region', $post_params))
					$insert_data['register_form']['region'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['region'])));

				if(array_key_exists('location', $post_params))
					$insert_data['register_form']['location'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['location'])));

				if(array_key_exists('experience', $post_params))
					$insert_data['register_form']['experience_id'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['experience'])));

				if(array_key_exists('salary_expectation', $post_params))
					$insert_data['register_form']['salary_expectation'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['salary_expectation'])));

				if(array_key_exists('job_description', $post_params))
					$insert_data['register_form']['job_description'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['job_description'])));

				if(array_key_exists('key_responsibilities', $post_params))
					$insert_data['register_form']['responsibilities'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['key_responsibilities'])));

				if(array_key_exists('skils_required', $post_params))
					$insert_data['register_form']['skill_requirement'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['skils_required'])));

				if(array_key_exists('duration', $post_params))
					$insert_data['register_form']['duration'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities($post_params['duration'])));

				if(array_key_exists('deadline', $post_params))
					$insert_data['register_form']['post_deadline'] = mysqli_real_escape_string($this->dbconnection,trim(htmlentities(date('Y-m-d',strtotime($post_params['deadline'])))));

				return $insert_data;
			}
			else 
				return false;
		}
		/***** Creating Data Array ******/

		/***** Saving Post ******/
		public function save_details($post_data) {
			
			$sanitized_data = $this->create_db_array($post_data);
			$tablename = "job_postings";
			
			if($sanitized_data && empty(@$sanitized_data['register_form']['id'])) {
				
				$query_result = $this->save_data($tablename,$sanitized_data['register_form']);
							
				if(!empty($query_result['DB_ERR'])) 
					$_SESSION['error'] =  $biodata_result['DB_ERR']; 

				else if(isset($query_result['id']))
					$_SESSION['success'] = "Successful. Request Sent for approval";

				else
					$_SESSION['error'] = "Posting Failed";
			}

			# Updating Profile
			else if($sanitized_data && isset($sanitized_data['register_form']['id'])) { 
				print "<pre>"; print_r($sanitized_data['register_form']); print "</pre>"; exit;
				$update_instance = new Update_Model();
				$where_condition = "id = {$sanitized_data['register_form']['id']}";
				$query_result = $update_instance->update_info($tablename,$where_condition,$sanitized_data['register_form']);
				
				if(!empty($query_result['DB_ERR'])) 
					$_SESSION['error'] =  $biodata_result['DB_ERR']; 

				else if(isset($query_result['id']))
					$_SESSION['success'] = "Update Successful.";

				else
					$_SESSION['error'] = "Update Failed";
			}

			else 
				$_SESSION['error'] = "Empty Data Set";

			return($_SESSION);
		}
		/***** Saving Post ******/
	}
 
	/******* Saving Job Posting *********/
	if(isset($_POST['post_job_submit'])) { 
		$object = new Job_Posting();
		$insert_data = $object->save_details($_POST);

		header("location: ".BASE_URL."company/new-intern-job.php"); exit;
		
	}
	else if(isset($_POST['edit_post_submit'])) {
		$object = new Job_Posting();
		$insert_data = $object->save_details($_POST);

		header("location: ".BASE_URL."job-details.php?r=".$_POST['id']); exit;
	}
	else {
		$_SESSION['error'] = "No Data Transmitted";
		header('location: '.BASE_URL."company/new-intern-job.php"); exit;
	}
	/******* Saving Job Posting *********/
	




	
	