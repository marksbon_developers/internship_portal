<?php 
	include('../config/config.inc.php'); 
	include('../models/Update_Model.php');
	include('../models/Retrieval_Model.php');

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '../PHPMailer/src/PHPMailer.php';
	require '../PHPMailer/src/SMTP.php';
	require '../PHPMailer/src/Exception.php';
	
	class ApproveJob extends Update_Model {
		
		public function change_status($tablename,$postparams) { 
			$status = trim(htmlentities($postparams['status']));
			$job_id = trim(htmlentities($postparams['id']));
			
			$query_result = $this->status_change($tablename,$status,$job_id);
			return $query_result;
		}

		public function change_post_status($tablename,$postparams) { 
			$status = trim(htmlentities($postparams['post_status']));
			$job_id = trim(htmlentities($postparams['job_id']));
			# Checking Company Status
			$select_instance = new Retrieval_Model();
			$tablename_1 = "vw_jobposting";
			$where_condition = array('company_status' => "pending",'id' => $job_id);
			$query_result = $select_instance->select_alldata_where($tablename_1,$where_condition);
			
			if (isset($query_result[0]->id)) {
				$_SESSION['error'] = "Approve Company Request First";
				return false;
			}
			else {
				$query_result = $this->status_change($tablename,$status,$job_id);
				return $query_result;
			}
		}

		public function company_change_status($postparams) { 
			$tablename = "company_signup_requests";
			$status = trim(htmlentities($postparams['status']));
			$job_id = trim(htmlentities($postparams['company_id']));
			$query_result = $this->status_change($tablename,$status,$job_id);
			
			return $query_result;
		}

		public function get_email_alerts($post_id) { 
			$job_id = trim(htmlentities($post_id));
			# Checking Company Status
			$select_instance = new Retrieval_Model();
			$tablename_1 = "vw_jobposting";
			$where_condition = array('id' => $job_id);
			$job_query_result = $select_instance->select_alldata_where($tablename_1,$where_condition);

			if($job_query_result[0]->id) {
				# Searching for subscribers
				$tablename = "email_alerts";
				$where_condition = array('specialization_id' => $job_query_result[0]->interest_category_id);
				$query_result = $select_instance->select_alldata_where($tablename,$where_condition);
				# Searching for subscribers
				if(isset($query_result[0]->id)) {
					//Server settings
					$mail = new PHPMailer(); 
			    $mail->SMTPDebug = 0;    
			    $mail->isSMTP();                                         // Set mailer to use SMTP
			    $mail->SMTPAuth = true;                               // Enable SMTP authentication
			    $mail->Host = 'ssl://smtp.gmail.com';  // Specify main and backup SMTP servers
			    $mail->Username = GMAIL_USERNAME;                 // SMTP username
			    $mail->Password = GMAIL_PASSWORD;                           // SMTP password
			    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			    $mail->Port = 465; 
			    //Recipients
					$mail->setFrom('info@gtuc.edu.gh','GTUC PORTAL ALERT');
	        $mail->addReplyTo('info@gtuc.edu.gh','GTUC PORTAL ALERT');

					foreach ($query_result as $to_emails) {
						$mail->addAddress($to_emails->email);
					}
					//Content
			    $mail->isHTML(true);                                  // Set email format to HTML
			    $mail->Subject = 'GTUC INTERN JOB ALERT';
			    $mail->Body = "
						<table><tbody>
							<tr><td> Company Name: </td><td> {$job_query_result[0]->company_name} </td></tr>
							<tr><td> Job Title: </td><td><a href='".BASE_URL."job-details.php?r=".$job_query_result[0]->id."'> {$job_query_result[0]->job_title} </a></td></tr>
							<tr><td> Location: </td><td> {$job_query_result[0]->location} - {$job_query_result[0]->region} </td></tr>
							<tr><td> Period: </td><td> {$job_query_result[0]->duration} </td></tr>
							<tr><td> Specialization: </td><td> {$job_query_result[0]->interest_category_name} </td></tr>
							<tr><td> Salary Expectatioin: </td><td> {$job_query_result[0]->salary_expectation} </td></tr>
							<tr><td> Job Description: </td><td> {$job_query_result[0]->job_description} </td></tr>
							<tr><td> Responsibilities: </td><td> {$job_query_result[0]->responsibilities} </td></tr>
							<tr><td> Requirements: </td><td> {$job_query_result[0]->skill_requirement} </td></tr>
							</tbody>
						</table>
					";

					if($mail->send())
						return true;
					else
						return $error['error'] = $mail->ErrorInfo();
				}
			}
			else 
				return $false;
		}
	}

	/***** Creating Instance Of Class *******/
	$ApproveJob_instance = new ApproveJob();
	/*********** Jobs Posted ************/
	if(isset($_POST['post_status'])) {
		$tablename = "job_postings";
		$email_alert_list = $ApproveJob_instance->get_email_alerts($_POST['job_id']);

		if(isset($email_alert_list['error'])) {
			$_SESSION['error'] = $email_alert_list['error'];
			header("location: ".BASE_URL."admin/"); exit;
		}
		$status_change = $ApproveJob_instance->change_post_status($tablename,$_POST);
		
		if(isset($status_change['DB_ERR'])) 
			$_SESSION['error'] = $status_change['DB_ERR'];

		elseif(!empty($status_change)) {
			$_SESSION['success'] = "Successful";
		}

		header("location: ".BASE_URL."admin/"); exit;
	}
	/*********** Jobs Posted ************/

	/****** Company Status Change *******/
	if(isset($_POST['company_status_change'])) {
		$tablename = "company_info";
		$postparams = array(
			'status' => trim(htmlentities($_POST['company_status_change'])),
			'id' => trim(htmlentities($_POST['company_id']))
		);
		$status_change = $ApproveJob_instance->change_status($tablename,$postparams);
		
		if(isset($status_change['DB_ERR'])) 
			$_SESSION['error'] = $status_change['DB_ERR'];
		
		elseif(empty($status_change)) 
			$_SESSION['error'] = "Process Failed";
		
		else
			$_SESSION['success'] = "Process Successful";

		header("location: ".BASE_URL."admin/"); exit;
	}
	/****** Company Status Change *******/

	/****** Student Status Change *******/
	if(isset($_POST['student_status_change'])) {
		$tablename = "student_registration";
		$postparams = array(
			'status' => trim(htmlentities($_POST['student_status_change'])),
			'id' => trim(htmlentities($_POST['student_id']))
		);
		$status_change = $ApproveJob_instance->change_status($tablename,$postparams);
		
		if(isset($status_change['DB_ERR'])) 
			$_SESSION['error'] = $status_change['DB_ERR'];
		
		elseif(empty($status_change)) 
			$_SESSION['error'] = "Process Failed";
		
		else
			$_SESSION['success'] = "Process Successful";

		header("location: ".BASE_URL."admin/"); exit;
	}
	/****** Student Status Change *******/

	/****** Qualify / Disqualify Student *******/
	if(isset($_POST['student_application_status'])) {
		//print_r($_POST); exit;
		$tablename = "student_job_applications";
		$postparams = array(
			'status' => trim(htmlentities($_POST['student_application_status'])),
			'id' => trim(htmlentities($_POST['student_id']))
		);
		$application_status = trim(htmlentities($_POST['student_application_status']));
		$status_change = $ApproveJob_instance->change_status($tablename,$postparams);
		
		if(isset($status_change['DB_ERR'])) 
			$_SESSION['error'] = $status_change['DB_ERR'];
		
		elseif(empty($status_change)) 
			$_SESSION['error'] = "Process Failed";
		
		else {
			$_SESSION['success'] = "Process Successful";
			# Sending student email

			//Server settings
			$mail = new PHPMailer(); 
	    $mail->SMTPDebug = 0;    
	    $mail->isSMTP();                                         // Set mailer to use SMTP
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Host = 'ssl://smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->Username = GMAIL_USERNAME;                 // SMTP username
	    $mail->Password = GMAIL_PASSWORD;                           // SMTP password
	    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 465; 
	    //Recipients
			$mail->setFrom('info@gtuc.edu.gh','GTUC PORTAL ALERT');
      $mail->addReplyTo('info@gtuc.edu.gh','GTUC PORTAL ALERT');

			$mail->addAddress($_POST['student_email']);
			
			//Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'GTUC INTERN JOB ALERT';
	    
	    if(!empty($application_status)) {
	    	$mail->Body = "Company has ".$application_status." your application";
	    }

			if($mail->send())
				$_SESSION['success'] .= ". Email Sent to Student";
			else
				$_SESSION['error'] .= "Email Sending Failed";//@$mail->ErrorInfo();
		}

		header("location: ".BASE_URL."company/view_applicants.php?d=".$_POST['post_id']); exit;
	}
	/****** Qualify / Disqualify Student *******/
?>