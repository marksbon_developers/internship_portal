<?php  
	
	require_once('../inc/models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class Profile extends Retrieval_Model {
		# Retrieving Student Info
		public function retrieve_student_info() { 
			$tablename = "vw_student_details";
			$where_condition = array("id" => @$_SESSION['user']['student_id']);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# Retrieving Student Info

		# Editing Student Info
		public function edit_profile($edit_info) {
			# Data check
			if (!empty($edit_info)) {
				
			} else {
				return $error = array('error' => "Empty Post Data");
			}
		}
		# Editing Student Info

		# System Defined Settings
		public function specialization_listing() { 
			$tablename = "settings_interest_category";
			$where_condition = array("status" => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}

		public function experience_listing() { 
			$tablename = "settings_experiences";
			$where_condition = array("status" => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# System Defined Settings

		# Retrieving Company Info
		public function companyinfo($company_id) { 
			$tablename = "company_info";
			$where_condition = array('id' => $company_id);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) 
				return $query_result = $query_result[0];
			else
				return false;
		}
		# Retrieving Company Info

		# Retrieving total approved jobs
		public function jobs_published($company_id) { 
			$tablename = "vw_jobposting";
			$where_condition = array('company_id = ' => $company_id, 'status = ' => "'approved'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total approved jobs

		# Retrieving total pending jobs
		public function jobs_pending($company_id) { 
			$tablename = "vw_jobposting";
			$where_condition = array('company_id = ' => $company_id, 'status = ' => "'pending'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			//print_r($query_result); exit;
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Retrieving total pending jobs
		public function retrieve_post_info($post_id) { 
			$tablename = "vw_jobposting";
			$where_condition = array('id = ' => $post_id, 'company_id = ' => $_SESSION['user']['company_id']);
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result[0];
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Retrieving total pending jobs
		public function retrieve_total_applicants($post_id) { 
			$tablename = "student_job_applications";
			
			if(isset($_SESSION['user']['company_id']))
				$where_condition = array('job_post_id = ' => $post_id, 'status ='=>"'pending'");
			
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs
	}
	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$object_instance = new Profile();
	# ******** All Listing *******
	$temp_storage = $object_instance->retrieve_student_info();
	$student_info = $temp_storage[0];
	# Industry Listing
	$specialization_list = $object_instance->specialization_listing();
	# Industry Listing
	$experiences_list = $object_instance->experience_listing();
	# REtrieving company info
	if($_SESSION['user']['user_type'] == "company")  {
		$company_data = $object_instance->companyinfo($_SESSION['user']['company_id']);	
		$current_job_listing = $object_instance->jobs_published($_SESSION['user']['company_id']);
		$pending_job_listing = $object_instance->jobs_pending($_SESSION['user']['company_id']);

		# Editing Job Posting
		if(isset($company_edit_id)) {
			$post_info = $object_instance->retrieve_post_info($company_edit_id);

			if(!$post_info) {
				$_SESSION['error'] = "Post Access Denied";
				header("location:".BASE_URL."company/pending-posts.php"); 
				exit;
			}
		}
		# Editing Job Posting

		$total_applicants_count = $object_instance->retrieve_total_applicants(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities(@$post_id))));

		if(!empty($total_applicants_count[0])) {
				$total_applicants_count = sizeof($total_applicants_count);
		}
		else
			$total_applicants_count = 0;
		
		//print "<pre>"; print_r($total_applicants_count);print "</pre>";  exit;
	}

	
