<?php  
	
	require_once('inc/models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class View_Post extends Retrieval_Model {
		# System Defined Settings
		public function specialization_listing() { 
			$tablename = "settings_interest_category";
			$where_condition = array("status" => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}

		public function experience_listing() { 
			$tablename = "settings_experiences";
			$where_condition = array("status" => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# System Defined Settings

		# Retrieving Company Info
		public function companyinfo($company_id) { 
			$tablename = "company_info";
			$where_condition = array('id' => $company_id);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) 
				return $query_result = $query_result[0];
			else
				return false;
		}
		# Retrieving Company Info

		# Retrieving total pending jobs
		public function retrieve_post_info($post_id) { 
			$tablename = "vw_jobposting";
			
			if(isset($_SESSION['user']['company_id']))
				$where_condition = array('id = ' => $post_id/*, 'company_id = ' => $_SESSION['user']['company_id']*/);
			else
				$where_condition = array('id = ' => $post_id);
			
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result[0];
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Retrieving total pending jobs
		public function retrieve_total_applicants($post_id) { 
			$tablename = "student_job_applications";
			
			if(isset($_SESSION['user']['company_id']))
				$where_condition = array('job_post_id = ' => $post_id, 'status ='=>"'pending'");
			
			$query_result = $this->select_alldata_where_custom($tablename,@$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs
	}
	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$object_instance = new View_Post();
	# ******** All Listing *******
	# Industry Listing
	$specialization_list = $object_instance->specialization_listing();
	# Industry Listing
	$experiences_list = $object_instance->experience_listing();
	# Total Applicants
	if(@$_SESSION['user']['user_type'] == "company")
		# Retrieving Applicants
		$total_applicants_count = $object_instance->retrieve_total_applicants(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities($post_id))));
	
	if(!empty($total_applicants_count[0]))
		$total_applicants_count = sizeof($total_applicants_count);
	else
		$total_applicants_count = 0;
	# Retrieving Applicants

	# Retrieving Post Details
	if(isset($post_id)) {
		$post_info = $object_instance->retrieve_post_info(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities($post_id))));
		if(!empty($post_info)) {
			$company_data = $object_instance->companyinfo($post_info->company_id);
		}
	}

	if(empty(@$post_info)) {
		header("location: ".BASE_URL); 
	}

	//print "<pre>"; print_r($post_id);print "</pre>";  exit;