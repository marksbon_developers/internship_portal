<?php
	require_once('inc/models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class Search_Job extends Retrieval_Model {
		# Retrieving total pending jobs
		public function retrieve_posts_by_category($search) { 
			$tablename = "vw_jobposting";
			$where_condition = array('interest_category_id = ' => $search, 'status = ' => "'approved'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}

		public function retrieve_posts_by_keyword_n_location($keyword=null,$location=null) { 
			$tablename = "vw_jobposting";
			if(!empty($keyword) && empty($location))
				$where_condition = array('job_title like' => '"%'.$keyword.'%"', 'status = ' => "'approved'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			
			if(!empty($location) && empty($keyword))
				$where_condition = array('region =' => "'".$location."'", 'status = ' => "'approved'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			
			if(!empty($location) && !empty($keyword))
				$where_condition = array('job_title like' => '"%'.$keyword.'%"','region =' => "'".$location."'", 'status = ' => "'approved'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Showing all interests
		public function show_all_interests() { 
			$tablename = "vw_settings_interest_category";
			$where_condition = array('status' => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# Showing all interests
	}
	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$object_instance = new Search_Job();
	
	# Retrieving Post Details
	if(isset($category_search_id)) {
		$search_result = $object_instance->retrieve_posts_by_category(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities($category_search_id))));
	}

	if(isset($keyword) || isset($location)) {
		$search_result = $object_instance->retrieve_posts_by_keyword_n_location(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities($keyword))),mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities($location))));
	}
	# Retrieving All Categories
	$all_categories = $object_instance->show_all_interests();

	//print_r($search_result); 