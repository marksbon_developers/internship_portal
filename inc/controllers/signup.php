<?php  
	
	require_once('inc/models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class SignUp extends Retrieval_Model {
		public function programme_listing() { 
			$tablename = "settings_programmes";
			$where_condition = array("status" => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}

		public function specialization_listing() { 
			$tablename = "settings_interest_category";
			$where_condition = array("status" => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}

		public function company_listing() { 
			$tablename = "company_info";
			$where_condition = array("status" => "approved");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
	}

	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$object_instance = new SignUp();
	# ******** All Listing *******
	$programme_list = $object_instance->programme_listing();
	
	$specialization_list = $object_instance->specialization_listing();

	$company_list = $object_instance->company_listing();

	//print_r($programme_list); exit;