<?php 
	include('../inc/config/config.inc.php');
	include('../inc/models/Retrieval_Model.php');
	
	/***** Class Definition *******/
	class AdminDashboard extends Retrieval_Model {
		# All Jobs Listing
		public function job_posts($status) { 
			$tablename = "vw_jobposting";
			$where_condition = array('status' => $status);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				$_SESSION['error'] = $query_result['DB_ERR'];

			return $query_result;
		}
		# All Jobs Listing

		# All Companies Listing
		public function companies($status) { 
			$tablename = "vw_company_info";
			$where_condition = array('status' => $status);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				$_SESSION['error'] = $query_result['DB_ERR'];

			return $query_result;
		}
		# All Companies Listing

		# All Students Listing
		public function students($status) { 
			$tablename = "vw_student_details";
			$where_condition = array('registration_status' => $status);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				$_SESSION['error'] = $query_result['DB_ERR'];

			return $query_result;
		}
		# All Students Listing

		# All Students Listing
		public function pending_newsletters() { 
			$tablename = "news_subscribers";
			$where_condition = array('status' => 'pending');
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				$_SESSION['error'] = $query_result['DB_ERR'];

			return $query_result;
		}
		# All Students Listing

		# All Email Subscribers
		public function email_alerts() { 
			$tablename = "email_alerts";
			$where_condition = array('status' => 'active');
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				$_SESSION['error'] = $query_result['DB_ERR'];

			return $query_result;
		}
		# All Email Subscribers
	}
	/***** Class Definition *******/






	/***** Creating Instance Of Class *******/
	$adminDashboard_instance = new AdminDashboard();
	
	/***** Published Intern Jobs *******/
		$temp_storage = $adminDashboard_instance->job_posts($status = "approved");
		if(!empty($temp_storage[0])) {
			$approved_job_posts = $temp_storage;
			$approved_job_post_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$approved_job_posts = array();
			$approved_job_post_count = 0;
		}
	/***** Published Intern Jobs *******/

	/***** Pending Intern Jobs *******/
		$temp_storage = $adminDashboard_instance->job_posts($status = "pending");
		if(!empty($temp_storage[0])) {
			$pending_job_posts = $temp_storage;
			$pending_job_post_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$pending_job_posts = array();
			$pending_job_post_count = 0;
		}
	/***** Pending Intern Jobs *******/

	/***** Declined Intern Jobs *******/
		$temp_storage = $adminDashboard_instance->job_posts($status = "declined");
		if(!empty($temp_storage[0])) {
			$declined_job_posts = $temp_storage;
			$declined_job_post_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$declined_job_posts = array();
			$declined_job_post_count = 0;
		}
	/***** Declined Intern Jobs *******/

	/***** Pending Companies *******/
		$temp_storage = $adminDashboard_instance->companies($status = "pending");
		if(!empty($temp_storage[0])) {
			$pending_companys = $temp_storage;
			$pending_company_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$pending_companys = array();
			$pending_company_count = 0;
		}
	/***** Pending Companies *******/

	/***** Confirmed Companies *******/
		$temp_storage = $adminDashboard_instance->companies($status = "approved");
		if(!empty($temp_storage[0])) {
			$approved_companys = $temp_storage;
			$approved_company_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$approved_companys = array();
			$approved_company_count = 0;
		}
	/***** Confirmed Companies *******/

	/***** Pending Students *******/
		$temp_storage = $adminDashboard_instance->students($status = "pending");
		if(!empty($temp_storage[0])) {
			$pending_student = $temp_storage;
			$pending_student_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$pending_student = array();
			$pending_student_count = 0;
		}
	/***** Pending Students *******/

	/***** Approved Students *******/
		$temp_storage = $adminDashboard_instance->students($status = "approved");
		if(!empty($temp_storage[0])) {
			$confirmed_student = $temp_storage;
			$confirmed_student_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$confirmed_student = array();
			$confirmed_student_count = 0;
		}
	/***** Approved Students *******/

	/***** Approved Students *******/
		$temp_storage = $adminDashboard_instance->students($status = "declined");
		if(!empty($temp_storage[0])) {
			$declined_student = $temp_storage;
			$declined_student_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$declined_student = array();
			$declined_student_count = 0;
		}
	/***** Approved Students *******/


	/***** News Subscribers *******/
		$temp_storage = $adminDashboard_instance->pending_newsletters();
		if(!empty($temp_storage[0])) {
			$pending_subscribers = $temp_storage;
			$pending_subscribers_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$pending_subscribers = array();
			$pending_subscribers_count = 0;
		}
	/***** News Subscribers *******/

	/***** News Subscribers *******/
		$temp_storage = $adminDashboard_instance->email_alerts();
		if(!empty($temp_storage[0])) {
			$pending_email_subscribers = $temp_storage;
			$pending_email_subscribers_count = sizeof($temp_storage);
		}
		elseif (!empty($temp_storage['DB_ERR'])) {
			header("location: ".BASE_URL."admin"); exit;
		}
		else {
			$pending_email_subscribers = array();
			$pending_email_subscribers_count = 0;
		}
	/***** News Subscribers *******/
	//print "<pre>"; print_r($all_pending_students);print "</pre>"; exit;