<?php 
	
	include('../config/config.inc.php');
	include('../models/Insertion_Model.php');
	include('../models/Retrieval_Model.php');
	
	class Login_Validation extends Retrieval_Model {
		/***** Saving Company Info ******/
		public function verify_user($post_params) { 
			if(!empty($post_params)) {
				if(isset($post_params['username']) && isset($post_params['password'])) {
					
					$data_array['username'] = trim(htmlentities($post_params['username']));
					$tablename = "users";

					$query_result = $this->select_alldata_where($tablename,$data_array);

					if(@$query_result['DB_ERR']) {
						$_SESSION['error'] = $query_result['DB_ERR'];
						header('location: '.$_SERVER['HTTP_REFERER']);
					}
					elseif(empty($query_result[0])) {
						$_SESSION['error'] = "Invalid Username / Password Combination";
						header('location: '.$_SERVER['HTTP_REFERER']);
					}
					else{
						$query_result = $query_result[0];
						$password_hash = $query_result->password;
						if(password_verify($post_params['password'],$password_hash)) {
							# Student Login
							if($query_result->student_id) {
								/****** Retrieving Student Details ********/
								$tablename = "vw_student_details";
								$where_condition = array('id' =>  $query_result->student_id);
								$student_info = $this->select_alldata_where($tablename,$where_condition);
								//print_r($student_info);
								/****** Retrieving Student Details ********/ 

								$_SESSION['user']['user_type'] = "student";
								$_SESSION['user']['student_id'] = $query_result->student_id;
								$_SESSION['user']['user_status'] = $student_info[0]->registration_status;
							}
							# Company Login
							elseif($query_result->company_id) {
								/****** Retrieving Student Details ********/
								$tablename = "company_info";
								$where_condition = array('id' =>  $query_result->company_id);
								$company_info = $this->select_alldata_where($tablename,$where_condition);
								/****** Retrieving Student Details ********/ 

								$_SESSION['user']['user_type'] = "company";
								$_SESSION['user']['company_id'] = $query_result->company_id;
								$_SESSION['user']['user_status'] = $company_info[0]->status;
							}

							elseif($data_array['username'] == "admin@system.com") {
								$_SESSION['user']['user_type'] = "Administrator";
							}

							$_SESSION['user']['username'] = $data_array['username'];
							$_SESSION['user']['id'] = $query_result->id;
							$_SESSION['user']['fullname'] = $query_result->firstname." ".$query_result->lastname;
							$_SESSION['user']['login_url'] = $query_result->login_url;
							$_SESSION['user']['profile_picture'] = $query_result->profile_picture;
							header('location: '.BASE_URL.$query_result->login_url);
						}
						else {
							$_SESSION['error'] = "Invalid Username / Password Combination";
							header('location: '.$_SERVER['HTTP_REFERER']);
						}
					}
				}
			}
			else 
				return false;
		}
	}

	/******** Validating Admin user **********/
	if(isset($_POST['validate_login'])) {
		
		$obj = new Login_Validation();
		
		$search_result = $obj->verify_user($_POST);
		
	}
	/******** Validating Admin user **********/

	/******** Validating Admin user **********/
	if(isset($_POST['client_login'])) {
		
		$obj = new Login_Validation();
		$search_result = $obj->verify_user($_POST);
	}
	else {
		print_r($_POST); exit;
		header("location: ".$_SERVER['HTTP_REFERER']);
	}
	/******** Validating Admin user **********/





	
	