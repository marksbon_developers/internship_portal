<?php  
	
	include('../models/Retrieval_Model.php');
	
	class SignupPAge extends Retrieval_Model {
		
		public function all_specializations() { 
			
			$tablename = "settings_interest_category";
			$where_condition = array('status' => "active");
			
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
	}
	/***** Creating Instance Of Class *******/
	$object_instance = new SignupPAge();
	/***** Total Jobs Posted *******/
	$specialisations = $object_instance->all_specializations();