<?php 
	include('../config/config.inc.php');
	include('../models/Insertion_Model.php');
	
	/******* End Of Class Company *************/
	class Company extends Insertion_Model {

		public $upload_path = "../../profiles/";
		protected $picture_allowed_extension = array("png","jpg","jpeg");
		
		protected function create_db_array($post_params) { 
			if(!empty($post_params)) {
				/***** Creating Array To Mimic DB Structure  ******/
				if(array_key_exists('company_name', $post_params)) 
					$insert_data['biodata_info']['name'] = trim(htmlentities($post_params['company_name']));

				if(array_key_exists('tagline', $post_params))
					$insert_data['biodata_info']['company_tagline'] = trim(htmlentities($post_params['tagline']));

				/*if(array_key_exists('profile_summary', $post_params))
					$insert_data['biodata_info']['profile_summary'] = trim(htmlentities($post_params['profile_summary']));*/

					if(array_key_exists('website', $post_params))
					$insert_data['biodata_info']['website'] = trim(htmlentities($post_params['website']));

				if(array_key_exists('specialization', $post_params))
					$insert_data['biodata_info']['specialization'] = trim(htmlentities($post_params['specialization']));

				if(array_key_exists('about_us', $post_params))
					$insert_data['biodata_info']['about_us'] = trim(htmlentities($post_params['about_us']));

				if(array_key_exists('location', $post_params))
					$insert_data['biodata_info']['residence_address'] = trim(htmlentities($post_params['location']));

				if(array_key_exists('postal_address', $post_params))
					$insert_data['biodata_info']['postal_address'] = trim(htmlentities($post_params['postal_address']));

				if(array_key_exists('phone_num_1', $post_params))
					$insert_data['biodata_info']['telephone_1'] = trim(htmlentities($post_params['phone_num_1']));

				if(array_key_exists('phone_num_2', $post_params))
					$insert_data['biodata_info']['telephone_2'] = trim(htmlentities($post_params['phone_num_2']));

				if(array_key_exists('email', $post_params))
					$insert_data['biodata_info']['email'] = trim(htmlentities($post_params['email']));

				if(array_key_exists('password', $post_params)) {
					$insert_data['login_info']['username'] = trim(htmlentities($post_params['email']));
					$insert_data['login_info']['password'] = password_hash(trim(htmlentities($post_params['password'])),PASSWORD_DEFAULT);
					$insert_data['login_info']['firstname'] = trim(htmlentities($post_params['company_name']));
					$insert_data['login_info']['login_url'] = "company/profile.php";
				}

				return $insert_data;
			}
			else 
				return false;
		}

		public function set_variable($post_data) {
			return($this->create_db_array($post_data));
		}

		public function save_company_details($sanitized_data) {
			# Checking post data
			if(!empty($sanitized_data)) {
				# saving company info
				$tablename = "company_info";
				$query_result = $this->save_data($tablename,$sanitized_data['biodata_info']);
				# checking for db error
				if(isset($query_result['DB_ERR'])) 
					return  $query_result;
				# successful insertion
				else if($query_result['id']) {
					# Uploading Company Logo
					$file_upload_rules = [
						'type' => "picture",
						'prefix' => "company_".base64_encode($query_result['id'])
					];
					$get_file_upload_result = $this->upload_file($_FILES['company_logo'],$file_upload_rules);
					# checking upload error
					if(!empty($get_file_upload_result['error'])) 
						$return_data['error'] = $get_file_upload_result['error'];
					# successful ==> saving user login info
					else{
						$tablename = "users";
						$sanitized_data['login_info']['company_id'] = $query_result['id'];
						$sanitized_data['login_info']['profile_picture'] = @$get_file_upload_result;
						$login_query_result = $this->save_data($tablename,$sanitized_data['login_info']);

						if(isset($login_query_result['DB_ERR'])) 
							return $return_data['error'] = $query_result['DB_ERR'];

						else {
							$_SESSION['user']['id'] = $login_query_result['id'];
							$_SESSION['user']['company_id'] = $query_result['id'];
							$_SESSION['user']['username'] = $sanitized_data['biodata_info']['email'];
							$_SESSION['user']['fullname'] = $sanitized_data['biodata_info']['name'];
							$_SESSION['user']['profile_picture'] = @$get_file_upload_result;
			 				$_SESSION['user']['user_type'] = "company";

							return $_SESSION;
						}
					}
				}
				# strange reasons for fail
				else {
					$_SESSION['error'] = "Error Saving Data";
					return $_SESSION;
				}
			}
			# failed post data
			else {
				return $return_data['error'] = "Empty Data Set";
			}
		}

		/***** Uploading File ******/
		public function upload_file($file_array,$file_upload_rules) {
			if(!empty($file_array)) {
				# Defining Picture Upload
				if($file_upload_rules['type'] == "picture") {
					# File extension check
					$get_file_extension = pathinfo($file_array['name'],PATHINFO_EXTENSION );
					
					if(in_array($get_file_extension, $this->picture_allowed_extension) === false) 
						return $errors = array('error'=>"Unsupported File Type");
					
					else {
						$file_array['name'] = $file_upload_rules['prefix'].".".$get_file_extension;

						if(move_uploaded_file($file_array['tmp_name'], $this->upload_path.$file_array['name']))
							return $file_array['name'];

						else
							return $errors['error'] = "File Upload Failed";
					}
				}
			}
			else 
				return $errors['error'] = "Empty File Upload";
		}
		/***** Uploading File ******/
	}
	/******* End Of Class Company *************/





	/******* Saving Company Details *********/
	if(isset($_POST['register_company'])) { 
		if(strcasecmp($_POST['password'], $_POST['confirm_password']) != 0) {
			$_SESSION['error'] = "Password Mismatch. Please Check Password";
			header('location: '.BASE_URL."signup.php"); exit;
		}
		
		$object = new Company();
		$insert_data = $object->set_variable($_POST);
		# data array successful
		if($insert_data) {
			$save_data_query = $object->save_company_details($insert_data);
			# Checking For Errors
			if(isset($save_data_query['DB_ERR']) || isset($_SESSION['error'])) {
				
				if(isset($save_data_query['DB_ERR']))
					$_SESSION['error'] = $save_data_query['DB_ERR'];

				if(isset($_SESSION['error']))
					$_SESSION['error'] = $_SESSION['error'];

				header('location: '.BASE_URL."signup.php"); exit;
			}
			# successful data insertion
			else {
				$_SESSION['success'] = "Successful.";
				header('location: '.BASE_URL."company/profile.php"); exit;
			}
		} 
		# creating data array failed
		else {
			$_SESSION['error'] = "Creating Data Array Failed";
			header('location: '.BASE_URL."signup.php"); exit;
		}
	}
	/******* Saving Company Details *********/	