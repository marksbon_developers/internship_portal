<?php  
	
	require_once('inc/models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class IndexPage extends Retrieval_Model {
		# Recent Jobs Post
		public function most_recent_jobs() { 
			$tablename = "vw_jobposting";
			$where_condition = array('company_status =' => "'approved'",'status = ' => "'approved'", 'CAST(post_deadline AS DATE) > ' => "'".gmdate('Y-m-d')."' ORDER BY id DESC");
			$query_result = $this->select_alldata_where_custom($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result;
			
			else
				return false;
		}
		# Recent Jobs Post

		# Showing all interests
		public function show_all_interests() { 
			$tablename = "vw_settings_interest_category";
			$where_condition = array('status' => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);
			
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# Showing all interests

		# Total Companies signed up
		public function all_companies() {
			$tablename = "company_info";
			$where_condition = array('status' => "active");
			$query_result = $this->select_alldata_where($tablename,$where_condition);

			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return sizeof($query_result);
			
			else
				return 0;
		}
		# Total Companies signed up

		# Total Students signed up
		public function all_students() {
			$tablename = "student_registration";
			$where_condition = array('status' => "approved");
			$query_result = $this->select_alldata_where($tablename,$where_condition);

			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return sizeof($query_result);
			
			else
				return 0;
		}
		# Total Students signed up
	}
	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$indexpage_instance = new IndexPage();
	# ******** All Category Job Listing *******
	$total_all_interests = $indexpage_instance->show_all_interests();
	if(!empty($total_all_interests)) {
		foreach ($total_all_interests as $value) {
			@$total_jobs += $value->total_post;
		}
	}
	else
		$total_jobs = 0;
	# ******** All Recent Jobs Listing *******
	$recent_job_posts = $indexpage_instance->most_recent_jobs();
	
	$total_companies = $indexpage_instance->all_companies();

	$total_students = $indexpage_instance->all_students();

	//print "<pre>"; print_r($_SESSION['user']); print "</pre>";  