<?php  
	
	require_once('../config/config.inc.php');
	require_once('../models/Insertion_Model.php');
	require_once('../models/Update_Model.php');
	require_once('../models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class EditProfile extends Retrieval_Model {
		public $upload_path = "../../profiles/";
		protected $picture_allowed_extension = array("png","jpg","jpeg");

		# Retrieving Student Info
		public function retrieve_student_info() { 
			$tablename = "vw_student_details";
			$where_condition = array("id" => $_SESSION['user']['student_id']);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# Retrieving Student Info

		# Editing Student Info
		public function edit_profile($edit_info) {
			# Data check
			if (!empty($edit_info)) {
				
			} else {
				return $error = array('error' => "Empty Post Data");
			}
		}
		# Editing Student Info

		# Existing Student Info Check
		public function job_check($student_id) {
			if(!empty($student_id)) {
				$tablename = "student_latest_job";
				$where_condition = array("student_id" => $_SESSION['user']['student_id']);
				$query_result = $this->select_alldata_where($tablename,$where_condition);
				$query_result = @$query_result[0];

				if(@$query_result->DB_ERR || empty($query_result))
					return false;

				else if(@$query_result->id)
					return $query_result->id;
			} 

			else 
				return false;
		}
		# Existing Student Info Check

		/***** Uploading File ******/
		public function upload_file($file_array,$file_upload_rules) {
			if(!empty($file_array)) {
				# Defining Picture Upload
				if($file_upload_rules['type'] == "picture") {
					# File extension check
					$get_file_extension = pathinfo($file_array['name'],PATHINFO_EXTENSION );
					
					if(in_array($get_file_extension, $this->picture_allowed_extension) === false) 
						return $errors = array('error'=>"Unsupported File Type");
					
					else {
						$file_array['name'] = $file_upload_rules['prefix'].".".$get_file_extension;

						if(move_uploaded_file($file_array['tmp_name'], $this->upload_path.$file_array['name']))
							return $file_array['name'];

						else
							return $errors = ['error' => "Error Uploading File"];
					}
				}
			}
			else 
				return $errors['error'] = "Empty File Upload";
		}
		/***** Uploading File ******/
	}
	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$class_instance = new EditProfile();
	# ******** All Listing *******
	$temp_storage = @$class_instance->retrieve_student_info();
	$student_info = @$temp_storage[0];

	# Editing Student Info
	if(isset($_POST['edit_profile'])) {
		# Creating Recent Job Data Array
		$recent_job_data = [
			'student_id' => $_SESSION['user']['student_id'],
			'company_name' => trim(htmlentities($_POST['company_name'])),
			'position_department' => trim(htmlentities($_POST['position'])),
			'period' => trim(htmlentities($_POST['start_date']." ~ ".$_POST['end_date'])),
			'duties' => trim(htmlentities($_POST['job_description'])),
			'career_objective' => $_POST['career_objective']
		];
		$tablename = "student_latest_job";

		# Student Info Already Available
		if($result_id = $class_instance->job_check($_SESSION['user']['student_id'])) {
			$where_condition = array('student_id' => $_SESSION['user']['id']);
			$where_condition = "id = ".$result_id." AND status = 'active'";
			$update_instance = new Update_Model();

			$query_result = $update_instance->update_info($tablename,$where_condition,$recent_job_data);
			if($query_result['id']) {
				$_SESSION['success'] = "Record Update Successful";
				# Profile Picture Update
				if(!empty($_FILES['profile_pic'])) {
					$profile_pic_prefix = explode('.', $student_info->profile_picture);
					//print_r($profile_pic_prefix); exit;
					$file_upload_rules = [
						'type' => "picture",
						'prefix' => "student_".base64_encode($_SESSION['user']['student_id'])
					];

					$get_file_upload_result = $class_instance->upload_file($_FILES['profile_pic'],$file_upload_rules);
					//print_r($get_file_upload_result); exit;
					# checking upload error
					if(!$get_file_upload_result['error'])  {
						$_SESSION['error'] = $get_file_upload_result['error'];
						header("location: ".BASE_URL."student/edit-profile.php"); exit;
					}
					else {
						# updating company logo path
						$_SESSION['user']['profile_picture'] = $get_file_upload_result;
						# Updating Record in users table
						$tablename_2 = "users";
						$users_data = ['profile_picture' =>  $get_file_upload_result];
						$where_condition = "student_id = ".$_SESSION['user']['student_id'];
						$update_instance = new Update_Model();
						$query_result = $update_instance->update_info($tablename_2,$where_condition,$users_data);
					}
				}
			}
			
			elseif ($query_result['DB_ERR']) 
				$_SESSION['error'] = $query_result['DB_ERR'];
				
			header("location: ".BASE_URL."student/profile.php");
			exit;
		}

		else {
			$insertion_instance = new Insertion_Model();
			$query_result = $insertion_instance->save_data($tablename,$recent_job_data);

			if(!empty($query_result['DB_ERR']))
				$_SESSION['error'] =  $query_result['DB_ERR'];

			else if($query_result['id'])
				$_SESSION['success'] = "Record Saved";

			header("location: ".BASE_URL."student/profile.php");
			exit;
		}
	}		
	# Editing Student Info

	# Editing Company Info
	else if(isset($_POST['edit_company_profile'])) {
		# Creadting Recent Job Data Array
		$post_data = [
			'name' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['company_name']))),
			'company_tagline' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['tagline']))),
			'email' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['email']))),
			'telephone_1' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['phone_num_1']))),
			'telephone_2' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['phone_num_2']))),
			'residence_address' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['location']))),
			'postal_address' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['postal_address']))),
			'about_us' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['about_us']))),
			'website' => mysqli_real_escape_string($class_instance->dbconnection,trim(htmlentities($_POST['website']))),
		];
		$tablename = "company_info";
		
		# Company Info Already Available
		if(isset($_SESSION['user']['company_id']) && $_SESSION['user']['user_type'] == "company") {

			if(!empty($_FILES['company_logo'])) {
				$file_upload_rules = [
					'type' => "picture",
					'prefix' => "company_".base64_encode($_SESSION['user']['company_id'])
				];

				$get_file_upload_result = $class_instance->upload_file($_FILES['company_logo'],$file_upload_rules);
				//print_r($get_file_upload_result); exit;
				# checking upload error
				if(!$get_file_upload_result['error'])  {
					$_SESSION['error'] = $get_file_upload_result['error'];
					header("location: ".BASE_URL."company/profile.php"); exit;
				}
				else {
					# updating company logo path
					$_SESSION['user']['profile_picture'] = $get_file_upload_result;
					# Updating Record in users table
					$tablename_2 = "users";
					$users_data = ['profile_picture' =>  $get_file_upload_result];
					$where_condition = "company_id = ".$_SESSION['user']['company_id'];
					$update_instance = new Update_Model();
					$query_result = $update_instance->update_info($tablename_2,$where_condition,$users_data);
				}
			}

			$where_condition = "id = ".$_SESSION['user']['company_id']." AND status != 'inactive'";
			$update_instance = new Update_Model();
			$query_result = $update_instance->update_info($tablename,$where_condition,$post_data);
			
			if(isset($query_result['id']))
				$_SESSION['success'] = "Record Update Successful";
			
			elseif ($query_result['DB_ERR']) 
				$_SESSION['error'] = $query_result['DB_ERR'];
				
			header("location: ".BASE_URL."company/profile.php");
			exit;
		}

		else {
			$_SESSION['error'] = "An Error OCcurred";

			header("location: ".BASE_URL."company/profile.php");
			exit;
		}
	}
	# Editing Company Info

	//print_r($student_info); exit;