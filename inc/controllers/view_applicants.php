<?php  
	
	require_once('../inc/models/Retrieval_Model.php');
	
	/************** Begin Of Class ****************/
	class ViewApplicants extends Retrieval_Model {
		# Retrieving total pending jobs
		public function retrieve_pending_applicants($post_id) { 
			$tablename = "vw_student_job_applications";
			
			if(isset($_SESSION['user']['company_id']))
				$where_condition = array('job_post_id = ' => $post_id, 'status ='=>"'pending'");
			
			$query_result = $this->select_alldata_where_custom($tablename,@$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Retrieving total pending jobs
		public function retrieve_qualified_applicants($post_id) { 
			$tablename = "vw_student_job_applications";
			
			if(isset($_SESSION['user']['company_id']))
				$where_condition = array('job_post_id = ' => $post_id, 'status ='=>"'approved'");
			
			$query_result = $this->select_alldata_where_custom($tablename,@$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Retrieving total pending jobs
		public function retrieve_disqualified_applicants($post_id) { 
			$tablename = "vw_student_job_applications";
			
			if(isset($_SESSION['user']['company_id']))
				$where_condition = array('job_post_id = ' => $post_id, 'status ='=>"'declined'");
			
			$query_result = $this->select_alldata_where_custom($tablename,@$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				return $query_result = $query_result['DB_ERR'];
			
			else if(!empty($query_result[0])) 
				return $query_result = $query_result;
			
			else
				return false;
		}
		# Retrieving total pending jobs

		# Retrieving Student Info
		public function retrieve_student_info($student_id) { 
			$tablename = "vw_student_details";
			$where_condition = array("id" =>$student_id);
			$query_result = $this->select_alldata_where($tablename,$where_condition);
		
			if(!empty($query_result[0])) 
				return $query_result;
			else
				return false;
		}
		# Retrieving Student Info
		
	}
	/************** End Of Class ****************/


	# ***** Creating Instance Of Class *******
	$object_instance = new ViewApplicants();
	# Total Pending Applicants
	$total_applicants_count = $object_instance->retrieve_pending_applicants(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities(@$post_id))));
	if(!empty($total_applicants_count[0])) {
			$all_applicants = $total_applicants_count;
			$total_applicants_count = sizeof($total_applicants_count);
	}
	else
		$total_applicants_count = 0;

	# Total Qualified Applicants
	$qualified_applicants_count = $object_instance->retrieve_qualified_applicants(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities(@$post_id))));
	if(!empty($qualified_applicants_count[0])) {
			$all_qualified_applicants = $qualified_applicants_count;
			$qualified_applicants_count = sizeof($qualified_applicants_count);
	}
	else
		$qualified_applicants_count = 0;

	# Total DisQualified Applicants
	$disqualified_applicants_count = $object_instance->retrieve_disqualified_applicants(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities(@$post_id))));
	if(!empty($disqualified_applicants_count[0])) {
			$all_disquialified_applicants = $disqualified_applicants_count;
			$disqualified_applicants_count = sizeof($disqualified_applicants_count);
	}
	else
		$disqualified_applicants_count = 0;

	# Student Info
	$temp_storage = $object_instance->retrieve_student_info(mysqli_real_escape_string($object_instance->dbconnection,trim(htmlentities(@$student_id))));
	$student_info = $temp_storage[0];

	//print "<pre>"; print_r($student_info);print "</pre>";  exit;
	
