<?php 
	include('../config/config.inc.php');
	include('../models/Insertion_Model.php');
	include('../models/Retrieval_Model.php');
	
	class Student extends Insertion_Model {

		public $upload_path = "../../profiles/";
		protected $picture_allowed_extension = array("png","jpg","jpeg");

		/***** Creating Data Array ******/
		protected function create_db_array($post_params) { 
			if(!empty($post_params)) {
				/***** Creating Array To Mimic DB Structure  ******/
				if(array_key_exists('index_no', $post_params))
					$insert_data['register_form']['index_no'] = trim(htmlentities($post_params['index_no']));

				/*if(array_key_exists('program_of_study', $post_params))
					$insert_data['register_form']['program_of_study'] = trim(htmlentities($post_params['program_of_study']));*/

				if(array_key_exists('specialization', $post_params))
					$insert_data['register_form']['specializations'] = str_replace('"', "'", json_encode($post_params['specialization']));
				
				if(array_key_exists('password', $post_params)) {
					$insert_data['login_info']['username'] = trim(htmlentities($post_params['email']));
					$insert_data['login_info']['password'] = password_hash(trim(htmlentities($post_params['password'])),PASSWORD_DEFAULT);
					$insert_data['login_info']['login_url'] = "student/profile.php";
				}

				return $insert_data;
			}
			else 
				return false;
		}
		/***** Creating Data Array ******/

		/***** Saving Student Data ******/
		public function save_details($sanitized_data,$select_obj_instance = null) {
			if($sanitized_data) {
				# Checking Existing Registration
				$tablename = "student_registration";
				$data_array['index_no'] = $sanitized_data['register_form']['index_no'];
				$query_result = $select_obj_instance->select_alldata_where($tablename,$data_array);
				
				# DB Error Check
				if(!empty($result['DB_ERR'])) 
					$_SESSION['error'] =  $result['DB_ERR'];
				
				else if(!empty($query_result[0]->id))
					$_SESSION['error'] =  "Student Already Registered";
				
				else {
					# Validating Student Index No
					$tablename = "student_biodata";
					$data_array['index_no'] = $sanitized_data['register_form']['index_no'];
					$biodata_result = $select_obj_instance->select_alldata_where($tablename,$data_array);

					if(!empty($biodata_result['DB_ERR'])) 
						$_SESSION['error'] =  $biodata_result['DB_ERR'];
						
					else {
						if(empty($biodata_result)) 
							$_SESSION['error'] = "Invalid Index No";
						
						else {
							# Uploading Profile Picture
							$file_upload_rules = [
								'type' => "picture",
								'prefix' => "student_".base64_encode($biodata_result[0]->index_no)
							];
							$get_file_upload_result = $this->upload_file($_FILES['profile_pc'],$file_upload_rules);

							if(!empty($get_file_upload_result['error'])) {
								$_SESSION['error'] = $get_file_upload_result['error'];
								header("location: ".BASE_URL."signup.php");
								exit;
							}

							# Saving student registration
							$tablename = "student_registration";
							$query_result = $this->save_data($tablename,$sanitized_data['register_form']);
							
							if(!empty($query_result['DB_ERR'])) 
								$_SESSION['error'] =  $biodata_result['DB_ERR']; 

							else if($query_result['id']) {
								$tablename = "users";
								$sanitized_data['login_info']['profile_picture'] = @$get_file_upload_result;
								$sanitized_data['login_info']['firstname'] = $biodata_result[0]->other_names;
								$sanitized_data['login_info']['lastname'] = $biodata_result[0]->surname;
								$sanitized_data['login_info']['student_id'] = $biodata_result[0]->id;
								$login_query_result = $this->save_data($tablename,$sanitized_data['login_info']);

								if(!empty($login_query_result['DB_ERR'])) 
									$_SESSION['error'] =  $login_query_result['DB_ERR'];

								else {
									$_SESSION['success'] = "Registration Successful. Request Sent For Approval";
									
									$_SESSION['user']['id'] = $login_query_result['id']; 
									$_SESSION['user']['profile_picture'] =  @$get_file_upload_result;
									$_SESSION['user']['username'] = $biodata_result[0]->email;
									$_SESSION['user']['fullname'] = $biodata_result[0]->surname." ".$biodata_result[0]->other_names; 
									$_SESSION['user']['index_no'] = $sanitized_data['register_form']['index_no'];
									$_SESSION['user']['user_type'] = "student";
									$_SESSION['user']['student_id'] = $biodata_result[0]->id;
								}
							}
						}
					}
				}
			}

			else 
				$_SESSION['error'] = "Empty Data Set";

			return($_SESSION);
		}
		/***** Saving Student Data ******/

		/***** Setting Data Variable ******/
		public function set_variable($post_data) {
			return($this->create_db_array($post_data));
		}
		/***** Setting Data Variable ******/

		/***** Uploading File ******/
		public function upload_file($file_array,$file_upload_rules) {
			if(!empty($file_array)) {
				# Defining Picture Upload
				if($file_upload_rules['type'] == "picture") {
					# File extension check
					$get_file_extension = pathinfo($file_array['name'],PATHINFO_EXTENSION );
					
					if(in_array($get_file_extension, $this->picture_allowed_extension) === false) 
						return $errors = array('error'=>"Unsupported File Type");
					
					else {
						$file_array['name'] = $file_upload_rules['prefix'].".".$get_file_extension;

						if(move_uploaded_file($file_array['tmp_name'], $this->upload_path.$file_array['name']))
							return $file_array['name'];

						else
							return $errors['error'] = "File Upload Failed";
					}
				}
			}
			else 
				return $errors['error'] = "Empty File Upload";
		}
		/***** Uploading File ******/
	}
 
	/******* Saving Student Details *********/
	if(isset($_POST['register_student'])) { 
		if(strcasecmp($_POST['password'], $_POST['confirm_password']) != 0) {
			$_SESSION['error'] = "Password Mismatch. Please Check Password";
			header('location: '.BASE_URL."signup.php"); exit;
		}

		else {
			$class_object = new Student();
			$select_obj_instance = new Retrieval_Model();

			$insert_data = $class_object->set_variable($_POST);

			if($insert_data) {
				$save_data_query = $class_object->save_details($insert_data,$select_obj_instance);
				$error_messsage = (@$save_data_query['DB_ERR']) ?  $save_data_query['DB_ERR'] : @$save_data_query['error'];
				
				if($error_messsage) {
					$_SESSION['error'] = $error_messsage;
					header("location: ".BASE_URL."signup.php");
					exit;
				}
				else {
					$_SESSION['success'] = "Registration Successful.";
					header("location: ".BASE_URL);
				}
			} 
			else {
				$_SESSION['error'] = "Setting Variable Failed";
				header('location: '.BASE_URL."signup.php"); exit;
			}
		}
	}
	/*else {
		$_SESSION['error'] = "No Data Transmitted";
		header('location: '.BASE_URL."signup.php"); exit;
	}*/
	/******* Saving Student Details *********/

	/******* Saving Email Alerts *********/
	if(isset($_POST['email_alert'])) { 
		
		$class_object = new Student();

		$insert_data = [
			'student_id' => $_SESSION['user']['student_id'],
			'email' => $_SESSION['user']['username'],
			'specialization_id' => $_POST['specialization'],
			'company_id'  => $_POST['company']
		];

		if($insert_data) {
			$tablename = "email_alerts";
			$save_data_query = $class_object->save_data($tablename,$insert_data);
			
			$error_messsage = (@$save_data_query['DB_ERR']) ?  $save_data_query['DB_ERR'] : @$save_data_query['error'];
			
			if($error_messsage) {
				$_SESSION['error'] = $error_messsage;
				header("location: ".BASE_URL."alerts.php");
				exit;
			}
			else {
				$_SESSION['success'] = "Subscription Successful.";
				header("location: ".BASE_URL);
			}
		} 
		else {
			$_SESSION['error'] = "Setting Variable Failed";
			header('location: '.BASE_URL."alerts.php"); exit;
		}
	}
	/******* Saving Email Alerts *********/
	




	
	