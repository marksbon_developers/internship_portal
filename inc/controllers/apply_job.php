<?php
	require_once('../config/config.inc.php');
	require_once('../models/Insertion_Model.php');
	require_once('../models/Retrieval_Model.php');

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '../PHPMailer/src/Exception.php';
	require '../PHPMailer/src/PHPMailer.php';
	require '../PHPMailer/src/SMTP.php';

	# Total Students signed up
	function newsletters_subscribe($post_params) {
		$insertion_instance = new Insertion_Model();
		$tablename = "news_subscribers";
		$sanitized_data['email'] = mysqli_real_escape_string($insertion_instance->dbconnection,trim(htmlentities($post_params)));
		$login_query_result = $insertion_instance->save_data($tablename,$sanitized_data);

		if(isset($login_query_result['DB_ERR'])) 
			return $return_data['error'] = $query_result['DB_ERR'];

		else
			return $login_query_result;
	}
	# Total Students signed up

	# Checking Subscribers
	if(isset($_POST['subscribe'])) {
		$query_result = newsletters_subscribe($_POST['email']);

		if(isset($query_result['error'])) 
			$_SESSION['error'] = $query_result['error'];
		
		else
			$_SESSION['success'] = "Successful";

		header("location: ".BASE_URL);
	}
	# Checking Subscribers

	# Not Logged In
	if(empty($_SESSION['user']['id'])) {
		header("location: ".BASE_URL."signin.php");
		exit;
	}

	elseif($_SESSION['user']['user_status'] != "approved") {
		$_SESSION['error'] = "Your Signup Request Still Awaits Approval";
		header("location:".$_SERVER['HTTP_REFERER']); exit;
	}

	else {
		$post_id = urldecode($_GET['p']);
		$insertion_instance = new Insertion_Model();
		$etrieval_instance = new Retrieval_Model();
		$tablename = "student_job_applications";
		# Checking For Previous Application
		$where_condition = ['student_id' => $_SESSION['user']['student_id'], 'job_post_id' => $post_id];
		$query_result = $etrieval_instance->select_alldata_where($tablename,$where_condition);

		if(!empty($query_result['DB_ERR'])) {
			$_SESSION['error'] = $query_result['DB_ERR'];
			header("location: ".$_SERVER['HTTP_REFERER']); exit;
		}
		
		else if(!empty($query_result[0])) {
			$_SESSION['error'] = "You cannot apply for this Job for more than once";
			header("location: ".$_SERVER['HTTP_REFERER']); exit;
		}
		# Checking For Previous Application

		# Saving New Data
		else if(empty($query_result)) {
			$query_result = $insertion_instance->save_data($tablename,$where_condition);
			
			if(!empty($query_result['DB_ERR'])) 
				$_SESSION['error'] =  $biodata_result['DB_ERR']; 

			else if($query_result['id'])
				$_SESSION['success'] = "Application Successful.";

			header("location: ".$_SERVER['HTTP_REFERER']); exit;
		}
		# Saving New Data
	}