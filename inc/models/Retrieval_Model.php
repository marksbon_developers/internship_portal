<?php 

	class Retrieval_Model extends DB_Connection {

		public function select_alldata_where($tablename,$data_array) {
			if(!empty($data_array)) {
				$where_condition = "";
				$return_data = array();
				foreach($data_array as $key=>$value) {
					$where_condition .= $key." = '".$value."' AND ";
				}

				$where_condition = rtrim($where_condition," AND ");
				$db_query = "SELECT * FROM $tablename WHERE $where_condition";
				$db_query_result = mysqli_query($this->dbconnection,$db_query);
				
				if($db_query_result) {
					while ($row = mysqli_fetch_object($db_query_result)) {
						$return_data[] = $row;
					}

					return $return_data;
				}
				else
					$return_data = array('DB_ERR'=>mysqli_error($this->dbconnection));
			}
			else 
				$return_data = array('DB_ERR' => "Empty Data Array");
			
			return $return_data;
		}

		public function select_alldata_where_custom($tablename,$data_array) {
			if(!empty($data_array)) {
				$where_condition = "";
				
				foreach($data_array as $key=>$value) {
					$where_condition .= $key." ".$value." AND ";
				}

				$where_condition = rtrim($where_condition," AND ");
				$db_query = "SELECT * FROM $tablename WHERE $where_condition";
				$db_query_result = mysqli_query($this->dbconnection,$db_query);
		
				if($db_query_result) {
					while ($row = mysqli_fetch_object($db_query_result)) {
						$return_data[] = $row;
					}
				}

				else
					$return_data = array('DB_ERR'=>mysqli_error($this->dbconnection));
			}
			else 
				$return_data = array('DB_ERR' => "Empty Data Array");
			
			return @$return_data;
		}

	}