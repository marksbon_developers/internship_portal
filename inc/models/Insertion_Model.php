<?php 

	class Insertion_Model extends DB_Connection {

		public function save_data($tablename,$data_array) {
			if(!empty($data_array)) {
				
				$db_fields = array_keys($data_array);
				$db_values = array_values($data_array);

				$db_fields = implode(",", $db_fields);
				$db_values = '"'.implode('","', $db_values).'"';

				$db_query = "INSERT INTO $tablename ($db_fields) VALUES ($db_values)";
				
				$db_query_result = mysqli_query($this->dbconnection,$db_query);

				if($db_query_result) 
					$return_data = array('id'=>mysqli_insert_id($this->dbconnection));
				else
					$return_data = array('DB_ERR'=>mysqli_error($this->dbconnection));
			}
			else 
				$return_data = array('DB_ERR' => "Data Array Empty");
			
			return $return_data;
		}
	}