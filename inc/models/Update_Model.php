<?php 

	class Update_Model extends DB_Connection {
		public function status_change($tablename,$current_value,$id) {
			if(!empty($current_value) && !empty($id)) {
				
				$db_query = "UPDATE $tablename SET status='$current_value' WHERE id='$id'";
				$db_query_result = mysqli_query($this->dbconnection,$db_query);

				if($db_query_result) 
					$return_data = array('id'=>$id);
				else
					$return_data = array('DB_ERR'=>mysqli_error($this->dbconnection));
			}
			else 
				$return_data = array('DB_ERR' => "Data Array Empty");
			
			return $return_data;
		}

		public function jobpost_status_change($tablename,$current_value,$id) {
			if(!empty($current_value) && !empty($id)) {
				
				$db_query = "UPDATE $tablename SET status='$current_value' WHERE id='$id'";
				$db_query_result = mysqli_query($this->dbconnection,$db_query);

				if($db_query_result) 
					$return_data = array('id'=>$id);
				else
					$return_data = array('DB_ERR'=>mysqli_error($this->dbconnection));
			}
			else 
				$return_data = array('DB_ERR' => "Data Array Empty");
			
			return $return_data;
		}

		public function update_info($tablename, $where_condition, $data) {
			if(!empty($tablename) && !empty($where_condition) && !empty($data)) {
				# Re-constructing set statement
				foreach ($data as $key=>$value) {
					@$set_data .= "$key = '$value',"; 
				}
				$set_data = rtrim($set_data,",");  
				
				$db_query = "UPDATE $tablename SET ".$set_data." WHERE $where_condition"; 
				
				$db_query_result = mysqli_query($this->dbconnection,$db_query);

				if($db_query_result) 
					$return_data = array('id'=>$db_query_result);
				else
					$return_data = array('DB_ERR'=>mysqli_error($this->dbconnection));

				return $return_data;
			}
			# Empty Data
			else
				$return_data = array('DB_ERR' => "Data Array Empty");

			return $return_data;
		}
	}