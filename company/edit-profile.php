<?php	

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		session_destroy();
		header("location:".BASE_URL."signin.php");
		exit;
	}

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
?>

	<section class=" job-bg ad-details-page">
		<div class="container">
		
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li><a href="<?=BASE_URL?>company/profile.php">My Profile</a></li>
					<li>Edit Profile</li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Edit Profile</h2>
			</div><!-- banner -->

			<div class="adpost-details post-resume">
				<div class="row">	
					<div class="col-md-10 clearfix">
						<form action="<?=BASE_URL?>inc/controllers/edit-profile.php" method="post" enctype="multipart/form-data">
							<fieldset>
								<div class="section education-background">
									<h4>Edit Company Profile</h4>
									<!-- <h4 style="padding-left: 15px"><i class="fa fa-angle-double-right"></i> Tetiary Education</h4> -->
									<div class="row form-group">
										<label class="col-sm-3 label-title">Company Name</label>
										<div class="col-sm-9">
											<input type="text" name="company_name" class="form-control" placeholder="John Doe" value="<?=$company_data->name?>">
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">Motto / Slogan</label>
										<div class="col-sm-9">
											<input type="text" name="tagline" class="form-control" placeholder="Motto / Slogan" value="<?=$company_data->company_tagline?>"/>
										</div>
									</div>
									<div class="row form-group time-period">
										<label class="col-sm-3 label-title">Email Address / Website</label>
										<div class="col-sm-9">
											<input type="text" name="email" class="form-control" placeholder="Email Address" value="<?=$company_data->email?>"><span>-</span>
											<input type="text" name="website" class="form-control pull-right" placeholder="Email Address" value="<?=$company_data->website?>">
										</div>
									</div>
									<div class="row form-group time-period">
										<label class="col-sm-3 label-title">Phone Number #</label>
										<div class="col-sm-9">
											<input type="text" name="phone_num_1" class="form-control" placeholder="Primary Tel #" value="<?=$company_data->telephone_1?>" ><span>-</span>
											<input type="text" name="phone_num_2" class="form-control pull-right" placeholder="Secondary Tel #" value="<?=$company_data->telephone_2?>" >
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">Residence Address</label>
										<div class="col-sm-9">
											<input type="text" name="location" class="form-control" placeholder="Residence Address" value="<?=$company_data->residence_address?>" />
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">Postal Address</label>
										<div class="col-sm-9">
											<input type="text" name="postal_address" class="form-control" placeholder="Postal Address" value="<?=$company_data->postal_address?>">
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">About Company</label>
										<div class="col-sm-9">
											<textarea rows="7" class="form-control" name="about_us" placeholder="About Company" style="height: 200px"><?=$company_data->about_us?></textarea>
										</div>
									</div>
									
								</div><!-- work-history -->
							</fieldset>
							<div class="buttons">
								<button type="submit" class="btn btn-primary" name="edit_company_profile">Update Profile</button>
								<a href="<?=BASE_URL?>" class="btn cancle">Cancel</a>
							</div>	
						</form><!-- form -->						
					</div>
				
					<!-- quick-rules --
					<div class="col-md-4">
						<div class="section quick-rules">
							<h4>Quick rules</h4>
							<p class="lead">Posting an ad on <a href="#">jobs.com</a> is free! However, all ads must follow our rules:</p>

							<ul>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
								<li>Do not put your email or phone numbers in the title or description.</li>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
							</ul>
						</div>
					</div><!-- quick-rules -->	
				</div><!-- photos-ad -->				
			</div>	
		</div><!-- container -->
	</section><!-- main -->
	
	<?php include("../footer.php"); ?>