<?php	

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		header("location:".BASE_URL."signin.php");
		exit;
	}

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
?>

	<section class=" job-bg page  ad-profile-page">
		<div class="container">
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li>Company Profile</li>
				</ol>						
				<h2 class="title">My Profile</h2>
			</div><!-- breadcrumb-section -->
			
			<div class="job-profile section">	
				<div class="user-profile">
					<div class="user-images">
						<img src="<?="../profiles/".$_SESSION['user']['profile_picture']?>" alt="Company Logo" class="img-responsive" style="height: 200px; width: 200px">
					</div>
					<div class="user">
						<h2> <a href="#"><?=$company_data->name?></a></h2>
						<address>
						    <p>
						    	<p><strong>Address:</strong> <?=$company_data->postal_address?> <br></p>
						    	<p><strong>Residence Addres:</strong> <?=$company_data->residence_address?> <br> </p>
						    	<p><strong>Primary Phone:</strong> <?=$company_data->telephone_1?> / <?=@$student_info->telephone_2?> <br> </p>
						    	<p><strong>Email:</strong> <a href="#"> <?=$company_data->email?></a><br/></p>
						    </p>
						</address>
					</div>

					<div class="favorites-user">
						<div class="favorites">
							<a href="<?=BASE_URL?>company/published-posts.php"><?=($current_job_listing) ? sizeof($current_job_listing) : 0?><small>Jobs Published</small></a>
						</div>
						<div class="favorites">
							<a href="<?=BASE_URL?>company/pending-posts.php"><?=($pending_job_listing) ? sizeof($pending_job_listing) : 0?><small>Jobs Pending Approval</small></a>
						</div>
					</div>								
				</div><!-- user-profile -->
						
				<ul class="user-menu">
					<li class="active"><a href="<?=BASE_URL?>company/profile.php">Profile</a></li>
					<li><a href="<?=BASE_URL?>company/published-posts.php">Published Posts</a></li>
					<li><a href="<?=BASE_URL?>company/pending-posts.php">Pending Posts</a></li>
					<!-- <li><a href="<?=BASE_URL?>company/bookmark.php">Bookmark</a></li>
					<li><a href="<?=BASE_URL?>company/applied-job.php">applied job</a></li>
					<li><a href="<?=BASE_URL?>company/delete-account.php">Close account</a></li> -->
				</ul>
			</div><!-- ad-profile -->

			<div class="adpost-details post-resume">
				<div class="row">	
					<div class="col-md-12 clearfix">
						<form action="<?=BASE_URL?>inc/controllers/edit-profile.php" method="post" enctype="multipart/form-data">
							<fieldset>
								<div class="section education-background">
									<h4>Edit Company Profile</h4>
									<!-- <h4 style="padding-left: 15px"><i class="fa fa-angle-double-right"></i> Tetiary Education</h4> -->
									<div class="row form-group">
										<label class="col-sm-3 label-title">Company Logo</label>
										<div class="col-sm-9">
											<input type="file" name="company_logo" class="form-control">
										</div>
									</div>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Company Name</label>
										<div class="col-sm-9">
											<input type="text" name="company_name" class="form-control" placeholder="John Doe" value="<?=$company_data->name?>">
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">Motto / Slogan</label>
										<div class="col-sm-9">
											<input type="text" name="tagline" class="form-control" placeholder="Motto / Slogan" value="<?=$company_data->company_tagline?>"/>
										</div>
									</div>
									<div class="row form-group time-period">
										<label class="col-sm-3 label-title">Email Address / Website</label>
										<div class="col-sm-9">
											<input type="text" name="email" class="form-control" placeholder="Email Address" value="<?=$company_data->email?>"><span>-</span>
											<input type="text" name="website" class="form-control pull-right" placeholder="Email Address" value="<?=$company_data->website?>">
										</div>
									</div>
									<div class="row form-group time-period">
										<label class="col-sm-3 label-title">Phone Number #</label>
										<div class="col-sm-9">
											<input type="text" name="phone_num_1" class="form-control" placeholder="Primary Tel #" value="<?=$company_data->telephone_1?>" ><span>-</span>
											<input type="text" name="phone_num_2" class="form-control pull-right" placeholder="Secondary Tel #" value="<?=$company_data->telephone_2?>" >
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">Residence Address</label>
										<div class="col-sm-9">
											<input type="text" name="location" class="form-control" placeholder="Residence Address" value="<?=$company_data->residence_address?>" />
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">Postal Address</label>
										<div class="col-sm-9">
											<input type="text" name="postal_address" class="form-control" placeholder="Postal Address" value="<?=$company_data->postal_address?>">
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-3 label-title">About Company</label>
										<div class="col-sm-9">
											<textarea rows="7" class="form-control" name="about_us" placeholder="About Company" style="height: 200px"><?=$company_data->about_us?></textarea>
										</div>
									</div>
									
								</div><!-- work-history -->
							</fieldset>
							<div class="buttons">
								<button type="submit" class="btn btn-primary" name="edit_company_profile">Update Profile</button>
								<a href="<?=BASE_URL?>" class="btn cancle">Cancel</a>
							</div>	
						</form><!-- form -->						
					</div>
				
					<!-- quick-rules --
					<div class="col-md-4">
						<div class="section quick-rules">
							<h4>Quick rules</h4>
							<p class="lead">Posting an ad on <a href="#">jobs.com</a> is free! However, all ads must follow our rules:</p>

							<ul>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
								<li>Do not put your email or phone numbers in the title or description.</li>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
							</ul>
						</div>
					</div><!-- quick-rules -->	
				</div><!-- photos-ad -->				
			</div>						
		</div><!-- container -->
	</section><!-- ad-profile-page -->

	<?php	include_once("../footer.php");  ?>