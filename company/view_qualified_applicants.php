<?php
	include_once("../inc/config/config.inc.php");

	if(isset($_GET['d'])) {
		$company_edit_id = $_GET['d'];
		$post_id = $_GET['d'];
	}

	else 
		header("location: ".BASE_URL);

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
	include_once("../inc/controllers/view_applicants.php");  
?>
	
	<section class="job-bg page job-details-page">
		<div class="container">
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li><a href="<?=BASE_URL?>inc/controllers/search_job.php/?cat=<?=$post_info->interest_category_id?>"><?=$post_info->interest_category_name?></a></li>
					<li><a href="<?=BASE_URL?>inc/controllers/search_job.php/?pos=<?=$post_info->interest_category_id?>"><?=$post_info->job_title?></a></li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Internship Details</h2>
			</div><!-- breadcrumb -->

			<div class="job-details">
				<div class="section job-ad-item">
					<div class="item-info">
						<div class="item-image-box">
							<div class="item-image">
								<img src="<?=BASE_URL?>profiles/<?=$post_info->company_logo?>" alt="Image" class="img-responsive">
							</div><!-- item-image -->
						</div>

						<div class="ad-info">
							<span><a href="<?=BASE_URL.'company/edit-intern-job.php?d='.$post_info->id?>" class="title"><?=ucwords($post_info->job_title)?></a> @ <a class="prevent-click" href="#"><?=ucwords($post_info->company_name)?></a></span>
							<div class="ad-meta">
								<ul>
									<li><a class="prevent-click" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>
										Location: <strong><?=ucwords($post_info->location." - ".$post_info->region)?></strong> </a></li>
									<li><a class="prevent-click" href="#"><i class="fa fa-clock-o" aria-hidden="true"></i>
										Duration: <strong><?=ucwords($post_info->duration)?></strong></a></li>
									<!-- <li><a class="prevent-click" href="#"><i class="fa fa-tags" aria-hidden="true"></i><?=ucwords($post_info->interest_category_name)?></a></li> -->
									<li><a class="prevent-click" href="#"><i class="fa fa-hourglass-start" aria-hidden="true"></i> 
										Application Deadline : <strong><?=ucwords(date('F, Y',strtotime($post_info->post_deadline)))?></strong></a></li>
								</ul>
							</div><!-- ad-meta -->									
						</div><!-- ad-info -->
					</div><!-- item-info -->
					<div class="social-media">
						<div class="button">
							<?php if(@$_SESSION['user']['user_type'] != "company") : ?>
							<a href="<?=BASE_URL?>inc/controllers/apply_job?p=<?=$post_info->id?>" class="btn btn-primary"><i class="fa fa-briefcase" aria-hidden="true"></i>Apply For This Job</a>
							<!-- <a href="#" class="btn btn-primary bookmark"><i class="fa fa-bookmark-o" aria-hidden="true"></i>Bookmark</a> -->
							<?php elseif(isset($_SESSION['user']['id']) && $_SESSION['user']['user_type'] == "company") :?>
							<a href="<?=BASE_URL.'company/edit-intern-job?d='.$post_info->id?>" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Post</a>
							<a href="<?=BASE_URL.'company/view_applicants.php?d='.$post_info->id?>" class="btn btn-primary"><i class="fa fa-users" aria-hidden="true"></i>Pending Applicants (<?=@$total_applicants_count?>)</a>
							<a href="<?=BASE_URL.'company/view_applicants.php?d='.$post_info->id?>" class="btn btn-primary"><i class="fa fa-users" aria-hidden="true"></i>Qualified (<?=@$qualified_applicants_count?>)</a>
							<a href="<?=BASE_URL.'company/view_applicants.php?d='.$post_info->id?>" class="btn btn-primary"><i class="fa fa-users" aria-hidden="true"></i>Disqualified (<?=@$disqualified_applicants_count?>)</a>
							<?php endif; ?>
						</div>
						<ul class="share-social">
							<li>Share this ad</li>
							<li><a href="#" class="prevent-click"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
							<li><a href="#" class="prevent-click"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
							<li><a href="#" class="prevent-click"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
							<li><a href="#" class="prevent-click"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
						</ul>
					</div>					
				</div><!-- job-ad-item -->
				
				<div class="job-details-info">
					<div class="row">
						<div class="col-sm-8">
							<div class="section job-description">
								<?php if(!empty($all_qualified_applicants)) : foreach($all_qualified_applicants as $applicant) : ?>
								<div class="col=md-4">
									<div class="w3-container">
									  <div class="w3-card" style="width:50%;">
									    <img src="<?=BASE_URL.'profiles/'.$applicant->profile_picture?>" alt="Avatar" style="width:200px; height: 200px">
									    <div class="w3-container">
									      <h4><b><?=$applicant->fullname?></b></h4> 
									      <p><?=$applicant->programme_name?></p> 
									      <p><a href="<?=BASE_URL?>student/view_profile.php?d=<?=$post_id?>&s=<?=$applicant->student_id?>" class="btn btn-primary">Student Details</a></p> 
									    </div>
									  </div>
									</div>
								</div>
								<?php endforeach; endif; ?>					
							</div>							
						</div>
						<div class="col-sm-4">
							<div class="section job-short-info">
								<h1>Addition Info</h1>
								<ul>
									<!-- <li><span class="icon"><i class="fa fa-bolt" aria-hidden="true"></i></span>Posted: 1 day ago</li> -->
									<li><span class="icon"><i class="fa fa-money" aria-hidden="true"></i></span> 
										Salary Expectation: <a href="#" class="prevent-click"><strong><?=ucwords("GHC ".$post_info->salary_expectation)?></strong></a></li>
									<li><span class="icon"><i class="fa fa-industry" aria-hidden="true"></i></span>
										Industry: <a href="#" class="prevent-click"><strong><?=$post_info->interest_category_name?></strong></a></li>
									<li><span class="icon"><i class="fa fa-line-chart" aria-hidden="true"></i></span>
										Experience: <a href="#" class="prevent-click"><strong><?=$post_info->experience_level?></strong></a></li>
								</ul>
							</div>
							<div class="section company-info">
								<h1>Company Info</h1>
								<ul>
									<li>Company Name: <a href="#" class="prevent-click"><strong><?=$company_data->name?></strong></a></li>
									<li>Address: <strong><?=$company_data->residence_address?></strong></li>
									<li>Industry: <a href="#" class="prevent-click">Technology</a></li>
									<li>Phone: <strong><?=$company_data->telephone_1?> / <?=$company_data->telephone_2?></strong></li>
									<li>Email: <a href="#" class="prevent-click"><strong><?=$company_data->email?></strong></a></li>
									<li>Website: <a href="#" class="prevent-click"><strong><?=$company_data->website?></strong></a></li>
								</ul>
								<ul class="share-social">
									<li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								</ul>								
							</div>
						</div>
					</div><!-- row -->					
				</div><!-- job-details-info -->				
			</div><!-- job-details -->
		</div><!-- container -->
	</section><!-- job-details-page -->
	
	<?php	include_once("../footer.php");  ?>