<?php

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		header("location:".BASE_URL."signin.php");
		exit;
	}

	if(isset($_GET['d'])) 
		$company_edit_id = $_GET['d'];
	
	else 
		header("location: ".BASE_URL."company/published-posts.php");

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
?>
	<section class=" job-bg ad-details-page">
		<div class="container" style="">
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li>Job Post </li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Post Your Job</h2>
			</div><!-- banner -->

			<div class="job-postdetails">
				<div class="row">	
					<div class="col-md-8">
						<form action="<?=BASE_URL?>inc/controllers/save_job_posting.php" method="post">
							<input type="hidden" name="id" value="<?=$post_info->id?>">
							<fieldset>
								<div class="section postdetails">
									<h4>Post Your Job<span class="pull-right">* Mandatory Fields</span></h4>
									<div class="row form-group add-title">
										<label class="col-sm-3 label-title">Job Industry</label>
										<div class="col-sm-9">
											<select class="form-control input-lg" name="industry" required>
                        <option label="Select One"></option>
                        <?php if(!empty($specialization_list)) : foreach ($specialization_list as $interest) : if($post_info->interest_category_id == $interest->id) {$selected = "selected='selected'";}?>
												<option value="<?=$interest->id?>" <?php print @$selected; unset($selected);?>><?=$interest->name?></option>
												<?php endforeach; endif; ?>
                      </select>
										</div>
									</div>	
									<div class="row form-group">
										<label class="col-sm-3 label-title">Job Title / Position <span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="text" name="job_title" class="form-control" placeholder="" value="<?=$post_info->job_title?>" required>
										</div>
									</div>					
									<div class="row form-group add-title location">
										<label class="col-sm-3 label-title">Location<span class="required">*</span></label>
										<div class="col-sm-9">
											<div class="col-sm-7" style="padding-left: 0px"> 
												<select class="form-control input-lg" name="region" required>
	                        <option label="Select Region"></option>
	                        <?php 
	                        	$regions = array("Greater Accra","Central Region","Western Region","Eastern Region","Volta Region","Ashanti Region","Brong-Ahafo","Nothern Region","Upper East","Upper West");

	                        	foreach ($regions as $region) {
	                        		if ($region == $post_info->region) 
	                        			print "<option selected='selected'>$region</option>";
	                        		else
	                        			print "<option>$region</option>";
	                        	}
	                        ?>
	                      </select>
											</div>
											
											<div class="form-group col-sm-5" style="padding-right: 0px">
												<!-- <div class="col-sm-5" style="width: 51%"> -->
													<input type="text" name="location" class="form-control" placeholder="Township" style="width: 100%" value="<?=$post_info->location?>" required>
												<!-- </div> -->
											</div>
										</div>
									</div>	
									<div class="row form-group add-title">
										<label class="col-sm-3 label-title">Experience<span class="required">*</span></label>
										<div class="col-sm-9">
											<select class="form-control input-lg" name="experience" required>
                        <option label="Select One"></option>
                        <?php if(!empty($experiences_list)) : foreach ($experiences_list as $experience) :  if($post_info->experience_id == $experience->id) {$selected = "selected='selected'";} ?>
												<option value="<?=$experience->id?>"  <?php print @$selected; unset($selected);?>><?=$experience->name?></option>
												<?php endforeach; endif; ?>
                      </select>
										</div>
									</div>			
									<div class="row form-group">
										<label class="col-sm-3 label-title">Duration<span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="text" name="duration" class="form-control" placeholder="May, 2017 - June, 2018" value="<?=$post_info->duration?>" required>
										</div>
									</div>	
									<div class="row form-group">
										<label class="col-sm-3 label-title">Salary<span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="text" name="salary_expectation" class="form-control" placeholder="GHC 500 - 600" value="<?=$post_info->salary_expectation?>" required>
										</div>
									</div>	
									<div class="row form-group item-description">
										<label class="col-sm-3 label-title">Job Description<span class="required">*</span></label>
										<div class="col-sm-9">
											<textarea class="form-control" name="job_description" id="textarea" placeholder="Write few lines about your jobs" style="height: 120px; !important" required><?=$post_info->job_description?></textarea>		
										</div>
									</div>
									<div class="row form-group item-description">
										<label class="col-sm-3 label-title">Key Responsibilities:<span class="required">*</span></label>
										<div class="col-sm-9">
											<textarea class="form-control" name="key_responsibilities" id="textarea" placeholder="Write few lines about your jobs" style="height: 120px; !important" required><?=$post_info->responsibilities?></textarea>		
										</div>
									</div>
									<div class="row form-group item-description">
										<label class="col-sm-3 label-title">Minimum Requirements<span class="required">*</span></label>
										<div class="col-sm-9">
											<textarea class="form-control" name="skils_required" id="textarea" placeholder="Write few lines about your jobs" style="height: 120px; !important" required><?=$post_info->skill_requirement?></textarea>		
										</div>
									</div>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Deadline For Post<span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="text" name="deadline" class="form-control" placeholder="3 weeks" value="<?=$post_info->post_deadline?>" required>
										</div>
									</div>										
								</div><!-- postdetails -->
								
								<div class="checkbox section agreement">
									<button type="submit" class="btn btn-primary" name="edit_post_submit">Save Changes</button>
									<a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-danger btn-lg" name="edit_post_submit">Cancel</a>
								</div><!-- section -->
							</fieldset>
						</form><!-- form -->	
					</div>
				

					<!-- quick-rules -->	
					<div class="col-md-4">
						<div class="section quick-rules">
							<h4>Quick rules for posting</h4>
							<ul>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
							</ul>
						</div>
						<div class="section company-info">
							<h1>Company Info</h1>
							<ul>
								<li>Company Name: <a href="#"><strong><?=$company_data->name?></strong></a></li>
								<li>Address: <strong><?=$company_data->residence_address?></strong></li>
								<li>Industry: <a href="#">Technology</a></li>
								<li>Phone: <strong><?=$company_data->telephone_1?> / <?=$company_data->telephone_2?></strong></li>
								<li>Email: <a href="#"><strong><?=$company_data->email?></strong></a></li>
								<li>Website: <a href="#"><strong><?=$company_data->website?></strong></a></li>
							</ul>
							<ul class="share-social">
								<li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
							</ul>								
						</div>
					</div><!-- quick-rules -->	
				</div><!-- photos-ad -->				
			</div>	
		</div><!-- container -->
	</section><!-- main -->
	
	<?php include("../footer.php"); ?>