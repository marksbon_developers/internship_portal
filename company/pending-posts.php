<?php	

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		header("location:".BASE_URL."signin.php");
		exit;
	}

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
?>

	<section class=" job-bg page  ad-profile-page">
		<div class="container">
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li>Company Profile</li>
				</ol>						
				<h2 class="title">My Profile</h2>
			</div><!-- breadcrumb-section -->
			
			<div class="job-profile section">	
				<div class="user-profile">
					<div class="user-images">
						<img src="<?="../profiles/".$_SESSION['user']['profile_picture']?>" alt="User Images" class="img-responsive" style="height: 200px; width: 200px">
					</div>
					<div class="user">
						<h2> <a href="#"><?=$company_data->name?></a></h2>
						<address>
						    <p>
						    	<p><strong>Address:</strong> <?=$company_data->postal_address?> <br></p>
						    	<p><strong>Residence Addres:</strong> <?=$company_data->residence_address?> <br> </p>
						    	<p><strong>Primary Phone:</strong> <?=$company_data->telephone_1?> / <?=@$student_info->telephone_2?> <br> </p>
						    	<p><strong>Email:</strong> <a href="#"> <?=$company_data->email?></a><br/></p>
						    </p>
						</address>
					</div>

					<div class="favorites-user">
						<div class="favorites">
							<a href="<?=BASE_URL?>company/published-posts.php"><?=($current_job_listing) ? sizeof($current_job_listing) : 0?><small>Jobs Published</small></a>
						</div>
						<div class="favorites">
							<a href="<?=BASE_URL?>company/pending-posts.php"><?=($pending_job_listing) ? sizeof($pending_job_listing) : 0?><small>Jobs Pending Approval</small></a>
						</div>
					</div>								
				</div><!-- user-profile -->
				<ul class="user-menu">
					<li><a href="<?=BASE_URL?>company/profile.php">Profile</a></li>
					<li><a href="<?=BASE_URL?>company/published-posts.php">Published Posts</a></li>
					<li class="active"><a href="<?=BASE_URL?>company/pending-posts.php">Pending Posts</a></li>
				</ul>
			</div><!-- ad-profile -->

			<?php if(!empty($pending_job_listing)) : ?>
			<div class="resume-content">
				<div class="career-objective section">
					<?php foreach ($pending_job_listing as $job_post) : ?>
	        <div class="job-ad-item">
							<div class="item-info">
								<div class="item-image-box">
									<div class="item-image">
										<a href="job-details.html"><img src="<?=BASE_URL.'profiles/'.$job_post->company_logo?>" alt="Image" class="img-responsive"></a>
									</div><!-- item-image -->
								</div>

								<div class="ad-info">
									<span><a href="<?=BASE_URL.'company/edit-intern-job.php?d='.$job_post->id?>" class="title"><?=ucwords($job_post->job_title)?></a> @ <a class="prevent-click" href="#"><?=ucwords($job_post->company_name)?></a></span>
									<div class="ad-meta">
										<ul>
											<li><a class="prevent-click" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?=ucwords($job_post->location." - ".$job_post->region)?> </a></li>
											<li><a class="prevent-click" href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?=ucwords($job_post->duration)?></a></li>
											<li><a class="prevent-click" href="#"><i class="fa fa-money" aria-hidden="true"></i><?=ucwords("GHC ".$job_post->salary_expectation)?></a></li>
											<li><a class="prevent-click" href="#"><i class="fa fa-tags" aria-hidden="true"></i><?=ucwords($job_post->interest_category_name)?></a></li>
										</ul>
									</div><!-- ad-meta -->									
								</div><!-- ad-info -->
								<div class="button">
									<!-- <a href="<?=BASE_URL.'company/edit-intern-job.php?d='.$job_post->id?>" class="btn btn-primary">Edit Info</a> -->
									<a href="<?=BASE_URL.'job-details.php?r='.$job_post->id?>" class="btn btn-primary">View Details</a>
								</div>
							</div><!-- item-info -->
					</div><!-- ad-item -->   
					<?php endforeach; ?>                             
				</div><!-- career-objective -->
			</div><!-- resume-content -->		
			<?php endif; ?> 				
		</div><!-- container -->
	</section><!-- ad-profile-page -->

	<?php	include_once("../footer.php");  ?>