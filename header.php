<?php include_once('inc/config/config.inc.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
    <meta name="description" content="">

    <title>GTUC Internship Job Portal</title>

    <!-- CSS -->
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/bootstrap.min.css" >
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/icofont.css"> 
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/slidr.css">     
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/main.css">  
    <link id="preset" rel="stylesheet" href="<?=BASE_URL?>assets/css/presets/preset1.css">  
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/responsive.css">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/fSelect.css">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/w3.css">
  
  <!-- font --assets/>
  <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

  <!-- icons -->
  <link rel="icon" href="images/ico/favicon.ico"> 
    <link rel="apple-touch-icon" sizes="144x144" href="<?=BASE_URL?>assets/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=BASE_URL?>assets/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=BASE_URL?>assets/images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=BASE_URL?>assets/images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->
  </head>
  <body>
  <!-- header -->
  <header id="header" class="clearfix">
    <!-- navbar -->
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- navbar-header -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=BASE_URL?>"><h4><strong> GTUC Intern Portal </strong></h4> <!-- <img class="img-responsive" src="<?=BASE_URL?>assets/images/logo.png" alt="Logo"> --></a>
        </div>
        <!-- /navbar-header -->
        
        <div class="navbar-left">
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?=BASE_URL?>">Home</a></li>
              <!--  ------- Student MEnu --------- -->
              <?php if(@$_SESSION['user']['user_type'] == "student") : ?>
              <li><a href="<?=BASE_URL?>student/profile.php">profile</a></li>
              <?php endif; ?>
              <?php if(@$_SESSION['user']['user_type'] == "company") : ?>
              <li><a href="<?=BASE_URL?>company/profile.php">profile</a></li>
              <?php endif; ?>
            </ul>
          </div>
        </div><!-- navbar-left -->
        
        <!-- nav-right -->
        <div class="nav-right">       
          <ul class="sign-in">
            <?php if(!isset($_SESSION['user'])) : ?>
            <li><i class="fa fa-user"></i></li>
            <li><a href="signin.php">Sign In</a></li>
            <li><a href="signup.php">Register</a></li>
          </ul><!-- sign-in -->         
          <?php endif; if(@$_SESSION['user']['user_type'] == "company") : ?>
            <a href="<?=BASE_URL?>company/new-intern-job.php" class="btn">Post Your Job</a>
          <?php endif; if(@$_SESSION['user']['id']) : ?>
            <a href="<?=BASE_URL?>logout.php" class="btn" style="background: red">Logout</a>
          <?php endif; ?>
        </div>
        <!-- nav-right -->
      </div><!-- container -->
    </nav><!-- navbar -->
  </header><!-- header -->