 		
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
				  <b>Version</b> 1.1
				</div>
				<strong>©Copyright 2018 <a href="<?=BASE_URL?>admin">GTUC Inernship Portal</a>.</strong> All rights
				reserved.
			</footer>
		</div>
		<!-- /. WRAPPER  -->
		<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
		<!-- JQUERY SCRIPTS -->
		<script src="../admin_assets/js/jquery-1.10.2.js"></script>
		<!-- BOOTSTRAP SCRIPTS -->
		<script src="../admin_assets/js/bootstrap.min.js"></script>
		<!-- METISMENU SCRIPTS -->
		<script src="../admin_assets/js/jquery.metisMenu.js"></script>
		<!-- Bootstrap Editor Js -->
		<script src="../admin_assets/js/wysihtml5-0.3.0.js"></script>
		<script src="../admin_assets/js/bootstrap-wysihtml5.js"></script>
		<!-- Scrollbar Js -->
		<script src="../admin_assets/js/jquery.slimscroll.js"></script>
		<!-- Dropzone Js -->
		<script src="../admin_assets/js/dropzone.js"></script>
		<!-- CUSTOM SCRIPTS -->
		<script src="../admin_assets/js/custom.js"></script>

		<!-- Notification -->
		  <script type="text/javascript" src="<?=BASE_URL?>assets/js/notify.js"></script>
		  <script type="text/javascript">
		    <?php if(isset($_SESSION['success'])) : ?>
		    $.notify("<?=$_SESSION['success']?>", "success");
		    <?php elseif(isset($_SESSION['error'])) : ?>
		    $.notify("<?=$_SESSION['error']?>", "error");
		    <?php endif; unset($_SESSION['success'],$_SESSION['error'])?>
		  </script>
		<!-- Notification -->
	</body>

<!-- Mirrored from live.themezhub.com/job-stock-large-preview/job-stock/dashboard/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 22 Feb 2018 10:48:15 GMT -->
</html>