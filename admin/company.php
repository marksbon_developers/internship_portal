﻿	<?php 
		require_once('../inc/config/config.inc.php'); 

		if(!isset($_SESSION['user']['id'])) {
			header("location: ".BASE_URL."signin.php"); exit;
		}

		if($_SESSION['user']['user_type'] != "Administrator") {
			header("location: ".BASE_URL."signin.php"); exit;
		}

		@require_once('../inc/controllers/admin_dashboard.php'); 
		require_once('header.php'); 
	?>
				<div id="page-inner">
				  <div class="row bott-wid">
						<div class="col-md-12 col-sm-12">
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Verified Companies</h4>
								</div>
								<!-- Job Lists-->
								<?php if (!empty($approved_companys)) : foreach($approved_companys as $company) :?>
									<div class="col-md-4">	
										<a href="#">
											<div class="user-img">
												<img src="<?=BASE_URL."profiles/".$company->company_logo?>" alt="user" class="img-circle" style="width: 150px; height: 150px">
												<span class="profile-status online pull-right"></span>
												<!-- <i class="fa fa-circle active" aria-hidden="true"></i> -->
											</div>
											<div class="message-content">
											<h5><?=$company->name?></h5>
											<span class="mail-desc"><?=$company->residence_address?></span>
											<div class="buttons-placement">
												<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
													<input type="hidden" name="company_id" value="<?=$company->id?>"/>
													<!-- <button class="btn btn-success btn-xxs" name="company_status_change" value="Approved">Approve</button> -->
													<button class="btn btn-danger btn-xxs" name="company_status_change" value="Declined">Decline</button>
												</form>
											</div>
											</div>
										</a>
									</div>
								<?php endforeach; endif; ?>
								<!-- ./Job Lists-->
							</div>
						</div>
					</div>
				</div>
			</div>
			 <!-- /. PAGE WRAPPER  -->
	<?php require_once('footer.php'); ?>