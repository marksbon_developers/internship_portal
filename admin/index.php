﻿	<?php 
		require_once('../inc/config/config.inc.php'); 

		if(!isset($_SESSION['user']['id'])) {
			header("location: ".BASE_URL."signin.php"); exit;
		}

		if($_SESSION['user']['user_type'] != "Administrator") {
			header("location: ".BASE_URL."signin.php"); exit;
		}

		@require_once('../inc/controllers/admin_dashboard.php'); 
		require_once('header.php'); 
	?>
				<div id="page-inner">
				  <div class="row bott-wid">
						<div class="col-md-3 col-sm-6">
							<div class="widget unique-widget">
								<div class="row">
									<div class="widget-caption info">
										<div class="col-xs-4 no-pad">
											<i class="icon icon-profile-male"></i>
										</div>
										<div class="col-xs-8 no-pad">
											<div class="widget-detail">
												<h3><?=$confirmed_student_count?></h3>
												<span>Registered Students</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="widget unique-widget">
								<div class="row">
									<div class="widget-caption danger">
										<div class="col-xs-4 no-pad">
											<i class="icon icon-profile-male"></i>
										</div>
										<div class="col-xs-8 no-pad">
											<div class="widget-detail">
												<h3><?=$pending_student_count?></h3>
												<span>Unconfirmed Students</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="widget unique-widget">
								<div class="row">
									<div class="widget-caption sucess">
										<div class="col-xs-4 no-pad">
											<i class="icon icon-newspaper"></i>
										</div>
										<div class="col-xs-8 no-pad">
											<div class="widget-detail">
												<h3><?=$approved_job_post_count?></h3>
												<span>Published Internship</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="widget unique-widget">
								<div class="row">
									<div class="widget-caption danger">
										<div class="col-xs-4 no-pad">
											<i class="icon icon-newspaper"></i>
										</div>
										<div class="col-xs-8 no-pad">
											<div class="widget-detail">
												<h3><?=$pending_job_post_count?></h3>
												<span>Pending Internships</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="widget unique-widget">
								<div class="row">
									<div class="widget-caption sucess">
										<div class="col-xs-4 no-pad">
											<i class="icon icon-basket"></i>
										</div>
										<div class="col-xs-8 no-pad">
											<div class="widget-detail">
												<h3><?=$approved_company_count?></h3>
												<span>Registered Companies</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="widget unique-widget">
								<div class="row">
									<div class="widget-caption danger">
										<div class="col-xs-4 no-pad">
											<i class="icon icon-basket"></i>
										</div>
										<div class="col-xs-8 no-pad">
											<div class="widget-detail">
												<h3><?=$pending_company_count?></h3>
												<span>Unverified Companies</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row bott-wid">
						<div class="col-md-8 col-sm-8">
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Pending Internship Posts</h4>
								</div>
								<!-- Job Lists-->
								<?php if (!empty($pending_job_posts)) : foreach($pending_job_posts as $post) :?>
								<div class="job-lists">
									<div class="row">
										<div class="col-md-12 col-sm-9">
											<div class="recent-job-box">
												<div class="recent-job-img">
													<img src="<?=BASE_URL.'profiles/'.$post->company_logo?>" class="img-responsive" alt="">
												</div>
												<a href="<?=BASE_URL?>job-details.php?r=<?=$post->id?>" style="color: #62748F">
													<div class="recent-job-caption">
														<h4><?=ucwords($post->job_title)." @ ".ucwords($post->company_name)?></h4>
														<p>
															<i class="fa fa-map-marker" aria-hidden="true"></i> <?=ucwords($post->location." - ".$post->region)?> | 
															<i class="fa fa fa-clock-o" aria-hidden="true"></i> <?=ucwords($post->duration)?> 
														</p>
														<p>
															<i class="fa fa fa-clock-o" aria-hidden="true"></i> <?=ucwords("GHC ".$post->salary_expectation)?> |
															<i class="fa fa fa-tags" aria-hidden="true"></i> <?=ucwords($post->interest_category_name)?> 
														</p>
														<span class="recent-job-status"><i class="fa fa-" aria-hidden="true"></i></span>
													</div>
												</a>
												<div class="buttons-placement">
													<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
														<input type="hidden" name="job_id" value="<?=$post->id?>"/>
														<!-- <input type="hidden" name="specialization_id" value="<?=$post->interest_category_id?>"/> -->
														<button class="btn btn-success btn-xxs" name="post_status" value="Approved">Approve</button>
														<button class="btn btn-danger btn-xxs" name="post_status" value="declined">Decline</button>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endforeach; else : ?> 
									<div class="message-content">
										<h3 style="margin-left: 15px"><em>No Pending Approvals</em></h3>
									</div>
								<?php endif; ?>
								<!-- ./Job Lists-->
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Pending Students Approvals</h4>
								</div>
								<div class="full-followers">
									<?php if (!empty($pending_student)) : foreach($pending_student as $student) :?>
										<a href="../student/view_profile.php?s=<?=$student->id?>">
											<div class="user-img">
												<img src="<?=BASE_URL."profiles/".$student->profile_picture?>" alt="user" class="img-circle">
												<span class="profile-status online pull-right"></span>
												<i class="fa fa-circle active" aria-hidden="true"></i>
											</div>
											<div class="message-content">
											<h5><?=$student->fullname?></h5>
											<span class="mail-desc"><?=$student->programme_name?></span>
											<div class="buttons-placement">
												<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
													<input type="hidden" name="student_id" value="<?=$student->registration_id?>"/>
													<button class="btn btn-success btn-xxs" name="student_status_change" value="Approved">Approve</button>
													<button class="btn btn-danger btn-xxs" name="student_status_change" value="Declined">Decline</button>
												</form>
											</div>
											</div>
										</a>
									<?php endforeach; else : ?> 
										<div class="message-content">
											<h3 style="margin-left: 15px"><em>No Pending Approvals</em></h3>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<br/><br/>
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Pending Company Approvals</h4>
								</div>
								<div class="full-followers">
									<?php if (!empty($pending_companys)) : foreach($pending_companys as $company) :?>
										<a href="#">
											<div class="user-img">
												<img src="<?=BASE_URL."profiles/".$company->company_logo?>" alt="user" class="img-circle">
												<span class="profile-status online pull-right"></span>
												<i class="fa fa-circle active" aria-hidden="true"></i>
											</div>
											<div class="message-content">
											<h5><?=$company->name?></h5>
											<span class="mail-desc"><?=$company->residence_address?></span>
											<div class="buttons-placement">
												<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
													<input type="hidden" name="company_id" value="<?=$company->id?>"/>
													<button class="btn btn-success btn-xxs" name="company_status_change" value="Approved">Approve</button>
													<button class="btn btn-danger btn-xxs" name="company_status_change" value="Declined">Decline</button>
												</form>
											</div>
											</div>
										</a>
									<?php endforeach; else : ?> 
										<div class="message-content">
											<h3 style="margin-left: 15px"><em>No Pending Approvals</em></h3>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<br/><br/>
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Newsletters Signups</h4>
								</div>
								<div class="full-followers">
									<?php if (!empty($pending_subscribers)) : foreach($pending_subscribers as $subscribers) :?>
											<div class="message-content" style="margin-left: 10px">
												<a href="mailto:"><?=$subscribers->email?></a>
												
												<!-- <div class="buttons-placement">
												<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
													<input type="hidden" name="company_id" value="<?=$company->id?>"/>
													<button class="btn btn-success btn-xxs" name="company_status_change" value="Approved">Approve</button>
													<button class="btn btn-danger btn-xxs" name="company_status_change" value="Declined">Decline</button>
												</form>
											</div> -->
											</div>
										
									<?php endforeach; else : ?> 
										<div class="message-content">
											<h3 style="margin-left: 15px"><em></em></h3>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<br/><br/>
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Email Subscribers</h4>
								</div>
								<div class="full-followers">
									<?php if (!empty($pending_email_subscribers)) : foreach($pending_email_subscribers as $subscribers) :?>
											<div class="message-content" style="margin-left: 10px">
												<a href="mailto:"><?=$subscribers->email?></a>
												
												<!-- <div class="buttons-placement">
												<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
													<input type="hidden" name="company_id" value="<?=$company->id?>"/>
													<button class="btn btn-success btn-xxs" name="company_status_change" value="Approved">Approve</button>
													<button class="btn btn-danger btn-xxs" name="company_status_change" value="Declined">Decline</button>
												</form>
											</div> -->
											</div>
										
									<?php endforeach; else : ?> 
										<div class="message-content">
											<h3 style="margin-left: 15px"><em></em></h3>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			 <!-- /. PAGE WRAPPER  -->
	<?php require_once('footer.php'); ?>