﻿	<?php 
		require_once('../inc/config/config.inc.php'); 

		if(!isset($_SESSION['user']['id'])) {
			header("location: ".BASE_URL."signin.php"); exit;
		}

		if($_SESSION['user']['user_type'] != "Administrator") {
			header("location: ".BASE_URL."signin.php"); exit;
		}

		@require_once('../inc/controllers/admin_dashboard.php'); 
		require_once('header.php'); 
	?>
				<div id="page-inner">
				  <div class="row bott-wid">
						<div class="col-md-12 col-sm-12">
							<div class="recent-jobs-pannel">
								<div class="pannel-header">
									<h4>Declined Internship Posts</h4>
								</div>
								<!-- Job Lists-->
								<?php if (!empty($declined_job_posts)) : foreach($declined_job_posts as $post) :?>
								<div class="job-lists">
									<div class="row">
										<div class="col-md-12 col-sm-9">
											<div class="recent-job-box">
												<div class="recent-job-img">
													<img src="<?=BASE_URL.'profiles/'.$post->company_logo?>" class="img-responsive" alt="">
												</div>
												<div class="recent-job-caption">
													<h4><?=ucwords($post->job_title)." @ ".ucwords($post->company_name)?></h4>
													<p>
														<i class="fa fa-map-marker" aria-hidden="true"></i> <?=ucwords($post->location." - ".$post->region)?> | 
														<i class="fa fa fa-clock-o" aria-hidden="true"></i> <?=ucwords($post->duration)?> | 
														<i class="fa fa fa-clock-o" aria-hidden="true"></i> <?=ucwords("GHC ".$post->salary_expectation)?> |
														<i class="fa fa fa-tags" aria-hidden="true"></i> <?=ucwords($post->interest_category_name)?> 
													</p>
													<form action="../inc/controllers/approve_job.php" method="post" style="display: inline-block;;">
														<input type="hidden" name="job_id" value="<?=$post->id?>"/>
														<button class="btn btn-success btn-xxs" name="post_status" value="Approved">Approve</button>
													</form>
												</div>
												<div class="buttons-placement">
													
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endforeach; endif; ?>
								<!-- ./Job Lists-->
							</div>
						</div>
					</div>
				</div>
			</div>
			 <!-- /. PAGE WRAPPER  -->
	<?php require_once('footer.php'); ?>