<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Best Responsive job portal template build on Latest Bootstrap.">
		<meta name="keywords" content="job, nob board, job portal, job listing">
		<meta name="robots" content="index,follow">

		<title>GTUC Internship - Admin Portal</title>
		
		<!-- BOOTSTRAP STYLES-->
		<link href="../admin_assets/css/bootstrap.css" rel="stylesheet" />
		<!-- FONTAWESOME STYLES-->
		<link href="../admin_assets/css/font-awesome.css" rel="stylesheet" />
		<!-- Line Font STYLES-->
		<link href="../admin_assets/css/line-font.css" rel="stylesheet" />
		<!-- Dropzone Style-->
		<link href="../admin_assets/css/dropzone.css" rel="stylesheet" />
		<!-- Bootstrap Editor-->
		<link href="../admin_assets/css/bootstrap-wysihtml5.css" rel="stylesheet" />
		<!-- CUSTOM STYLES-->
		<link href="../admin_assets/css/custom.css" rel="stylesheet" />
	</head>

	<body>	  
		<div id="wrapper">
			<div class="navbar navbar-inverse navbar-fixed-top" >
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?=BASE_URL?>admin">GTUC INTERN PORTAL ADMIN</a>
					</div>
					<div class="navbar-collapse">
						<ul class="nav navbar-top-links navbar-right"> 
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<!-- /.dropdown -->
							<li class="dropdown">
								<!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
									<img src="../admin_assets/img/user.jpg" class="img-responsive img-circle" alt="user">
								</a> -->
								<ul class="dropdown-menu dropdown-user">
									<li class="divider"></li>
									<li><a href="<?=BASE_URL?>logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
									</li>
								</ul>
								<!-- /.dropdown-user -->
							</li>
							<!-- /.dropdown -->
						</ul>
						<!-- /.navbar-top-links -->
					</div>
				</div>
			</div>         
			<!-- /. NAV TOP  -->
			<nav class="navbar navbar-side">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<li class="active">
							<a href="<?=BASE_URL?>admin/"><i class="fa fa-cog" aria-hidden="true"></i>Dashboard</a>
						</li>
						<li><a href="<?=BASE_URL?>admin/published_jobs.php"><i class="fa fa-cog" aria-hidden="true"></i>All Published Jobs</a></li>
						<li><a href="<?=BASE_URL?>admin/declined_jobs.php"><i class="fa fa-cog" aria-hidden="true"></i>Declined Posts</a></li>
						<li><a href="<?=BASE_URL?>admin/company.php"><i class="fa fa-cog" aria-hidden="true"></i>Verified Companies</a></li>
						<li><a href="<?=BASE_URL?>admin/students.php"><i class="fa fa-cog" aria-hidden="true"></i>Verified Students</a></li>
						<li><a href="<?=BASE_URL?>admin/declined_students.php"><i class="fa fa-cog" aria-hidden="true"></i>Declined Students</a></li>
						<li class="log-off">
							<a href="<?=BASE_URL?>logout.php"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a>
						</li>
					</ul>
				</div>
			</nav>
			<!-- /. NAV SIDE  -->
			
			<div id="page-wrapper" >
				<div class="row bg-title">
					<!-- <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Expired Job</h4>
					</div> 
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ol class="breadcrumb">
							<li><a href="index.html">Dashboard</a></li>
							<li class="active">Expired Job</li>
						</ol>
					</div>-->
					<!-- /.col-lg-12 -->
				</div>              
				 <!-- /. ROW  -->