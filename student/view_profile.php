<?php	

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		header("location:".BASE_URL."signin.php");
		exit;
	}

	if(!empty($_GET['s']))
		$student_id = urldecode($_GET['s']);
	else {
		$_SESSION['error'] = "Student Info Access Denied";
		header("location: ".$_SERVER['HTTP_REFERER']); exit;
	}

	include_once("../header.php");  
	include_once("../inc/controllers/view_applicants.php");  
?>

	<section class=" job-bg page  ad-profile-page">
		<div class="container">
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li>Student Profile</li>
				</ol>						
				<h2 class="title"><?=$student_info->fullname?></h2>
			</div><!-- breadcrumb-section -->
			
			<div class="job-profile section">	
				<div class="user-profile">
					<div class="user-images">
						<img src="<?="../profiles/".$student_info->profile_picture?>" alt="User Images" class="img-responsive" style="height: 200px; width: 200px">
					</div>
					<div class="user">
						<h2> <a href="#"><?=$student_info->fullname?></a></h2>
						<address>
						    <p>
						    	<p><strong>Address:</strong> <?=$student_info->postal_address?> <br></p>
						    	<p><strong>Residence Addres:</strong> <?=$student_info->phone_no_1?> <br> </p>
						    	<p><strong>Primary Phone:</strong> <?=$student_info->phone_no_1?> / <?=$student_info->phone_no_2?> <br> </p>
						    	<p><strong>Email:</strong> <a href="#"> <?=$student_info->email?></a><br/></p>
						    	<p><strong>Date Of Birth :</strong> <?=$student_info->date_of_birth?></p>
						    </p>
						</address>
					</div>								
				</div><!-- user-profile -->
						
				<ul class="user-menu">
					<li class="active">
						<form method="post" action="<?=BASE_URL?>/inc/controllers/approve_job.php">
							<input type="hidden" name="student_id" value="<?=$student_info->id?>">
							<input type="hidden" name="student_email" value="<?=$student_info->email?>">
							<input type="hidden" name="post_id" value="<?=$_GET['d']?>">
							<button type="submit" class="btn btn-info" value="approved" name="student_application_status" style="color:white">Qualified</button>
						</form>
					</li>
					<li class="active">
						<form method="post" action="<?=BASE_URL?>/inc/controllers/approve_job.php">
							<input type="hidden" name="student_id" value="<?=$student_info->id?>">
							<input type="hidden" name="student_email" value="<?=$student_info->email?>">
							<input type="hidden" name="post_id" value="<?=$_GET['d']?>">
							<button type="submit" class="btn btn-danger" value="declined" name="student_application_status" style="color:white">Disquialified</button>
						</form>
					</li>
				</ul>
			</div><!-- ad-profile -->

			<div class="resume-content">
				

				<div class="career-objective section">
			        <div class="icons">
			            <i class="fa fa-black-tie" aria-hidden="true"></i>
			        </div>   
			        <div class="career-info">
			        	<h3>Career Objective / Key Interests</h3>
			        	<p><span><?=$student_info->career_objective?></span></p>
			        </div>                                 
				</div><!-- career-objective -->

				<div class="work-history section">
			        <div class="icons">
			            <i class="fa fa-briefcase" aria-hidden="true"></i>
			        </div>   
			        <div class="work-info">
			        	<h3>Recent On-Job Experience</h3>
			        	<ul>
			        		<li>
				        		<h4><?=ucwords($student_info->position_department). " @ " .strtoupper($student_info->company_name)?> <span><?=$student_info->period?></span></h4>
				        		<p><?=$student_info->duties?></p>
			        		</li>
			        	</ul>
			        </div>                                 
				</div><!-- work-history -->

				<div class="educational-background section">
					<div class="icons">
					    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
					</div>	
					<div class="educational-info">
						<h3>Education Background</h3>
						<ul>
							<li>
								<h4><?= strtoupper("Ghana Technology University College");?></h4>
								<ul>
									<li>Programme: <span style="font-weight: 600"><?=ucwords($student_info->programme_name)?></span></li>
									<li>Current Level: <span style="font-weight: 600"><?=$student_info->current_level?></span> </li><br/>
									<li>Courses Registered For Current System: 
										<ul style="padding-left: 7px;">
											<?php $all_registered_courses = explode(",", $student_info->current_registered_courses); foreach ($all_registered_courses as $courses) : ?>
											<li><span style="font-weight: 600"><?=ucwords($courses)?></span></li><br/>
											<?php endforeach; ?>
										</ul>
									</li>
								</ul>
							</li>
							<!-- <li>
								<h4><?=/*ucwords($student_info->shs_course_studied)." @ ".*/strtoupper($student_info->shs_name); ?></h4>
								<ul>
									<li>Year: <span><?=ucwords(date('F, Y',strtotime($student_info->shs_entry_date)))?> ~ <?=ucwords(date('F, Y',strtotime($student_info->shs_completion_date)))?></span> </li>
									<li>Course Studied: <span><?=ucwords($student_info->shs_course_studied)?></span></li>
									<li>Course Duration: <span><?=(int)date('Y',strtotime($student_info->shs_completion_date)) - date('Y',strtotime($student_info->shs_entry_date))?> Years</span></li>
								</ul>
							</li> -->
						</ul>
					</div>				
				</div><!-- educational-background -->								
				<!-- <div class="buttons">
					<a href="#" class="btn">Update Profile</a>
					<a href="#" class="btn cancle">Cancle</a>
				</div> -->
				<!-- <div class="download-button resume">
					<a href="#" class="btn">Download Resume as doc</a>
				</div> -->
			</div><!-- resume-content -->						
		</div><!-- container -->
	</section><!-- ad-profile-page -->

	<?php	include_once("../footer.php");  ?>