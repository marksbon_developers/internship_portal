<?php	

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		session_destroy();
		header("location:".BASE_URL."signin.php");
		exit;
	}

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
?>

	<section class=" job-bg ad-details-page">
		<div class="container">
		
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li><a href="<?=BASE_URL?>student/profile.php">My Profile</a></li>
					<li>Edit Profile</li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Edit Profile</h2>
			</div><!-- banner -->

			<div class="adpost-details post-resume">
				<div class="row">	
					<div class="col-md-10 clearfix">
						<form action="<?=BASE_URL?>inc/controllers/edit-profile.php" method="post" enctype="multipart/form-data">
							<fieldset>
								<div class="section express-yourself">
									<h4>Personal Details</h4>
									<div class="row form-group">
										<label class="col-sm-4 label-title">Change Profile Picture</label>
										<div class="col-sm-8">
											<input type="file" name="profile_pic" class="form-control" placeholder="John Doe"/>
										</div>
									</div>
									<div class="row form-group">
										<label class="col-sm-4 label-title">Fullname</label>
										<div class="col-sm-8">
											<input type="text" name="name" class="form-control" placeholder="John Doe" value="<?=$student_info->surname." ".$student_info->other_names?>" disabled>
										</div>
									</div>
									<div class="row form-group additional-information">
										<label class="col-sm-4 label-title">Additional Information</label>
										<div class="col-sm-8">
											<textarea class="form-control" placeholder="Address : 123 West 12th Street, Suite 456 New York. &#10 Phone: +012 345 678 910 &#10 Email: itsme@surzilegeek.com*" disabled>Address: <?=$student_info->residence_address?> &#10 Phone: <?=$student_info->phone_no_1?> &#10 Email: <?=$student_info->email?></textarea>
										</div>
									</div>
								</div><!-- postdetails -->
								
								<div class="section education-background">
									<h4>Education Background</h4>
									<h4 style="padding-left: 15px"><i class="fa fa-angle-double-right"></i> Tetiary Education</h4>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Name Of Institution</label>
										<div class="col-sm-9">
											<input type="text" name="name_of_tertiary" class="form-control" placeholder="" value="Ghana Technology University College" disabled>
										</div>
									</div>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Programme / Course</label>
										<div class="col-sm-9">
											<input type="text" name="tertiary_programme" class="form-control" placeholder="" value="<?=$student_info->programme_name?>" disabled>
										</div>
									</div>
									<!-- <div class="row form-group">
										<label class="col-sm-3 label-title">Concentration / Major</label>
										<div class="col-sm-9">
											<input type="text" name="tertiary_major" class="form-control" placeholder="" disabled>
										</div>
									</div> -->
									<div class="row form-group time-period">
										<label class="col-sm-3 label-title">Time Period</label>
										<div class="col-sm-9">
											<input type="text" name="tertiary_start_date" class="form-control" placeholder="dd/mm/yy" value="<?=date('F, Y',strtotime($student_info->admission_date))?>" disabled><span>-</span>
											<input type="text" name="tertiary_end_date" class="form-control pull-right" placeholder="dd/mm/yy" value="<?=date('F, Y',strtotime($student_info->completion_date))?>" disabled>
										</div>
									</div>

									<hr>

									<h4 style="padding-left: 15px"><i class="fa fa-angle-double-right"></i> Secondary Education</h4>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Name Of Institution</label>
										<div class="col-sm-9">
											<input type="text" name="name_of_secondary" class="form-control" placeholder="" value="<?=$student_info->shs_name?>" disabled>
										</div>
									</div>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Programme / Course</label>
										<div class="col-sm-9">
											<input type="text" name="secondary_programme" class="form-control" placeholder="" value="<?=$student_info->shs_course_studied?>" disabled>
										</div>
									</div>
									<!-- <div class="row form-group">
										<label class="col-sm-3 label-title">Concentration / Major</label>
										<div class="col-sm-9">
											<input type="text" name="secondary_major" class="form-control" placeholder="" disabled>
										</div>
									</div> -->
									<div class="row form-group time-period">
										<label class="col-sm-3 label-title">Time Period</label>
										<div class="col-sm-9">
											<input type="text" name="secondary_start_date" class="form-control" placeholder="Month, Year" value="<?=date('F, Y',strtotime($student_info->shs_entry_date))?>" disabled><span>-</span>
											<input type="text" name="secondary_end_date" class="form-control pull-right" placeholder="Month, Year" value="<?=date('F, Y',strtotime($student_info->shs_completion_date))?>" disabled>
										</div>
									</div>
									<!-- <div class="buttons pull-right">
										<a href="#" class="btn">Add New Education</a>
										<a href="#" class="btn delete">Delete</a>
									</div> -->
								</div><!-- work-history -->

								<div class="section">
									<h4>Latest Internship Experience</h4>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Company Name </label>
										<div class="col-sm-9">
											<input type="text" class="form-control" placeholder="Name" name="company_name" value="<?=$student_info->company_name?>" required>
										</div>
									</div>
									<div class="row form-group">
										<label class="col-sm-3 label-title">Position / Department</label>
										<div class="col-sm-9">
											<input type="text" name="position" class="form-control" placeholder="Human Resource Manager" value="<?=$student_info->position_department?>" required>
										</div>
									</div>
									<div class="row form-group time-period">
										<?php $period = explode(" ~ ", $student_info->period); ?>
										<label class="col-sm-3 label-title">Time Period</label>
										<div class="col-sm-9">
											<input type="text" name="start_date" class="form-control" placeholder="dd/mm/yy" value="<?=@$period[0]?>" required><span>-</span>
											<input type="text" name="end_date" class="form-control pull-right" placeholder="dd/mm/yy" value="<?=@$period[1]?>" required>
										</div>
									</div>
									<div class="row form-group job-description">
										<label class="col-sm-3 label-title">Duties & Responsibility</label>
										<div class="col-sm-9">
											<textarea class="form-control" placeholder="" rows="5" name="job_description" wrap="hard" required style="height: 100px !important"><?=$student_info->duties?></textarea>		
										</div>
									</div>
								</div><!-- work-history -->

								<div class="section career-objective">
									<h4>Career Objective / Key Interests</h4>
									<div class="form-group">
										<textarea class="form-control" placeholder="Write few lines about your career objective" name="career_objective" required style="height: 150px !important"><?=$student_info->career_objective?></textarea>		
									</div>
									<!-- <span>5000 characters left</span> -->
								</div><!-- career-objective -->	
							</fieldset>
							<div class="buttons">
								<button type="submit" class="btn btn-primary" name="edit_profile">Update Profile</button>
								<a href="<?=BASE_URL?>" class="btn cancle">Cancel</a>
							</div>	
						</form><!-- form -->						
					</div>
				
					<!-- quick-rules --
					<div class="col-md-4">
						<div class="section quick-rules">
							<h4>Quick rules</h4>
							<p class="lead">Posting an ad on <a href="#">jobs.com</a> is free! However, all ads must follow our rules:</p>

							<ul>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
								<li>Do not put your email or phone numbers in the title or description.</li>
								<li>Make sure you post in the correct category.</li>
								<li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
								<li>Do not upload pictures with watermarks.</li>
								<li>Do not post ads containing multiple items unless it's a package deal.</li>
							</ul>
						</div>
					</div><!-- quick-rules -->	
				</div><!-- photos-ad -->				
			</div>	
		</div><!-- container -->
	</section><!-- main -->
	
	<?php include("../footer.php"); ?>