<?php	

	include_once("../inc/config/config.inc.php");
	
	if(!isset($_SESSION['user']['id'])) {
		header("location:".BASE_URL."signin.php");
		exit;
	}

	include_once("../header.php");  
	include_once("../inc/controllers/profile.php");  
?>

	<section class=" job-bg page  ad-profile-page">
		<div class="container">
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="<?=BASE_URL?>">Home</a></li>
					<li>Student Profile</li>
				</ol>						
				<h2 class="title">My Profile</h2>
			</div><!-- breadcrumb-section -->
			
			<div class="job-profile section">	
				<div class="user-profile">
					<div class="user-images">
						<img src="<?="../profiles/".$_SESSION['user']['profile_picture']?>" alt="User Images" class="img-responsive" style="height: 200px; width: 200px">
					</div>
					<div class="user">
						<h2> <a href="#"><?=$_SESSION['user']['fullname']?></a></h2>
						<address>
						    <p>
						    	<p><strong>Address:</strong> <?=$student_info->postal_address?> <br></p>
						    	<p><strong>Residence Addres:</strong> <?=$student_info->phone_no_1?> <br> </p>
						    	<p><strong>Primary Phone:</strong> <?=$student_info->phone_no_1?> / <?=$student_info->phone_no_2?> <br> </p>
						    	<p><strong>Email:</strong> <a href="#"> <?=$_SESSION['user']['username']?></a><br/></p>
						    	<p><strong>Date Of Birth :</strong> <?=$student_info->date_of_birth?></p>
						    </p>
						</address>
					</div>

					<div class="favorites-user">
						<div class="favorites">
							<a href="#"><?=(@$student_info->total_jobs_applied) ? @$student_info->total_jobs_applied : 0?><small>Total Jobs Applied</small></a>
						</div>
					</div>								
				</div><!-- user-profile -->
						
				<ul class="user-menu">
					<li class="active"><a href="<?=BASE_URL?>student/profile.php">Profile Details </a></li>
					<li><a href="<?=BASE_URL?>student/edit-profile.php">Edit Profile</a></li>
					<li><a href="#">Bookmark</a></li>
					<li><a href="#">Intern Jobs Applied</a></li>
				</ul>
			</div><!-- ad-profile -->
			<div class="resume-content">
				<div class="career-objective section">
			        <div class="icons">
			            <i class="fa fa-black-tie" aria-hidden="true"></i>
			        </div>   
			        <div class="career-info">
			        	<h3>Career Objective / Key Interests</h3>
			        	<p><span><?=$student_info->career_objective?></span></p>
			        </div>                                 
				</div><!-- career-objective -->
				<div class="work-history section">
			        <div class="icons">
			            <i class="fa fa-briefcase" aria-hidden="true"></i>
			        </div>   
			        <div class="work-info">
			        	<h3>Recent On-Job Experience</h3>
			        	<ul>
			        		<li>
				        		<h4><?=ucwords($student_info->position_department). " @ " .strtoupper($student_info->company_name)?> <span><?=$student_info->period?></span></h4>
				        		<p><?=$student_info->duties?></p>
			        		</li>
			        	</ul>
			        </div>                                 
				</div><!-- work-history -->

				<div class="educational-background section">
					<div class="icons">
					    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
					</div>	
					<div class="educational-info">
						<h3>Education Background</h3>
						<ul>
							<li>
								<h4><?= strtoupper("Ghana Technology University College");?></h4>
								<ul>
									<li>Programme: <span style="font-weight: 600"><?=ucwords($student_info->programme_name)?></span></li>
									<li>Current Level: <span style="font-weight: 600"><?=$student_info->current_level?></span> </li><br/>
									<li>Courses Registered For Current System: 
										<ul style="padding-left: 7px;">
											<?php $all_registered_courses = explode(",", $student_info->current_registered_courses); foreach ($all_registered_courses as $courses) : ?>
											<li><span style="font-weight: 600"><?=ucwords($courses)?></span></li><br/>
											<?php endforeach; ?>
										</ul>
									</li>
								</ul>
							</li>
							<!-- <li>
								<h4><?=/*ucwords($student_info->shs_course_studied)." @ ".*/strtoupper($student_info->shs_name); ?></h4>
								<ul>
									<li>Year: <span><?=ucwords(date('F, Y',strtotime($student_info->shs_entry_date)))?> ~ <?=ucwords(date('F, Y',strtotime($student_info->shs_completion_date)))?></span> </li>
									<li>Course Studied: <span><?=ucwords($student_info->shs_course_studied)?></span></li>
									<li>Course Duration: <span><?=(int)date('Y',strtotime($student_info->shs_completion_date)) - date('Y',strtotime($student_info->shs_entry_date))?> Years</span></li>
								</ul>
							</li> -->
						</ul>
					</div>				
				</div>
				<!-- <div class="download-button resume">
					<a href="#" class="btn">Download Resume as doc</a>
				</div> -->
			</div><!-- resume-content -->						
		</div><!-- container -->
	</section><!-- ad-profile-page -->

	<?php	include_once("../footer.php");  ?>